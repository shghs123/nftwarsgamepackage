//
//  GetTargetForUltTests.swift
//  NFTWarsSimTests
//
//  Created by Zengo on 2023. 02. 17..
//

import XCTest
import NFTWarsGamePackage

final class GetTargetForUltTests: XCTestCase {

    override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
    }
    
    func testGetTargetForUlt_AllEnemy1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // Mind kettő ellenfél él, 2 target van
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 2)
        XCTAssertEqual(targets1[0].name, "heroB0")
        XCTAssertEqual(targets1[1].name, "heroB1")
        
        // 1 ellenfél él, 1 target van, és az az élő
        heroB1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB0")
        
        // Csak halott van, nincs target
        heroB0.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 0)
    }
    
    func testGetTargetForUlt_AllEnemy2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // Mind kettő ellenfél él, 2 target van
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 2)
        XCTAssertEqual(targets1[0].name, "heroA0")
        XCTAssertEqual(targets1[1].name, "heroA1")
        
        // 1 ellenfél él, 1 target van, és az az élő
        heroA1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA0")
        
        // Csak halott van, nincs target
        heroA0.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 0)
    }
    
    func testGetTargetForUlt_AllFriendly1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        
        heroA0.currentHP = 90
        heroA1.currentHP = 90
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        //2 élő és sebzett friendly target van
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 2)
        XCTAssertEqual(targets1[0].name, "heroA0")
        XCTAssertEqual(targets1[1].name, "heroA1")
        
        //1 élő és sebzett friendly target van
        heroA1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA0")
        
        //2 full HP-s target van
        heroA0.currentHP = 100
        heroA1.currentHP = 100
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 0)
    }
    
    func testGetTargetForUlt_AllFriendly2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        
        heroB0.currentHP = 90
        heroB1.currentHP = 90
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        //2 élő és sebzett friendly target van
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 2)
        XCTAssertEqual(targets1[0].name, "heroB0")
        XCTAssertEqual(targets1[1].name, "heroB1")
        
        //1 élő és sebzett friendly target van
        heroB1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB0")
        
        //2 full HP-s target van
        heroB0.currentHP = 100
        heroB1.currentHP = 100
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 0)
    }
    
    func testGetTargetForUlt_SingleFirstEnemy1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroB0")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroB0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB1")
        
        // Nics élő target
        heroB1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 0)
    }
    
    func testGetTargetForUlt_SingleFirstEnemy2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroA0")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroA0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA1")
        
        // Nics élő target
        heroA1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 0)
    }
	
	func testGetTargetForUlt_SingleLastEnemy1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1, heroB2])
		
		// 2 élő target, de az utolsót veszit ki
		let targets1 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroB2")
		
		// Az 2-es target halott így is a 1-est veszi ki
		heroB2.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroB1")
		
		// Nics élő target
		heroB0.currentHP = 0
		heroB1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets3.count, 0)
	}
	
	func testGetTargetForUlt_SingleLastEnemy2() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAmbush, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1])
		
		// 2 élő target, de az utolsót veszit ki
		let targets1 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroA2")
		
		// Az első target halott ezért a 2.-at veszi ki
		heroA2.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroA1")
		
		// Nics élő target
		heroA0.currentHP = 0
		heroA1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets3.count, 0)
	}
    
    func testGetTargetForUlt_SingleLowestCurrentHPFriendly1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        heroA0.currentHP = 90
        heroA1.currentHP = 60
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő friendly target van ezért a 2.-at veszi ki, mert kevesebb az élete
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroA1")
        
        // 2 élő friendly target van ezért a 1.-at veszi ki, mert kevesebb az élete
        heroA0.currentHP = 40
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA0")
        
        // 1 élő target van a kettőből és azt veszi ki
        heroA1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroA0")
        
        // 0 élő target van ezért nem vesz ki senkit
        heroA0.currentHP = 100
        heroA1.currentHP = 100
        let targets4 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets4.count, 0)
    }
    
    func testGetTargetForUlt_SingleLowestCurrentHPFriendly2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        heroB0.currentHP = 90
        heroB1.currentHP = 60
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő friendly target van ezért a 2.-at veszi ki, mert kevesebb az élete
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroB1")
        
        // 2 élő friendly target van ezért a 1.-at veszi ki, mert kevesebb az élete
        heroB0.currentHP = 40
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB0")
        
        // 1 élő target van a kettőből és azt veszi ki
        heroB1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroB0")
        
        // 0 élő target van ezért nem vesz ki senkit
        heroB0.currentHP = 100
        heroB1.currentHP = 100
        let targets4 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets4.count, 0)
    }
    
    func testGetTargetForUlt_SingleEnemyRandomBelow20_1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        
        heroA0.currentHP = 90
        heroA1.currentHP = 20
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő friendly target van ezért a 2.-at veszi ki, ert 20% alatt az élete
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroA1")
        
        // 2 élő friendly target van ezért a 1.-at veszi ki, ert 20% alatt az élete
        heroA0.currentHP = 1
        heroA1.currentHP = 21
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA0")
        
        // 1 élő target van a kettőből és azt veszi ki
        heroA0.currentHP = 10
        heroA1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroA0")
        
        // 0 élő target van ezért nem vesz ki senkit
        heroA0.currentHP = 0
        heroA1.currentHP = 0
        let targets4 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets4.count, 0)
        
        // Mindenki HP-ja 20% felett van target van ezért nem vesz ki senkit
        heroA0.currentHP = 21
        heroA1.currentHP = 100
        let targets5 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets5.count, 0)
        
        // Mindenki HP-ja 20% alatt van csak 1 targetre castol
        heroA0.currentHP = 19
        heroA1.currentHP = 18
        let targets6 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets6.count, 1)
        XCTAssert(targets6.first!.isTeamA)
    }
    
    func testGetTargetForUlt_SingleEnemyRandomBelow20_2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 100, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        
        heroB0.currentHP = 90
        heroB1.currentHP = 20
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        // 2 élő friendly target van ezért a 2.-at veszi ki, mert 20% alatt az élete
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroB1")
        
        // 2 élő friendly target van ezért a 1.-at veszi ki, mert 20% alatt az élete
        heroB0.currentHP = 19
        heroB1.currentHP = 21
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB0")
        
        // 1 élő target van a kettőből és azt veszi ki
        heroB0.currentHP = 1
        heroB1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroB0")
        
        // 0 élő target van ezért nem vesz ki senkit
        heroB0.currentHP = 0
        heroB1.currentHP = 0
        let targets4 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets4.count, 0)
        
        // Mindenki HP-ja 20% felett van target van ezért nem vesz ki senkit
        heroB0.currentHP = 21
        heroB1.currentHP = 100
        let targets5 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets5.count, 0)
        
        // Mindenki HP-ja 20% alatt van csak 1 targetre castol
        heroB0.currentHP = 19
        heroB1.currentHP = 18
        let targets6 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets6.count, 1)
        XCTAssert(!targets6.first!.isTeamA)
    }
    
    func testGetTargetForUlt_SingleFirstFriendly1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroA0")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroA0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA1")
        
        // Az első és második target halott ezért a 3.-at veszi ki
        heroA1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroA2")
    }
    
    func testGetTargetForUlt_SingleFirstFriendly2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1, heroB2])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroB0")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroB0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB1")
        
        // Az első és második target halott ezért a 3.-at veszi ki
        heroB1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroB2")
    }
    
    func testGetTargetForUlt_self_0() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_damageTakenReduction, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroA2")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroA0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroA2")
        
        // Az első és második target halott ezért a 3.-at veszi ki
        heroA1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroA2")
    }
    
    func testGetTargetForUlt_self_1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_phyPowerUp, level: 0)
        let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_damageTakenReduction, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1, heroB2])
        
        // 2 élő target, de az elsőt veszit ki
        let targets1 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets1.count, 1)
        XCTAssertEqual(targets1[0].name, "heroB2")
        
        // Az első target halott ezért a 2.-at veszi ki
        heroB0.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets2.count, 1)
        XCTAssertEqual(targets2[0].name, "heroB2")
        
        // Az első és második target halott ezért a 3.-at veszi ki
        heroB1.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroB2")
    }
    
    func testGetTargetForUlt_selfUnder30_0() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1])
        
        // mindenki full hp senkit se vesz ki
        let targets1 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets1.count, 0)
        
        // Saját maga full hp ezért senki sem vesz ki
        heroA0.currentHP = 10
        heroA1.currentHP = 20
        let targets2 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets2.count, 0)
        
        // Mindenki és maga is low hp, magát veszi ki fixen
        heroA2.currentHP = 31
        let targets3 = battle.getTargetForUlt(caster: heroA2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroA2")
    }
    
    func testGetTargetForUlt_selfUnder30_1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1, heroB2])
        
        // mindenki full hp senkit se vesz ki
        let targets1 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets1.count, 0)
        
        // Saját maga full hp ezért senki sem vesz ki
        heroB0.currentHP = 10
        heroB1.currentHP = 20
        let targets2 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets2.count, 0)
        
        // Mindenki és maga is low hp, magát veszi ki fixen
        heroB2.currentHP = 31
        let targets3 = battle.getTargetForUlt(caster: heroB2)
        XCTAssertEqual(targets3.count, 1)
        XCTAssertEqual(targets3[0].name, "heroB2")
    }
    
    func testGetTargetForUlt_singleRandomEnemy_0() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_hasteRegenerationBlockDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1, heroB2])
        
        let targets1 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssert(targets1[0].name.contains("heroB"))
        
        heroB1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssert(targets1[0].name.contains("heroB"))
        
        heroB2.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroA0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssert(targets1[0].name.contains("heroB"))
    }
    
    func testGetTargetForUlt_singleRandomEnemy_1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_hasteRegenerationBlockDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
  
        
        let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1])
        
        let targets1 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets1.count, 1)
        XCTAssert(targets1[0].name.contains("heroA"))
        
        heroA1.currentHP = 0
        let targets2 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets2.count, 1)
        XCTAssert(targets1[0].name.contains("heroA"))
        
        heroA2.currentHP = 0
        let targets3 = battle.getTargetForUlt(caster: heroB0)
        XCTAssertEqual(targets3.count, 1)
        XCTAssert(targets1[0].name.contains("heroA"))
    }
	
	func testGetTargetForUlt_MostBuffedEnemy1() {
		let aoeBuff = Ability.example_magicalAOEHOT
        let statusEffect1 = StatusEffect(effect: aoeBuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_immune
        let statusEffect2 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleDispell, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let targets0 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets0.count, 0)
		
		heroB0.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect1)
		heroB2.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect2)
		let targets1 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroB1")
		
		heroB0.currentBuffs = []
		heroB0.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroB1")
		
		heroB1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets3.count, 1)
		XCTAssertEqual(targets3[0].name, "heroB2")
		
		heroB2.currentHP = 0
		let targets4 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets4.count, 0)
	}
	
	func testGetTargetForUlt_MostBuffedEnemy2() {
		let aoeBuff = Ability.example_magicalAOEHOT
        let statusEffect1 = StatusEffect(effect: aoeBuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_immune
        let statusEffect2 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleDispell, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let targets0 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets0.count, 0)
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroA1.applyBuff(newEffect: statusEffect1)
		heroA2.applyBuff(newEffect: statusEffect1)
		heroA1.applyBuff(newEffect: statusEffect2)
		let targets1 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroA1")
		
		heroA0.currentBuffs = []
		heroA0.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroA1")
		
		heroA1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets3.count, 1)
		XCTAssertEqual(targets3[0].name, "heroA2")
		
		heroA2.currentHP = 0
		let targets4 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets4.count, 0)
	}
	
	func testGetTargetForUlt_MostDebuffedFriendly1() {
		let aoeDebuff = Ability.example_magicalAOEDOT
        let statusEffect1 = StatusEffect(effect: aoeDebuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleCleanse, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let targets0 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets0.count, 0)
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect1)
		heroA2.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect2)
		let targets1 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroA1")
		
		heroA0.currentDebuffs = []
		heroA0.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroA1")
		
		heroA1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets3.count, 1)
		XCTAssertEqual(targets3[0].name, "heroA2")
		
		heroA2.currentHP = 0
		let targets4 = battle.getTargetForUlt(caster: heroA0)
		XCTAssertEqual(targets4.count, 0)
	}
	
	func testGetTargetForUlt_MostDebuffedFriendly2() {
		let aoeDebuff = Ability.example_magicalAOEDOT
        let statusEffect1 = StatusEffect(effect: aoeDebuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleCleanse, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let targets0 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets0.count, 0)
		
		heroB0.applyDebuff(newEffect: statusEffect1)
		heroB1.applyDebuff(newEffect: statusEffect1)
		heroB2.applyDebuff(newEffect: statusEffect1)
		heroB1.applyDebuff(newEffect: statusEffect2)
		let targets1 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets1.count, 1)
		XCTAssertEqual(targets1[0].name, "heroB1")
		
		heroB0.currentDebuffs = []
		heroB0.currentHP = 0
		let targets2 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets2.count, 1)
		XCTAssertEqual(targets2[0].name, "heroB1")
		
		heroB1.currentHP = 0
		let targets3 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets3.count, 1)
		XCTAssertEqual(targets3[0].name, "heroB2")
		
		heroB2.currentHP = 0
		let targets4 = battle.getTargetForUlt(caster: heroB0)
		XCTAssertEqual(targets4.count, 0)
	}
}
