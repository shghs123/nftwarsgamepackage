//
//  UltimateTests.swift
//  NFTWarsSimTests
//
//  Created by Korsós Tibor on 2022. 12. 15..
//

import XCTest
import NFTWarsGamePackage

class CastUltimateTests: XCTestCase {
	
	override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
	}

	
	// MARK: - castUltimateAttack
	
	func testCastUltimateAttack_DamagePhySingle1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalStrike.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 élő target, elsőre ut
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		
		// 1 élő tartget ami a 2. és arra is ut
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamagePhySingle2() {
        let expectedDamage1 = Int((100 * Ability.example_physicalStrike.value!).rounded())
		let expectedCurrentHP1 = 1000 - expectedDamage1
		let expectedCurrentHP2 = 1000
		let expectedCurrentHP3 = 1000 - expectedDamage1
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 élő target, elsőre ut
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		
		// 1 élő tartget ami a 2. és arra is ut
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamagePhyAOE1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB1.getDefenseMultiplier(for: .phy)) * Ability.example_whirlwind.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000 - expectedDamage1 - expectedDamage1
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 élő target mind 2-t sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP1)
		
		// 1 élő target csak őt sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		XCTAssertEqual(heroB0.currentHP, 0)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamagePhyAOE2() {
        let expectedDamage1 = Int((100*Ability.example_whirlwind.value!).rounded())
		let expectedCurrentHP1 = 1000 - expectedDamage1
		let expectedCurrentHP2 = 1000 - expectedDamage1 - expectedDamage1
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 élő target mind 2-t sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP1)
		
		// 1 élő target csak őt sebzi
		heroA0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP2)
		XCTAssertEqual(heroA0.currentHP, 0)
		
		// Nincs élő target nem castol ultit
		heroA1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcSingle1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicStrike.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 target csak az elsőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		
		// 2. csak az élő target csak őt sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
		XCTAssertEqual(heroB0.currentHP, 0)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcSingle2() {
        let expectedDamage1 = Int((100 * Ability.example_magicStrike.value!).rounded())
		let expectedCurrentHP1 = 1000 - expectedDamage1
		let expectedCurrentHP2 = 1000
		let expectedCurrentHP3 = 1000 - expectedDamage1
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 target csak az elsőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP2)
		
		// 2. csak az élő target csak őt sebzi
		heroA0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP3)
		XCTAssertEqual(heroA0.currentHP, 0)
		
		heroA1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcAOE1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_tornado.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000 - expectedDamage1 - expectedDamage1
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 Target mind kettőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP1)
		
		// első target halott ezért csak a 2.-at sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		
		// Nincs élő target nem castol
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcAOE2() {
        let expectedDamage1 = Int((100 * Ability.example_tornado.value!).rounded())
		let expectedCurrentHP1 = 1000 - expectedDamage1
		let expectedCurrentHP2 = 1000 - expectedDamage1 - expectedDamage1
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
		let heroB0 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 Target mind kettőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP1)
		
		// első target halott ezért csak a 2.-at sebzi
		heroA0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP2)
		XCTAssertEqual(heroA0.currentHP, 0)
		
		// Nincs élő target nem castol
		heroA1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_HealMgcSingle1() {
        let expectedHeal1 = Int((Double(100) * Ability.example_heal.value!).rounded())
		let expectedCurrentHPAHero0_1 = 900
		let expectedCurrentHPAHero1_1 = 800 + expectedHeal1
		let expectedCurrentHP2 = 800 + expectedHeal1 + expectedHeal1
		let expectedCurrentHP3 = 1000
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = expectedCurrentHPAHero0_1
		heroA1.currentHP = 800
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 target él, de csak az elsőre castolja
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero0_1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHPAHero1_1)
		
		// 2 target van de arra castolja aminek nincs full HP-ja
		heroA0.currentHP = 1000
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP2)
		
		// Nem castol mert mindenkinek full HP-ja van
		heroA1.currentHP = 1000
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
		
		// Nem csordul tul a heal
		heroA0.currentHP = 999
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted4)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP3)
		
		// Nem castol mert neki max a HP-ja, a másik pedig halott
		heroA0.currentHP = 0
		let isUltCasted5 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted5)
	}
	
	func testCastUltimateAttack_HealMgcAOE1() {
        let expectedHeal1 = Int((Double(100) * Ability.example_AOEHeal.value!).rounded())
		let expectedCurrentHP1 = 800 + expectedHeal1
		let expectedCurrentHP2 = 800 + expectedHeal1 + expectedHeal1
		let expectedCurrentHP3 = 1000
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 800
		heroA1.currentHP = 800
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 target van mind2 sebzett ezért mind2-re healel
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP1)
		
		// 1 élő target van, arra healel, a halottra nem
		heroA1.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
		XCTAssertEqual(heroA1.currentHP, 0)
		
		// 2 élő sebzett target van, mindkettőre healel, de nem overhealel
		heroA0.currentHP = 999
		heroA1.currentHP = 999
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted3)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP3)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP3)
		
		// 2 élő full HP-s target van, nem castol ultit
		heroA0.currentHP = 1000
		heroA1.currentHP = 1000
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted4)
	}
	
	func testCastUltimateAttack_DamageMgcLifeSteal1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicLifeSteal.value!).rounded())
        let expectedCurrentHP1_heroA = 500 + expectedDamage1
        let expectedCurrentHP1_heroB0 = 1000 - expectedDamage1
        let expectedCurrentHP1_heroB1 = 1000
        let expectedCurrentHP2_heroA = 500 + expectedDamage1 + expectedDamage1
        let expectedCurrentHP2_heroB1 = 1000 - expectedDamage1
        
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 target csak az elsőt sebzi
		heroA0.currentHP = 500
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1_heroB0)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP1_heroB1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1_heroA)
		
		// 2. csak az élő target csak őt sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2_heroB1)
		XCTAssertEqual(heroB0.currentHP, 0)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2_heroA)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamagePhyLifeSteal1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalLifeSteal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
        let expectedDamage1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalLifeSteal.value!).rounded())
        let expectedCurrentHP1_heroA = 500 + expectedDamage1
        let expectedCurrentHP1_heroB0 = 1000 - expectedDamage1
        let expectedCurrentHP1_heroB1 = 1000
        let expectedCurrentHP2_heroA = 500 + expectedDamage1 + expectedDamage1
        let expectedCurrentHP2_heroB1 = 1000 - expectedDamage1
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 target csak az elsőt sebzi
		heroA0.currentHP = 500
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1_heroB0)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP1_heroB1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1_heroA)
		
		// 2. csak az élő target csak őt sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2_heroB1)
		XCTAssertEqual(heroB0.currentHP, 0)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2_heroA)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcPhySingle1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPhyStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 100, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 100, ulti: Ability.example_heal, level: 0)
		
        let expectedMgcDmg = Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedPhyDmg = Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedDamage1 = expectedPhyDmg + expectedMgcDmg
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		// 2 target csak az elsőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
		
		// 2. csak az élő target csak őt sebzi
		heroB0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
		XCTAssertEqual(heroB0.currentHP, 0)
		
		// Nincs élő target nem castol ultit
		heroB1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted3)
	}
	
	func testCastUltimateAttack_DamageMgcPhySingle2() {
        let expectedMgcDmg = Int((Double(100) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedPhyDmg = Int((Double(100) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
		let expectedDamage1 = expectedPhyDmg + expectedMgcDmg
		let expectedCurrentHP1 = 1000 - expectedDamage1
		let expectedCurrentHP2 = 1000
		let expectedCurrentHP3 = 1000 - expectedDamage1
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPhyStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPhyStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 100, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPhyStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		
		// 2 target csak az elsőt sebzi
		let isUltCasted1 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP1)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP2)
		
		// 2. csak az élő target csak őt sebzi
		heroA0.currentHP = 0
		let isUltCasted2 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA1.currentHP, expectedCurrentHP3)
		XCTAssertEqual(heroA0.currentHP, 0)
		
		heroA1.currentHP = 0
		let isUltCasted3 = battle.castUltimateAttack(caster: heroB0)
		XCTAssert(!isUltCasted3)
	}
	
    func testCastUltimateAttack_DamageTrueSingle1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 1000, magicResist: 1000, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 1000, magicResist: 1000, ulti: Ability.example_heal, level: 0)
        
        let expectedDmg = Int(Ability.example_trueStrike.value!)
        let expectedDamage1 = expectedDmg
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 target csak az elsőt sebzi
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2. csak az élő target csak őt sebzi
        heroB0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        XCTAssertEqual(heroB0.currentHP, 0)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamagePhySingleMaxHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(1000 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMaxHPStrike.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra is ut
        heroB0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamagePhySingleMaxHP2() {
        let expectedDamage1 = Int((1000 * Ability.example_physicalMaxHPStrike.value!).rounded())
        let expectedCurrentHP1 = 1000 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 1000 - expectedDamage1
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra is ut
        heroB0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamageMgcSingleMaxHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(900 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMaxHPStrike.value!).rounded())
        let expectedCurrentHP1 = 900 - expectedDamage1
        let expectedCurrentHP2 = 900
        let expectedCurrentHP3 = 900 - expectedDamage1
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra is ut
        heroB0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamageMgcSingleMaxHP2() {
        let expectedDamage1 = Int((900 * Ability.example_magicMaxHPStrike.value!).rounded())
        let expectedCurrentHP1 = 900 - expectedDamage1
        let expectedCurrentHP2 = 900
        let expectedCurrentHP3 = 900 - expectedDamage1
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra is ut
        heroB0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamagePhySingleCurrHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(800 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalCurrentHPStrike.value!).rounded())
        let expectedCurrentHP1 = 800 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 500 - Int((Double(500 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalCurrentHPStrike.value!).rounded())
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 800
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 500
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamagePhySingleCurrHP2() {
        let expectedDamage1 = Int((750 * Ability.example_physicalCurrentHPStrike.value!).rounded())
        let expectedCurrentHP1 = 750 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 300 - Int((300 * Ability.example_physicalCurrentHPStrike.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 750
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 300
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamageMgcSingleCurrHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(800 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicCurrentHPStrike.value!).rounded())
        let expectedCurrentHP1 = 800 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 500 - Int((Double(500 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicCurrentHPStrike.value!).rounded())
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 800
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 500
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamageMgcSingleCurrHP2() {
        let expectedDamage1 = Int((750 * Ability.example_magicCurrentHPStrike.value!).rounded())
        let expectedCurrentHP1 = 750 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 300 - Int((300 * Ability.example_magicCurrentHPStrike.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 750
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 300
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DamagePhySingleMissHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(200 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMissingHPStrike.value!).rounded())
        let expectedCurrentHP1 = 800 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 600 - Int((Double(400 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMissingHPStrike.value!).rounded())
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 800
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2 élő target, elsőre ut 0-t
        heroB0.currentHP = 1100
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentHP, 1000)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 600
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted3)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted4)
    }
    
    func testCastUltimateAttack_DamagePhySingleMissHP2() {
        let expectedDamage1 = Int((250 * Ability.example_physicalMissingHPStrike.value!).rounded())
        let expectedCurrentHP1 = 750 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 300 - Int((700 * Ability.example_physicalMissingHPStrike.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 750
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2 élő target, elsőre ut 0-t
        heroB0.currentHP = 1100
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentHP, 1000)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 300
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted3)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted4)
    }
    
    func testCastUltimateAttack_DamageMgcSingleMissHP1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 150, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 150, ulti: Ability.example_heal, level: 0)
        
        let expectedDamage1 = Int((Double(200 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMissingHPStrike.value!).rounded())
        let expectedCurrentHP1 = 800 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 600 - Int((Double(400 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMissingHPStrike.value!).rounded())
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 800
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2 élő target, elsőre ut 0-t
        heroB0.currentHP = 1100
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentHP, 1000)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 600
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted3)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted4)
    }
    
    func testCastUltimateAttack_DamageMgcSingleMissHP2() {
        let expectedDamage1 = Int((250 * Ability.example_magicMissingHPStrike.value!).rounded())
        let expectedCurrentHP1 = 750 - expectedDamage1
        let expectedCurrentHP2 = 1000
        let expectedCurrentHP3 = 300 - Int((700 * Ability.example_magicMissingHPStrike.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 10, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 élő target, elsőre ut
        heroB0.currentHP = 750
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2 élő target, elsőre ut 0-t
        heroB0.currentHP = 1100
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentHP, 1000)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 1 élő tartget ami a 2. és arra ut
        heroB0.currentHP = 0
        heroB1.currentHP = 300
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted3)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP3)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted4)
    }
    
    func testCastUltimateAttack_DamageExecuteSingle1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 1000, magicResist: 1000, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 1000, magicResist: 1000, ulti: Ability.example_heal, level: 0)
        
        let expectedCurrentHP1 = 1000
        let expectedCurrentHP2 = 1000
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 target full hpn, nem castol
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted1)
        XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
        XCTAssertEqual(heroB1.currentHP, expectedCurrentHP2)
        
        // 2. csak az élő target csak őt sebzi
        heroB0.currentHP = 0
        heroB1.currentHP = 200
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB1.currentHP, 0)
        
        // Nincs élő target nem castol ultit
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
        
        // Nincs 20% alatti target nem castol ultit
        heroB0.currentHP = 1000
        heroB1.currentHP = 250
        let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted4)
    }
    
    func testCastUltimateAttack_HOTEffectApply() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        // Mindenki full HP nem castol
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        
        // 2 target, csak a 2.ra rakja a buffot, mert ő a kevesebb hp-s
        heroA1.currentHP = 500
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        let isUltCasted1_5 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssert(isUltCasted1_5)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.first!.ultimate.id, Ability.example_magicalHOT.id)
        
        // 1. csak az élő target csak őt healeli
        heroA1.currentBuffs = []
        heroA1.currentHP = 0
        heroA0.currentHP = 200
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.first!.ultimate.id, Ability.example_magicalHOT.id)
    }
    
    func testCastUltimateAttack_DebuffEffectApply() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        // 2 target csak az elsőre rakja a buffot
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.first!.ultimate.id, Ability.example_physicalDOT.id)
        
        // 2. csak az élő target csak őt sebzi
        heroB0.currentDebuffs = []
        heroB0.currentHP = 0
        heroB1.currentHP = 200
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.first!.ultimate.id, Ability.example_physicalDOT.id)
        
        // Nincs élő target nem castol ultit
        heroB1.currentDebuffs = []
        heroB1.currentHP = 0
        let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(!isUltCasted3)
    }
    
    func testCastUltimateAttack_DebuffDurationReset() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.first!.remainingDuration, Ability.example_physicalDOT.duration!)
        
        heroB0.currentDebuffs.first!.remainingDuration = 1
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.first!.remainingDuration, Ability.example_physicalDOT.duration!)
    }
    
    func testCastUltimateAttack_BuffDurationReset() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        heroA0.currentHP = 500
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.first!.remainingDuration, Ability.example_magicalHOT.duration!)
        
        heroA0.currentBuffs.first!.remainingDuration = 1
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.first!.remainingDuration, Ability.example_magicalHOT.duration!)
    }
    
    func testCastUltimateAttack_AOEHealOnFullHP() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)        
    }
    
    func testCastUltimateAttack_PowerUp_phy() {
        let buff = Ability.example_phyPowerUp
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, Int(buff.value!))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, Int(buff.value!))
    }
    
    func testCastUltimateAttack_PowerUp_mgc() {
        let buff = Ability.example_mgcPowerUp
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, Int(buff.value!))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, Int(buff.value!))
    }
    
    func testCastUltimateAttack_PowerDown_phy_0() {
        let debuff = Ability.example_phyPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCastUltimateAttack_PowerDown_phy_1() {
        let debuff = Ability.example_phyPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 383, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 383 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 383 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDown_mgc_0() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPowerDown, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
    }
    
    func testCastUltimateAttack_PowerDown_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 37)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 591, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 591 - Int((debuff.value! + 37.0 * Constants.example_mgcPowerUpValueLVLMultiplier).rounded()))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 591 - Int((debuff.value! + 37.0 * Constants.example_mgcPowerUpValueLVLMultiplier).rounded()))
    }
    
    func testCastUltimateAttack_PowerUpAOE_phy() {
        let buff = Ability.example_phyPowerUpAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 50, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, Int(buff.value!))
        XCTAssertEqual(heroA1.currentPhy, 50 + Int(buff.value!))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, Int(buff.value!))
        XCTAssertEqual(heroA1.currentPhy, 50 + Int(buff.value!))
    }
    
    func testCastUltimateAttack_PowerUpAOE_mgc() {
        let buff = Ability.example_mgcPowerUpAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 72, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, Int(buff.value!))
        XCTAssertEqual(heroA1.currentMgc, 72 + Int(buff.value!))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, Int(buff.value!))
        XCTAssertEqual(heroA1.currentMgc, 72 + Int(buff.value!))
    }
    
    func testCastUltimateAttack_PowerDownAOE_phy_0() {
        let debuff = Ability.example_phyPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCastUltimateAttack_PowerDownAOE_phy_1() {
        let debuff = Ability.example_phyPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 277, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 277 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 277 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDownAOE_mgc_0() {
        let debuff = Ability.example_mgcPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCastUltimateAttack_PowerDownAOE_mgc_1() {
        let debuff = Ability.example_mgcPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 578, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 578 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 578 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDown_armor_0() {
        let debuff = Ability.example_armorPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentArmor, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentArmor, 0)
    }
    
    func testCastUltimateAttack_PowerDown_armor_1() {
        let debuff = Ability.example_armorPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 383, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentArmor, 383 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentArmor, 383 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDown_magicResist_0() {
        let debuff = Ability.example_magicResistPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentArmor, 0)

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentArmor, 0)
    }
    
    func testCastUltimateAttack_PowerDown_magicResist_1() {
        let debuff = Ability.example_magicResistPowerDown
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 345, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMR, 345 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMR, 345 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDownAOE_armor_0() {
        let debuff = Ability.example_armorPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 383, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 300, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentArmor, 383 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
        XCTAssertEqual(heroB1.currentArmor, 300 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentArmor, 383 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
        XCTAssertEqual(heroB1.currentArmor, 300 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_PowerDownAOE_magic_resist_0() {
        let debuff = Ability.example_magicResistPowerDownAOE
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 60)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 283, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 250, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMR, 283 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))
        XCTAssertEqual(heroB1.currentMR, 250 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))

        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMR, 283 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))
        XCTAssertEqual(heroB1.currentMR, 250 - Int(debuff.value! + 60.0 * Constants.example_magicResistPowerDownValueLVLMultiplier))
    }
    
    func testCastUltimateAttack_hasteToZero_0() {
        let spell = Ability.example_hasteZero
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: spell, level: 60)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 650
        heroB1.currentHaste = 300
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHaste, 300)
        
        heroB0.currentHP = 0
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHaste, 0)
        
        heroB0.currentHP = 1000
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHaste, 0)
    }
    
    func testCastUltimateAttack_Absorb() {
        let ability = Ability.example_singleAbsorb
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        // Mindenki él 1.-re castol
        heroA0.currentHP = 500
        heroA1.currentHP = 500
        let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
        XCTAssert(isUltCasted0)
        XCTAssertEqual(heroA0.currentHP, 500)
        XCTAssertEqual(heroA0.currentAbsorb, Int(ability.value!))
        XCTAssertEqual(heroA1.currentAbsorb, 0)
        
        // 2 target, csak az 1.re castolja a hero1
        let isUltCasted1 = battle.castUltimateAttack(caster: heroA1)
        XCTAssert(isUltCasted1)
        XCTAssertEqual(heroA0.currentHP, 500)
        XCTAssertEqual(heroA0.currentAbsorb, 2 * Int(ability.value!))
        XCTAssertEqual(heroA1.currentAbsorb, 0)
        
        // 2. csak az élő target rá castolja
        heroA0.currentAbsorb = 0
        heroA0.currentHP = 0
        let isUltCasted2 = battle.castUltimateAttack(caster: heroA1)
        XCTAssert(isUltCasted2)
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA1.currentHP, 500)
        XCTAssertEqual(heroA0.currentAbsorb, 0)
        XCTAssertEqual(heroA1.currentAbsorb, Int(ability.value!))
    }
	
	func testCastUltimateAttack_reorderEnemyTeam() {
		let ability = Ability.example_reorderEnemyTeam
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		XCTAssertEqual(heroB0.teamPosition, 0)
		XCTAssertEqual(heroB1.teamPosition, 1)
		XCTAssertEqual(heroB2.teamPosition, 2)
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted0)

		while [heroB0.teamPosition, heroB1.teamPosition, heroB2.teamPosition] == [0, 1, 2] {
			let isUltCasted = battle.castUltimateAttack(caster: heroA0)
			XCTAssert(isUltCasted)
		}
	}
	
	func testCastUltimateAttack_singleCleanse() {
		let aoeDebuff = Ability.example_magicalAOEDOT
        let statusEffect1 = StatusEffect(effect: aoeDebuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_immune
        let statusEffect3 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleCleanse, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted0)
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect1)
		heroA2.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect2)
		heroA0.applyBuff(newEffect: statusEffect3)
		heroA1.applyBuff(newEffect: statusEffect3)
		heroA2.applyBuff(newEffect: statusEffect3)
		
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 2)
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 1)
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0) // ezt cleanselte
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 1)
		
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0) // ezt cleanselte
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0)
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 1)
		
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted3)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0)
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 0) // ezt cleanselte
		
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted4)
	}
	
	func testCastUltimateAttack_singleDispell() {
		let aoeBuff = Ability.example_magicalAOEHOT
        let statusEffect1 = StatusEffect(effect: aoeBuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_immune
        let statusEffect2 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect3 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_singleDispell, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted0)
		
		heroB0.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect1)
		heroB2.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect2)
		heroB0.applyDebuff(newEffect: statusEffect3)
		heroB1.applyDebuff(newEffect: statusEffect3)
		heroB2.applyDebuff(newEffect: statusEffect3)
		
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 2)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 1)
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)  // ezt dispellezte
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 1)
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		
		let isUltCasted2 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted2)
		XCTAssertEqual(heroB0.currentBuffs.count, 0) // ezt dispellezte
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 1)
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		
		let isUltCasted3 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted3)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 0) // ezt dispellezte
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(!isUltCasted4)
	}
	
	func testCastUltimateAttack_aoeCleanse() {
		let aoeDebuff = Ability.example_magicalAOEDOT
        let statusEffect1 = StatusEffect(effect: aoeDebuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_immune
        let statusEffect3 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_aoeCleanse, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted0)
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect1)
		heroA2.applyDebuff(newEffect: statusEffect1)
		heroA1.applyDebuff(newEffect: statusEffect2)
		heroA0.applyBuff(newEffect: statusEffect3)
		heroA1.applyBuff(newEffect: statusEffect3)
		heroA2.applyBuff(newEffect: statusEffect3)
		
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 2)
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 1)
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0)
		XCTAssertEqual(heroA2.currentBuffs.count, 1)
		XCTAssertEqual(heroA2.currentDebuffs.count, 0)
		
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted4)
	}
    
	func testCastUltimateAttack_aoeDispell() {
		let aoeBuff = Ability.example_magicalAOEHOT
        let statusEffect1 = StatusEffect(effect: aoeBuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let buff = Ability.example_enrage
        let statusEffect2 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let debuff = Ability.example_hasteRegenerationBlockDOT
        let statusEffect3 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_aoeDispell, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB2 = SimulationEntity(name: "heroB2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1, heroA2], teamB: [heroB0, heroB1, heroB2])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted0)
		
		heroB0.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect1)
		heroB2.applyBuff(newEffect: statusEffect1)
		heroB1.applyBuff(newEffect: statusEffect2)
		heroB0.applyDebuff(newEffect: statusEffect3)
		heroB1.applyDebuff(newEffect: statusEffect3)
		heroB2.applyDebuff(newEffect: statusEffect3)
		
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 2)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 1)
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		let isUltCasted1 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB2.currentBuffs.count, 0)
		XCTAssertEqual(heroB2.currentDebuffs.count, 1)
		
		let isUltCasted4 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted4)
	}
	
	func testCastUltimateAttack_single_stun_0() {
		let ability = Ability.example_singleStun
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 60)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentDebuffs.count, 0)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
	}
	
	func testCastUltimateAttack_aoe_stun_0() {
		let ability = Ability.example_aoeStun
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 60)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
		
		let isUltCasted0 = battle.castUltimateAttack(caster: heroA0)
		XCTAssert(isUltCasted0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
	}
}
