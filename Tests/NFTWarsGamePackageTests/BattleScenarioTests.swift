//
//  BattleScenarioTests.swift
//  NFTWarsSimTests
//
//  Created by Korsós Tibor on 2022. 12. 16..
//

import XCTest
import NFTWarsGamePackage

class BattleScenarioTests: XCTestCase {
	
	override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
	}

	// A0 dies by base before could heal himself
	func testActionOrdersScenarios_1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 1
		heroA0.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, 0)
	}

	// A0 dies by base before mate could heal him
	func testActionOrdersScenarios_2() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 1
		heroA1.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, 0)
	}

	// A0 dies by base before could heal aoe himself
	func testActionOrdersScenarios_3() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		
		heroA0.currentHP = 1
		heroA0.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, 0)
	}

	// A0 dies by base before mate could aoe heal him
	func testActionOrdersScenarios_4() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		
		heroA0.currentHP = 1
		heroA1.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, 0)
	}
	
	// A0 dies by base before could lifeSteal himself
	func testActionOrdersScenarios_5() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 1
		heroA0.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, 0)
	}
	
	// A0 heals him full then gets a hit
	func testActionOrdersScenarios_6() {
		let expectedHP1_heroA0 = Int((1000 - (30 * Constants.baseAttackMultiplier)).rounded())
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 999
		heroA0.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
	}
	
	// A0 lifeSteals him full then gets a hit
	func testActionOrdersScenarios_7() {
		let expectedHP1_heroA0 = Int((1000 - (30 * Constants.baseAttackMultiplier)).rounded())
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 30, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		heroA0.currentHP = 999
		heroA0.currentHaste = 650
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		
		XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
	}
	
	func testDotsAndHots() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalDOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_magicalDOT, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalHOT, level: 0)
		
		let heroA0ExpectedDMG = 220 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0ExpectedTickDMG = 220 * Ability.example_physicalDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0ExpectedDMG = 240 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroB0ExpectedTickDMG = 275 * Ability.example_magicalDOT.value! * heroA0.getDefenseMultiplier(for: .mgc)
		let heroA1ExpectedTickHeal = 120 * Ability.example_magicalHOT.value!
		let heroB1ExpectedTickHeal = 100 * Ability.example_magicalHOT.value!
		let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
		let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
		let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHaste, 20)
		
		heroA0.currentHaste = 650
		heroA1.currentHaste = 600
		heroB1.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
		XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
		XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
		XCTAssertEqual(heroA0.currentHaste, 50)
		XCTAssertEqual(heroB0.currentHaste, 40)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
		XCTAssertEqual(heroA0.currentHaste, 60)
		XCTAssertEqual(heroB0.currentHaste, 60)
		
		heroB0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 4)
		XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 3)
		XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
		XCTAssertEqual(heroA0.currentHaste, 70)
		XCTAssertEqual(heroB0.currentHaste, 50)
		
		// Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 5)
		XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
		XCTAssertEqual(heroA0.currentHaste, 80)
		XCTAssertEqual(heroB0.currentHaste, 70)
	}
	
	func testDifferentDotsSameTarget1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 190, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 160, mgc: 260, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalDOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 90, magicResist: 145, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let heroA0ExpectedDMG = 220 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0ExpectedTickDMG = 225 * Ability.example_physicalDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
		let heroA1ExpectedDMG = 160 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA1ExpectedTickDMG = 260 * Ability.example_magicalDOT.value! * heroB0.getDefenseMultiplier(for: .mgc)
		
		let expectedHP1_heroB0 = 1000 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA1ExpectedDMG.rounded())
		let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded())
		let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
		
		heroA0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)

		heroA1.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 2)
		XCTAssertEqual(heroB1.currentDebuffs.count, 0)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
		XCTAssertEqual(heroB0.currentDebuffs[1].remainingDuration, Ability.example_magicalDOT.duration! - 1)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 4)
		XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 2)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
		XCTAssertEqual(heroB0.currentDebuffs[1].remainingDuration, Ability.example_magicalDOT.duration! - 2)

		// Ha 4 a spell duration csak jár le az egyik DOT a B0-n
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 5)
		XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 3)
		
		// Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 6)
		XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 0)
	}
	
	func testDifferentDotsSameTarget2() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 190, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalAOEDOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 160, mgc: 260, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalDOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 90, magicResist: 145, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let heroA0ExpectedDMG = 220 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0ExpectedTickDMG = 225 * Ability.example_physicalAOEDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
		let heroA1ExpectedDMG = 160 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA1ExpectedTickDMG = 260 * Ability.example_magicalDOT.value! * heroB0.getDefenseMultiplier(for: .mgc)
		
		let expectedHP1_heroB0 = 1000 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA1ExpectedDMG.rounded())
		let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded())
		let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA1ExpectedDMG.rounded()) - Int(heroA1ExpectedTickDMG.rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
		
		heroA0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)

		heroA1.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 2)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 2)
		XCTAssertEqual(heroB0.currentDebuffs[1].remainingDuration, Ability.example_magicalDOT.duration! - 1)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 4)
		XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 2)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 3)
		XCTAssertEqual(heroB0.currentDebuffs[1].remainingDuration, Ability.example_magicalDOT.duration! - 2)

		// Ha 4 a spell duration csak jár le az egyik DOT a B0-n
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 5)
		XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 3)
		
		// Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 6)
		XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 0)
	}
	
	func testDifferentHOTSSameTarget1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 185, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 260, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
		
		let heroA0ExpectedTickDMG = 185 * Ability.example_magicalAOEHOT.value!
		let heroA1ExpectedTickDMG = 260 * Ability.example_magicalHOT.value!
		
		let expectedHP1_heroA0 = 500
		let expectedHP1_heroA1 = 300
		let expectedHP2_heroA0 = expectedHP1_heroA0  + Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP2_heroA1 = expectedHP1_heroA1  + Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP3_heroA0 = expectedHP2_heroA0  + Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP3_heroA1 = expectedHP2_heroA1  + Int(heroA0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP4_heroA0 = expectedHP3_heroA0  + Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP4_heroA1 = expectedHP3_heroA1  + Int(heroA0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP5_heroA0 = expectedHP4_heroA0  + Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP5_heroA1 = expectedHP4_heroA1  + Int(heroA0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickDMG.rounded())
		let expectedHP6_heroA0 = expectedHP5_heroA0
		let expectedHP6_heroA1 = expectedHP5_heroA1 + Int(heroA1ExpectedTickDMG.rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
		heroA0.currentHP = 500
		heroA1.currentHP = 300
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP1_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
		
		heroA0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP2_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)
		XCTAssertEqual(heroA1.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)


		heroA1.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP3_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 2)
		XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 2)
		XCTAssertEqual(heroA1.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 2)
		XCTAssertEqual(heroA1.currentBuffs[1].remainingDuration, Ability.example_magicalHOT.duration! - 1)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 4)
		XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP4_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 2)
		XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 3)
		XCTAssertEqual(heroA1.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 3)
		XCTAssertEqual(heroA1.currentBuffs[1].remainingDuration, Ability.example_magicalHOT.duration! - 2)

		// Ha 4 a spell duration csak jár le az egyik DOT a B0-n
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 5)
		XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP5_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 3)
		
		// Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 6)
		XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
		XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
	}
	
	func testAOEDotsAndHots() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalAOEDOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_magicalAOEDOT, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0ExpectedDMG = 220 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0ExpectedTickDMG = 225 * Ability.example_physicalAOEDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0ExpectedDMG = 240 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroB0ExpectedTickDMG = 275 * Ability.example_magicalAOEDOT.value! * heroA0.getDefenseMultiplier(for: .mgc)
		let heroA1ExpectedTickHeal = 120 * Ability.example_magicalAOEHOT.value!
		let heroB1ExpectedTickHeal = 100 * Ability.example_magicalAOEHOT.value!
		let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
		let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
		let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded()) + Int(heroB1ExpectedTickHeal.rounded())
		let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded()) + Int(heroA1ExpectedTickHeal.rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHaste, 20)
		
		heroA0.currentHaste = 650
		heroA1.currentHaste = 600
		heroB1.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
		XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)
		XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
		XCTAssertEqual(heroB1.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)
		XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)
		XCTAssertEqual(heroA1.currentBuffs[0].remainingDuration, Ability.example_magicalAOEHOT.duration! - 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].ultimate.id, Ability.example_physicalAOEDOT.id)
		XCTAssertEqual(heroB0.currentBuffs[0].ultimate.id, Ability.example_magicalAOEHOT.id)
		XCTAssertEqual(heroB1.currentDebuffs[0].ultimate.id, Ability.example_physicalAOEDOT.id)
		XCTAssertEqual(heroB1.currentBuffs[0].ultimate.id, Ability.example_magicalAOEHOT.id)
		XCTAssertEqual(heroA0.currentBuffs[0].ultimate.id, Ability.example_magicalAOEHOT.id)
		XCTAssertEqual(heroA1.currentBuffs[0].ultimate.id, Ability.example_magicalAOEHOT.id)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA1.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 1)
		
		heroB0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 4)
		XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA1.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB1.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroB1.currentBuffs.count, 1)

		// Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 5)
		XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
		XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 0)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
	}
	
	func testArmorDownGrade() {
		let debuff = Ability.example_armorPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_physicalStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0ExpectedDefenseMultiplier = 1 - (((log2(Double(heroA0.armor)) * log2(Double(heroA0.armor))) / Constants.defenseMultiplier)/100)
		let heroB0ExpectedDefenseMultiplier = 1 - (((log2(Double(heroB0.armor)) * log2(Double(heroB0.armor))) / Constants.defenseMultiplier)/100)
		let defuseValueForA0 = debuff.value! + (Double(statusEffect1.casterLevel) * Constants.example_armorPowerDownValueLVLMultiplier)
		let defuseValueForB0 = debuff.value! + (Double(statusEffect0.casterLevel) * Constants.example_armorPowerDownValueLVLMultiplier)
		let heroA0DefusedArmorValue = Double(heroA0.armor)-defuseValueForA0
		let heroB0DefusedArmorValue = Double(heroB0.armor)-defuseValueForB0
		let heroA0ExpectedDefusedDefenseMultiplier = 1 - (((log2(Double(heroA0DefusedArmorValue)) * log2(Double(heroA0DefusedArmorValue))) / Constants.defenseMultiplier)/100)
		let heroB0ExpectedDefusedDefenseMultiplier = 1 - (((log2(Double(heroB0DefusedArmorValue)) * log2(Double(heroB0DefusedArmorValue))) / Constants.defenseMultiplier)/100)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0ExpectedDefenseMultiplier
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0ExpectedDefenseMultiplier
		let heroA0DefusedBaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0ExpectedDefusedDefenseMultiplier
		let heroB0DefusedBaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0ExpectedDefusedDefenseMultiplier
		let heroA0ExpectedDefusedUltDMG = Double(heroA0.phy) * heroB0ExpectedDefusedDefenseMultiplier * Ability.example_physicalStrike.value!
		let heroB0ExpectedDefusedUltDMG = Double(heroB0.phy) * heroA0ExpectedDefusedDefenseMultiplier * Ability.example_physicalStrike.value!
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.getDefenseMultiplier(for: .phy), heroA0ExpectedDefenseMultiplier)
		XCTAssertEqual(heroB0.getDefenseMultiplier(for: .phy), heroB0ExpectedDefenseMultiplier)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroB0.applyDebuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.getDefenseMultiplier(for: .phy), heroA0ExpectedDefusedDefenseMultiplier)
		XCTAssertEqual(heroB0.getDefenseMultiplier(for: .phy), heroB0ExpectedDefusedDefenseMultiplier)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0DefusedBaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0DefusedBaseAttackDamage.rounded()))
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0ExpectedDefusedUltDMG.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0ExpectedDefusedUltDMG.rounded()))
	}
	
	func testMagicResistDownGrade() {
		let debuff = Ability.example_magicResistPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 220, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_magicStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_magicStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0ExpectedDefenseMultiplier = 1 - (((log2(Double(heroA0.magicResist)) * log2(Double(heroA0.magicResist))) / Constants.defenseMultiplier)/100)
		let heroB0ExpectedDefenseMultiplier = 1 - (((log2(Double(heroB0.magicResist)) * log2(Double(heroB0.magicResist))) / Constants.defenseMultiplier)/100)
		let defuseValueForA0 = debuff.value! + (Double(statusEffect1.casterLevel) * Constants.example_magicResistPowerDownValueLVLMultiplier)
		let defuseValueForB0 = debuff.value! + (Double(statusEffect0.casterLevel) * Constants.example_magicResistPowerDownValueLVLMultiplier)
		let heroA0DefusedArmorValue = Double(heroA0.magicResist)-defuseValueForA0
		let heroB0DefusedArmorValue = Double(heroB0.magicResist)-defuseValueForB0
		let heroA0ExpectedDefusedDefenseMultiplier = 1 - (((log2(Double(heroA0DefusedArmorValue)) * log2(Double(heroA0DefusedArmorValue))) / Constants.defenseMultiplier)/100)
		let heroB0ExpectedDefusedDefenseMultiplier = 1 - (((log2(Double(heroB0DefusedArmorValue)) * log2(Double(heroB0DefusedArmorValue))) / Constants.defenseMultiplier)/100)
		
		let heroA0ExpectedDefusedUltDMG = Double(heroA0.mgc) * heroB0ExpectedDefusedDefenseMultiplier * Ability.example_magicStrike.value!
		let heroB0ExpectedDefusedUltDMG = Double(heroB0.mgc) * heroA0ExpectedDefusedDefenseMultiplier * Ability.example_magicStrike.value!
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.getDefenseMultiplier(for: .mgc), heroA0ExpectedDefenseMultiplier)
		XCTAssertEqual(heroB0.getDefenseMultiplier(for: .mgc), heroB0ExpectedDefenseMultiplier)
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroB0.applyDebuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.getDefenseMultiplier(for: .mgc), heroA0ExpectedDefusedDefenseMultiplier)
		XCTAssertEqual(heroB0.getDefenseMultiplier(for: .mgc), heroB0ExpectedDefusedDefenseMultiplier)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0ExpectedDefusedUltDMG.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0ExpectedDefusedUltDMG.rounded()))
	}
	
	func testDamageDealtIncrease_phy_ult() {
		let buff = Ability.example_damageDealtIncrease
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_physicalStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 + statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 + statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.phy) * heroB0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.phy) * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 + statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 + statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testDamageDealtReduction_phy_ult() {
		let debuff = Ability.example_damageDealtReduction
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_physicalStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.phy) * heroB0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.phy) * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroB0.applyDebuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testDamageTakenReduction_phy_ult() {
		let buff = Ability.example_damageTakenReduction
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 120, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_physicalStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.phy) * heroB0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.phy) * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testDamageDealtIncrease_mgc_ult() {
		let buff = Ability.example_damageDealtIncrease
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 180, mgc: 250, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_magicStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 120, magicResist: 160, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 156, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_magicStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 140, magicResist: 180, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 + statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 + statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.mgc) * heroB0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.mgc) * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 + statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 + statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testDamageDealtReduction_mgc_ult() {
		let debuff = Ability.example_damageDealtReduction
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 211, mgc: 120, hp: 1000, haste: 10, armor: 110, magicResist: 250, ulti: Ability.example_magicStrike, level: 10)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 212, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 100, ulti: Ability.example_magicStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.mgc) * heroB0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.mgc) * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyDebuff(newEffect: statusEffect1)
		heroB0.applyDebuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testDamageTakenReduction_mgc_ult() {
		let buff = Ability.example_damageTakenReduction
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 210, magicResist: 145, ulti: Ability.example_magicStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 220, magicResist: 300, ulti: Ability.example_magicStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		let heroA0InfusedBaseAttackDamage = Int((heroA0BaseAttackDamage.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0InfusedBaseAttackDamage = Int((heroB0BaseAttackDamage.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		let heroA0ExpectedUltDMG = Double(heroA0.mgc) * heroB0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroB0ExpectedUltDMG = Double(heroB0.mgc) * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
		let heroA0ExpectedInfusedUltDMG = Int((heroA0ExpectedUltDMG.rounded() * (1.0 - statusEffect1.ultimate.value!)).rounded())
		let heroB0ExpectedInfusedUltDMG = Int((heroB0ExpectedUltDMG.rounded() * (1.0 - statusEffect0.ultimate.value!)).rounded())
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0InfusedBaseAttackDamage)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0InfusedBaseAttackDamage)
		
		heroA0.currentHP = heroA0.hp
		heroB0.currentHP = heroB0.hp
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - heroB0ExpectedInfusedUltDMG)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - heroA0ExpectedInfusedUltDMG)
	}
	
	func testImmune_phy_ult() {
		let buff = Ability.example_immune
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 110, magicResist: 150, ulti: Ability.example_physicalStrike, level: 10)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 130, magicResist: 170, ulti: Ability.example_physicalStrike, level: 20)

		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroA0.currentHaste, 0)
		XCTAssertEqual(heroB0.currentHaste, 0)
	}
	
	func testImmune_mgc_ult() {
		let buff = Ability.example_immune
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 210, magicResist: 145, ulti: Ability.example_magicStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 220, magicResist: 300, ulti: Ability.example_magicStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroA0.currentHaste, 0)
		XCTAssertEqual(heroB0.currentHaste, 0)
	}
	
	func testImmune_lifeSteal_ult() {
		let buff = Ability.example_immune
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 240, hp: 1000, haste: 10, armor: 210, magicResist: 145, ulti: Ability.example_magicLifeSteal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 220, magicResist: 300, ulti: Ability.example_physicalLifeSteal, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroA0.currentHaste, 0)
		XCTAssertEqual(heroB0.currentHaste, 0)
	}
	
	func testImmune_true_ult() {
		let buff = Ability.example_immune
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 240, hp: 1000, haste: 10, armor: 210, magicResist: 145, ulti: Ability.example_trueStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 220, magicResist: 300, ulti: Ability.example_trueStrike, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroA0.currentHaste, 0)
		XCTAssertEqual(heroB0.currentHaste, 0)
	}
	
	func testImmune_execute_ult() {
		let buff = Ability.example_immune
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 10, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 20, casterRandomModifier: 1, caster: nil)
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 240, hp: 1000, haste: 10, armor: 210, magicResist: 145, ulti: Ability.example_execute, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 240, mgc: 275, hp: 1000, haste: 20, armor: 220, magicResist: 300, ulti: Ability.example_execute, level: 20)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroB0BaseAttackDamage = Double(heroB0.phy) * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.applyBuff(newEffect: statusEffect1)
		heroB0.applyBuff(newEffect: statusEffect0)
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, heroA0.hp - Int(heroB0BaseAttackDamage.rounded()))
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 600
		heroB0.currentHaste = 600
		heroA0.currentHP = 50
		heroB0.currentHP = 50
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHP, 50)
		XCTAssertEqual(heroB0.currentHP, 50)
		XCTAssertEqual(heroA0.currentHaste, 0)
		XCTAssertEqual(heroB0.currentHaste, 0)
	}
	
	func testEnrage_doublesDMG() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_enrage, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 220, magicResist: 300, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)

		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		// csak ut mert o maga nem lowos, és nem kap hastet
		heroA0.currentHaste = 610
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 610)
 
		// Nem ut mert castol és nem kap hastet
		heroB0.currentHP = 1000
		heroA0.currentHP = 100
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp)
		
		// duplát ut az enrage miatt
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(1.0 + Ability.example_enrage.value!) * Int(heroA0BaseAttackDamage.rounded()))
		
		heroB0.currentHP = 1000
		heroB0.currentAbsorb = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroB0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
		XCTAssertEqual(heroB1.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp)
		XCTAssertEqual(heroB0.currentAbsorb, 1000 - Int(1.0 + Ability.example_enrage.value!) * Int(heroA0BaseAttackDamage.rounded()))
	}
	
	func testHOTExpiry_afterDeath_0() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 100, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_immune, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 250, mgc: 0, hp: 1000, haste: 50, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		heroA0.currentHaste = 650
		heroA0.currentHP = 1
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 50)
		XCTAssertEqual(heroA0.currentHP, 0)
	}
	
	func testHOTExpiry_afterDeath_1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 100, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_immune, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 250, mgc: 0, hp: 1000, haste: 50, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		heroA0.currentHaste = 650
		heroA0.currentHP = 1
		heroB0.currentPhy = 0
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 1)
		XCTAssertEqual(heroA0.currentHaste, 50)
		
		heroB0.currentPhy = 250
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHP, 0)
	}
	
	func testStun() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_singleStun, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0UltimateDamage = Double(heroA0.phy) * Double(Ability.example_physicalStrike.value!) * heroB0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroB0.currentHP = 1000
		heroB0.currentHaste = 610
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroB0.currentHP, 1000)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroB0.currentHP, 1000)
		
		battle.calculateNextRound(manualNext: true)
		battle.calculateNextRound(manualNext: true)
		battle.calculateNextRound(manualNext: true)
		
		heroA0.currentHaste = 0
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 650
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 50)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0UltimateDamage.rounded()))
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentHaste, 60)
	}
	
	func testAOEStun() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 220, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_aoeStun, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
		
		let heroA0BaseAttackDamage = Double(heroA0.phy) * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
		let heroA0UltimateDamage = Double(heroA0.phy) * Double(Ability.example_physicalStrike.value!) * heroB0.getDefenseMultiplier(for: .phy)

		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA1.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroA1.currentHaste, 30)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroB0.currentHP = 1000
		heroB0.currentHaste = 610
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroA1.currentHaste, 60)
		XCTAssertEqual(heroB0.currentHP, 1000)
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentDebuffs.count, 1)
		XCTAssertEqual(heroA1.currentDebuffs.count, 1)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroA1.currentHaste, 60)
		XCTAssertEqual(heroB0.currentHP, 1000)
		
		battle.calculateNextRound(manualNext: true)
		battle.calculateNextRound(manualNext: true)
		battle.calculateNextRound(manualNext: true)
		
		heroA0.currentHaste = 0
		heroA1.currentHaste = 0
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroA1.currentHaste, 30)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 20)
		XCTAssertEqual(heroA1.currentHaste, 60)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0BaseAttackDamage.rounded()))
		
		heroA0.currentHaste = 650
		heroB0.currentHP = 1000
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentBuffs.count, 0)
		XCTAssertEqual(heroA0.currentHaste, 50)
		XCTAssertEqual(heroB0.currentHP, heroB0.hp - Int(heroA0UltimateDamage.rounded()))
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(heroA0.currentHaste, 60)
	}
	
	func testCalculateRound1_phyDOT_single_absorb() {
		let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
		let heroA0ExpectedTickDMG = 100 * Ability.example_physicalDOT.value!
		let expectedAbsorb1_heroB0 = Int((200 - heroA0ExpectedDMG).rounded())
		let expectedAbsorb2_heroB0 = expectedAbsorb1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP3_heroB0 = 1000 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
		let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())

		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
		
		heroB0.currentAbsorb = 200
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 1)
		XCTAssertEqual(heroB0.currentHP, 1000)
		XCTAssertEqual(heroB0.currentAbsorb, expectedAbsorb1_heroB0)
		XCTAssertEqual(heroA0.currentHaste, 10)
		XCTAssertEqual(heroB0.currentHaste, 20)
		
		heroA0.currentHaste = 650
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 2)
		XCTAssertEqual(heroB0.currentHP, 1000)
		XCTAssertEqual(heroB0.currentAbsorb, expectedAbsorb2_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
		XCTAssertEqual(heroA0.currentHaste, 50)
		XCTAssertEqual(heroB0.currentHaste, 40)
		
		battle.calculateNextRound(manualNext: true)
		battle.calculateNextRound(manualNext: true)
		
		/*
		battle.calculateNextRound(manualNext: true)
		XCTAssertEqual(battle.round, 3)
		XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
		XCTAssertEqual(heroB0.currentDebuffs.count, 1)
		XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
		XCTAssertEqual(heroA0.currentHaste, 60)
		 */
	}
}
