//
//  GetHealthChangeValueTests.swift
//  NFTWarsSimTests
//
//  Created by Zengo on 2023. 02. 17..
//

import XCTest
import NFTWarsGamePackage

final class GetHealthChangeValueTests: XCTestCase {

    override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
    }
    
    func testGetHealthChangeValue_PhyDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)

        let expectedDamage1 = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(13 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalStrike.value!).rounded())
        let expectedDamage3 = 0
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_PhyDamage2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)

        let expectedDamage1 = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_whirlwind.value!).rounded())
        let expectedDamage2 = -Int((Double(13 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_whirlwind.value!).rounded())
        let expectedDamage3 = 0
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_magicStrike, level: 0)

        let expectedDamage1 = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(13 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicStrike.value!).rounded())
        let expectedDamage3 = 0
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcDamage2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_tornado, level: 0)

        let expectedDamage1 = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_tornado.value!).rounded())
        let expectedDamage2 = -Int((Double(13 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_tornado.value!).rounded())
        let expectedDamage3 = 0
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcLifeSteal1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 100, ulti: Ability.example_magicLifeSteal, level: 0)

        let expectedLifeSteal1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicLifeSteal.value!).rounded())
        let expectedLifeSteal2 = Int((Double(13 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicLifeSteal.value!).rounded())
        let expectedLifeSteal3 = 0
        
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, expectedLifeSteal1)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        
        heroA0.currentMgc = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.lifeSteal, expectedLifeSteal2)
        XCTAssertEqual(hpChangeValue2.damage, 0)
        XCTAssertEqual(hpChangeValue2.heal, 0)
        
        heroA0.currentMgc = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.lifeSteal, expectedLifeSteal3)
        XCTAssertEqual(hpChangeValue3.damage, 0)
        XCTAssertEqual(hpChangeValue3.heal, 0)
    }
    
    func testGetHealthChangeValue_phyLifeSteal1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalLifeSteal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_physicalLifeSteal, level: 0)

        let expectedLifeSteal1 = Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalLifeSteal.value!).rounded())
        let expectedLifeSteal2 = Int((Double(13 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalLifeSteal.value!).rounded())
        let expectedLifeSteal3 = 0
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, expectedLifeSteal1)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.lifeSteal, expectedLifeSteal2)
        XCTAssertEqual(hpChangeValue2.damage, 0)
        XCTAssertEqual(hpChangeValue2.heal, 0)
        
        heroA0.currentPhy = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.lifeSteal, expectedLifeSteal3)
        XCTAssertEqual(hpChangeValue3.damage, 0)
        XCTAssertEqual(hpChangeValue3.heal, 0)
    }
    
    func testGetHealthChangeValue_MgcPhyDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_mgcPhyStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 50, magicResist: 100, ulti: Ability.example_mgcPhyStrike, level: 0)
        
        let expectedMgcDmg = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .mgc)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedPhyDmg = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedDamage1 = expectedPhyDmg + expectedMgcDmg
        let expectedDamage2 = expectedMgcDmg + -Int((Double(13 * heroB0.getDefenseMultiplier(for: .phy)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedDamage3 = expectedPhyDmg + -Int((Double(13 * heroB0.getDefenseMultiplier(for: .mgc)) * (Ability.example_mgcPhyStrike.value!/2)).rounded())
        let expectedDamage4 = 0
        

        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 100
        heroA0.currentMgc = 13
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 0
        heroA0.currentMgc = 0
        let hpChangeValue4 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue4.damage, expectedDamage4)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcPhyDamage2() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)

        let expectedDamage1 = -Int((Double(100 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_whirlwind.value!).rounded())
        let expectedDamage2 = -Int((Double(13 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_whirlwind.value!).rounded())
        let expectedDamage3 = 0
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcHeal1() {
        let expectedDamage1 = Int((Double(100) * Ability.example_heal.value!).rounded())
        let expectedDamage2 = Int((Double(25) * Ability.example_heal.value!).rounded())
        let expectedDamage3 = 0
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 100, ulti: Ability.example_heal, level: 0)

        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.heal, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 25
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.heal, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.heal, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcHeal2() {
        let expectedDamage1 = Int((Double(100) * Ability.example_AOEHeal.value!).rounded())
        let expectedDamage2 = Int((Double(25) * Ability.example_AOEHeal.value!).rounded())
        let expectedDamage3 = 0
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 100, ulti: Ability.example_AOEHeal, level: 0)

        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.heal, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 25
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.heal, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentMgc = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.heal, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.damage, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_TrueDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 1)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 100, ulti: Ability.example_trueStrike, level: 1)

        let expectedDamage1 = -Int(Ability.example_trueStrike.value!)
        let expectedDamage2 = -Int(Ability.example_trueStrike.value!)
        let expectedDamage3 = -Int(Ability.example_trueStrike.value!)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 13
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroA0.currentPhy = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_PhyMaxHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 150, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(1000 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMaxHPStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(1000 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMaxHPStrike.value!).rounded())
        let expectedDamage3 = -Int((Double(1000 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMaxHPStrike.value!).rounded())
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 500
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MagicMaxHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 100, magicResist: 150, ulti: Ability.example_magicMaxHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(900 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMaxHPStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(900 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMaxHPStrike.value!).rounded())
        let expectedDamage3 = -Int((Double(900 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMaxHPStrike.value!).rounded())
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 500
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_PhyCurrHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(850 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalCurrentHPStrike.value!).rounded())
        let expectedDamage2 = 0
        let expectedDamage3 = 0
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        heroB0.currentHP = 850
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MagicCurrHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 900, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_magicCurrentHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(800 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicCurrentHPStrike.value!).rounded())
        let expectedDamage2 = 0
        let expectedDamage3 = 0
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        heroB0.currentHP = 800
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 0
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_PhyMissHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(150 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMissingHPStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(999 * heroB0.getDefenseMultiplier(for: .phy)) * Ability.example_physicalMissingHPStrike.value!).rounded())
        let expectedDamage3 = 0
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        heroB0.currentHP = 850
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1000
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
    func testGetHealthChangeValue_MgcMissHPDamage1() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)

        let expectedDamage1 = -Int((Double(450 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMissingHPStrike.value!).rounded())
        let expectedDamage2 = -Int((Double(999 * heroB0.getDefenseMultiplier(for: .mgc)) * Ability.example_magicMissingHPStrike.value!).rounded())
        let expectedDamage3 = 0
        let battle = Battle(teamA: [heroA0], teamB: [heroB0])
        
        heroB0.currentHP = 550
        let hpChangeValue1 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue1.damage, expectedDamage1)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1
        let hpChangeValue2 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue2.damage, expectedDamage2)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
        
        heroB0.currentHP = 1001
        let hpChangeValue3 = battle.getHealthChangeValue(caster: heroA0, target: heroB0)
        XCTAssertEqual(hpChangeValue3.damage, expectedDamage3)
        XCTAssertEqual(hpChangeValue1.heal, 0)
        XCTAssertEqual(hpChangeValue1.lifeSteal, 0)
    }
    
}
