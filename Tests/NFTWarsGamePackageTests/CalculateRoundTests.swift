//
//  CalculateRoundTests.swift
//  NFTWarsSimTests
//
//  Created by Zengo on 2023. 02. 25..
//

import XCTest
import NFTWarsGamePackage

final class CalculateRoundTests: XCTestCase {
    
    override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
    }
    
    func testCalculateRound1_phy_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltDMG = 100 * Ability.example_physicalStrike.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltDMG = 200 * Ability.example_physicalStrike.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_phy_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 200, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalStrikeMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalStrikeMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgc_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltDMG = 200 * Ability.example_magicStrike.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltDMG = 400 * Ability.example_magicStrike.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_mgc_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 200, magicResist: 200, ulti: Ability.example_magicStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 100, magicResist: 150, ulti: Ability.example_magicStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((200.0 * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicStrikeMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((400.0 * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicStrikeMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_phy_aoe() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 200, magicResist: 200, ulti: Ability.example_whirlwind, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 150, magicResist: 150, ulti: Ability.example_whirlwind, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_whirlwind, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_whirlwindMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_whirlwindMultiplier).rounded())
        let heroA1ExpectedHP = 1000 - Int((200 * Constants.example_tornadoMultiplier).rounded())
        let heroB1ExpectedHP = 1000 - Int((100 * Constants.example_tornadoMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        XCTAssertEqual(heroB1.currentHP, heroB1ExpectedHP)
        XCTAssertEqual(heroA1.currentHP, 1000)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHP, heroB1ExpectedHP)
        XCTAssertEqual(heroA1.currentHP, heroA1ExpectedHP)
    }
    
    func testCalculateRound1_mgc_aoe() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 200, magicResist: 200, ulti: Ability.example_tornado, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 100, magicResist: 150, ulti: Ability.example_tornado, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_tornado, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((200.0 * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_tornadoMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((400.0 * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_tornadoMultiplier).rounded())
        let heroA1ExpectedHP = 1000 - Int((400 * Constants.example_tornadoMultiplier).rounded())
        let heroB1ExpectedHP = 1000 - Int((200 * Constants.example_tornadoMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        XCTAssertEqual(heroB1.currentHP, heroB1ExpectedHP)
        XCTAssertEqual(heroA1.currentHP, 1000)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHP, heroB1ExpectedHP)
        XCTAssertEqual(heroA1.currentHP, heroA1ExpectedHP)
    }
    
    func testCalculateRound1_lifeSteal_mgc_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 200, magicResist: 200, ulti: Ability.example_magicLifeSteal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 100, magicResist: 150, ulti: Ability.example_magicLifeSteal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicLifeSteal, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltLifeSteal = Int(((200.0 * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicLifeStealMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltLifeSteal = Int(((400.0 * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicLifeStealMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        
        // Előbb B0 base attack, utána A0 life steal ult fullre
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltLifeSteal
        let rawExpectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG + heroA0ExpectedUltLifeSteal
        let expectedHP4_heroA0 = rawExpectedHP4_heroA0 > 1000 ? 1000 : rawExpectedHP4_heroA0
        
        // Előbb a B0 life steal ult aztán A0 baseAttack
        let rawExpectedHP5_heroB0 = expectedHP4_heroB0 + heroB0ExpectedUltLifeSteal
        let expectedHP5_heroB0 = (rawExpectedHP5_heroB0 > 1000 ? 1000 : rawExpectedHP5_heroB0) - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltLifeSteal
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_lifeSteal_phy_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 200, magicResist: 200, ulti: Ability.example_physicalLifeSteal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalLifeSteal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 100, magicResist: 150, ulti: Ability.example_physicalLifeSteal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalLifeSteal, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltLifeSteal = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalLifeStealMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltLifeSteal = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalLifeStealMultiplier).rounded())
        let expectedHP1_heroB0 = 1000 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        
        // Előbb B0 base attack, utána A0 life steal ult fullre
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltLifeSteal
        let rawExpectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG + heroA0ExpectedUltLifeSteal
        let expectedHP4_heroA0 = rawExpectedHP4_heroA0 > 1000 ? 1000 : rawExpectedHP4_heroA0
        
        // Előbb a B0 life steal ult aztán A0 baseAttack
        let rawExpectedHP5_heroB0 = expectedHP4_heroB0 + heroB0ExpectedUltLifeSteal
        let expectedHP5_heroB0 = (rawExpectedHP5_heroB0 > 1000 ? 1000 : rawExpectedHP5_heroB0) - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltLifeSteal
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgc_heal_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltHeal = 200 * Ability.example_heal.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltHeal = 400 * Ability.example_heal.value!
        let expectedHP1_heroB0 = Int((500 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((500 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedUltHeal.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedUltHeal.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        heroA0.currentHP = 500
        heroB0.currentHP = 500
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgc_heal_aoe() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltHeal = 200 * Ability.example_AOEHeal.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltHeal = 400 * Ability.example_AOEHeal.value!
        let expectedHP1_heroB0 = Int((500 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((500 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedUltHeal.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedUltHeal.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0
        
        let expectedHP6_heroA1 = 500 + Int(heroA0ExpectedUltHeal.rounded())
        let expectedHP6_heroB1 = 500 + Int(heroB0ExpectedUltHeal.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 200, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 400, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
        
        heroA0.currentHP = 500
        heroA1.currentHP = 500
        heroB0.currentHP = 500
        heroB1.currentHP = 500
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
        
        XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
        XCTAssertEqual(heroB1.currentHP, expectedHP6_heroB1)
    }
    
    func testCalculateRound1_true_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltDMG = Ability.example_trueStrike.value
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltDMG = Ability.example_trueStrike.value
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG!.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG!.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_trueStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_phyMaxHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltDMG = 900 * Ability.example_physicalMaxHPStrike.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltDMG = 1000 * Ability.example_physicalMaxHPStrike.value!
        let expectedHP1_heroB0 = Int((900 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 900, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_phyMaxHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 200, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 900, haste: 20, armor: 100, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMaxHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((900.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalMaxHPStrikeMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((1000.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalMaxHPStrikeMultiplier).rounded())
        let expectedHP1_heroB0 = 900 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1000 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgcMaxHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedUltDMG = 800 * Ability.example_magicMaxHPStrike.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedUltDMG = 1100 * Ability.example_magicMaxHPStrike.value!
        let expectedHP1_heroB0 = Int((800 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1100 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_mgcMaxHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 200, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 100, ulti: Ability.example_magicMaxHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMaxHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroA0ExpectedUltDMG = Int(((800.0 * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicMaxHPStrikeMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedUltDMG = Int(((1100.0 * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicMaxHPStrikeMultiplier).rounded())
        let expectedHP1_heroB0 = 800 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1100 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgcCurrentHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let expectedHP1_heroB0 = Int((800 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1100 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let heroA0ExpectedUltDMG = Double(expectedHP3_heroB0) * Ability.example_magicCurrentHPStrike.value!
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let heroB0ExpectedUltDMG = Double(expectedHP4_heroA0) * Ability.example_magicCurrentHPStrike.value!
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_mgcCurrentHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 200, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 100, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicCurrentHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1_heroB0 = 800 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1100 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let heroA0ExpectedUltDMG = Int(((Double(expectedHP3_heroB0) * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicCurrentHPStrikeMultiplier).rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let heroB0ExpectedUltDMG = Int(((Double(expectedHP4_heroA0) * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicCurrentHPStrikeMultiplier).rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_phyCurrentHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let expectedHP1_heroB0 = Int((800 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1100 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let heroA0ExpectedUltDMG = Double(expectedHP3_heroB0) * Ability.example_physicalCurrentHPStrike.value!
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let heroB0ExpectedUltDMG = Double(expectedHP4_heroA0) * Ability.example_physicalCurrentHPStrike.value!
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_phyCurrentHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 200, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 100, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalCurrentHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1_heroB0 = 800 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1100 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let heroA0ExpectedUltDMG = Int(((Double(expectedHP3_heroB0) * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalCurrentHPStrikeMultiplier).rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let heroB0ExpectedUltDMG = Int(((Double(expectedHP4_heroA0) * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalCurrentHPStrikeMultiplier).rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_phyMissingHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let expectedHP1_heroB0 = Int((800 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1100 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let heroA0ExpectedUltDMG = Double(800 - expectedHP3_heroB0) * Ability.example_physicalMissingHPStrike.value!
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let heroB0ExpectedUltDMG = Double(1100 - expectedHP4_heroA0) * Ability.example_physicalMissingHPStrike.value!
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_phyMissingHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 200, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 100, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalMissingHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1_heroB0 = 800 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1100 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let heroA0ExpectedUltDMG = Int(((Double(800 - expectedHP3_heroB0) * heroB0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalMissingHPStrikeMultiplier).rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let heroB0ExpectedUltDMG = Int(((Double(1100 - expectedHP4_heroA0) * heroA0.getDefenseMultiplier(for: .phy)) * Constants.example_physicalMissingHPStrikeMultiplier).rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_mgcMissingHP_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let expectedHP1_heroB0 = Int((800 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1100 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let heroA0ExpectedUltDMG = Double(800 - expectedHP3_heroB0) * Ability.example_magicMissingHPStrike.value!
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedUltDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let heroB0ExpectedUltDMG = Double(1100 - expectedHP4_heroA0) * Ability.example_magicMissingHPStrike.value!
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedUltDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound2_mgcMissingHP_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1100, haste: 10, armor: 0, magicResist: 200, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 800, haste: 20, armor: 0, magicResist: 100, ulti: Ability.example_magicMissingHPStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_magicMissingHPStrike, level: 0)
        
        let heroA0ExpectedDMG = Int(((100.0 * heroB0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let heroB0ExpectedDMG = Int(((200.0 * heroA0.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1_heroB0 = 800 - heroA0ExpectedDMG
        let expectedHP1_heroA0 = 1100 - heroB0ExpectedDMG
        let expectedHP2_heroB0 = expectedHP1_heroB0 - heroA0ExpectedDMG
        let expectedHP2_heroA0 = expectedHP1_heroA0 - heroB0ExpectedDMG
        let expectedHP3_heroB0 = expectedHP2_heroB0 - heroA0ExpectedDMG
        let expectedHP3_heroA0 = expectedHP2_heroA0 - heroB0ExpectedDMG
        let heroA0ExpectedUltDMG = Int(((Double(800 - expectedHP3_heroB0) * heroB0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicMissingHPStrikeMultiplier).rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - heroA0ExpectedUltDMG
        let expectedHP4_heroA0 = expectedHP3_heroA0 - heroB0ExpectedDMG
        let expectedHP5_heroB0 = expectedHP4_heroB0 - heroA0ExpectedDMG
        let heroB0ExpectedUltDMG = Int(((Double(1100 - expectedHP4_heroA0) * heroA0.getDefenseMultiplier(for: .mgc)) * Constants.example_magicMissingHPStrikeMultiplier).rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - heroB0ExpectedUltDMG
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
        XCTAssertEqual(heroB0.currentHaste, 0)
    }
    
    func testCalculateRound1_execute_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = 0
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP5_heroB1 = 1000 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_execute, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 800
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 800)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 800)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 800)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHP = 200
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 200)
        XCTAssertEqual(heroB0.currentHaste, 80)
        
        heroB0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB1.currentHP, expectedHP5_heroB1)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 210)
    }
    
    func testCalculateRound1_phyDOT_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickDMG = 100 * Ability.example_physicalDOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickDMG = 200 * Ability.example_physicalDOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 80)
        XCTAssertEqual(heroB0.currentHaste, 70)
    }
    
    func testCalculateRound2_phyDOT_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: Ability.example_physicalDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedTickDMG = 100 * Ability.example_physicalDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedTickDMG = 200 * Ability.example_physicalDOT.value! * heroA0.getDefenseMultiplier(for: .phy)
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 80)
        XCTAssertEqual(heroB0.currentHaste, 70)
    }
    
    func testCalculateRound1_mgcDOT_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickDMG = 150 * Ability.example_magicalDOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickDMG = 220 * Ability.example_magicalDOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 150, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicalDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 220, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicalDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 80)
        XCTAssertEqual(heroB0.currentHaste, 70)
    }
    
    func testCalculateRound2_mgcDOT_single() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 250, hp: 1000, haste: 10, armor: 120, magicResist: 162, ulti: Ability.example_magicalDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 300, hp: 1000, haste: 20, armor: 123, magicResist: 210, ulti: Ability.example_magicalDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedTickDMG = 250 * Ability.example_magicalDOT.value! * heroB0.getDefenseMultiplier(for: .mgc)
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedTickDMG = 300 * Ability.example_magicalDOT.value! * heroA0.getDefenseMultiplier(for: .mgc)
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 80)
        XCTAssertEqual(heroB0.currentHaste, 70)
    }
    
    func testCalculateRound1_mgcHOT_single() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickHeal = 160 * Ability.example_magicalHOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickHeal = 230 * Ability.example_magicalHOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedTickHeal.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedTickHeal.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 160, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 230, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicalHOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroB0.currentHaste, 40)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 60)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 3)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        // Ha 4 a spell duration csak akkor fogy el a A0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 2)
        XCTAssertEqual(heroA0.currentHaste, 80)
        XCTAssertEqual(heroB0.currentHaste, 70)
    }
    
    func testCalculateRound1_phyDOT_AOE() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickDMG = 100 * Ability.example_physicalAOEDOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickDMG = 200 * Ability.example_physicalAOEDOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let expectedHP1_heroA1 = 1000
        let expectedHP1_heroB1 = 1000
        let expectedHP2_heroA1 = expectedHP1_heroA1
        let expectedHP2_heroB1 = expectedHP1_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA1 = expectedHP2_heroA1
        let expectedHP3_heroB1 = expectedHP2_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA1 = expectedHP3_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP4_heroB1 = expectedHP3_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA1 = expectedHP4_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB1 = expectedHP4_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP6_heroA1 = expectedHP5_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP6_heroB1 = expectedHP5_heroB1
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalAOEDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalAOEDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroB1.currentHP, expectedHP1_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP1_heroA1)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP2_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP2_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP3_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP3_heroA1)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP4_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP4_heroA1)

        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP5_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP5_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentHP, expectedHP6_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
    }
    
    func testCalculateRound2_phyDOT_AOE() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 120, magicResist: 0, ulti: Ability.example_physicalAOEDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 150, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 160, magicResist: 0, ulti: Ability.example_physicalAOEDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 180, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedTickDMGForB0 = 100 * Ability.example_physicalAOEDOT.value! * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedTickDMGForB1 = 100 * Ability.example_physicalAOEDOT.value! * heroB1.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedTickDMGForA0 = 200 * Ability.example_physicalAOEDOT.value! * heroA0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedTickDMGForA1 = 200 * Ability.example_physicalAOEDOT.value! * heroA1.getDefenseMultiplier(for: .phy)
        
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMGForA0.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMGForA0.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMGForA0.rounded())
        
        let expectedHP1_heroA1 = 1000
        let expectedHP1_heroB1 = 1000
        let expectedHP2_heroA1 = expectedHP1_heroA1
        let expectedHP2_heroB1 = expectedHP1_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP3_heroA1 = expectedHP2_heroA1
        let expectedHP3_heroB1 = expectedHP2_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP4_heroA1 = expectedHP3_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP4_heroB1 = expectedHP3_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP5_heroA1 = expectedHP4_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP5_heroB1 = expectedHP4_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP6_heroA1 = expectedHP5_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP6_heroB1 = expectedHP5_heroB1
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroB1.currentHP, expectedHP1_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP1_heroA1)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP2_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP2_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP3_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP3_heroA1)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP4_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP4_heroA1)

        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP5_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP5_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentHP, expectedHP6_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
    }
    
    func testCalculateRound1_mgcDOT_AOE() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickDMG = 175 * Ability.example_magicalAOEDOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickDMG = 190 * Ability.example_magicalAOEDOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMG.rounded())
        
        let expectedHP1_heroA1 = 1000
        let expectedHP1_heroB1 = 1000
        let expectedHP2_heroA1 = expectedHP1_heroA1
        let expectedHP2_heroB1 = expectedHP1_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP3_heroA1 = expectedHP2_heroA1
        let expectedHP3_heroB1 = expectedHP2_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP4_heroA1 = expectedHP3_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP4_heroB1 = expectedHP3_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP5_heroA1 = expectedHP4_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP5_heroB1 = expectedHP4_heroB1 - Int(heroA0ExpectedTickDMG.rounded())
        let expectedHP6_heroA1 = expectedHP5_heroA1 - Int(heroB0ExpectedTickDMG.rounded())
        let expectedHP6_heroB1 = expectedHP5_heroB1
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 175, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 190, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroB1.currentHP, expectedHP1_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP1_heroA1)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP2_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP2_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP3_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP3_heroA1)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP4_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP4_heroA1)

        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP5_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP5_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentHP, expectedHP6_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
    }
    
    func testCalculateRound2_mgcDOT_AOE() {
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 250, hp: 1000, haste: 10, armor: 0, magicResist: 150, ulti: Ability.example_magicalAOEDOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 175, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 210, hp: 1000, haste: 20, armor: 0, magicResist: 189, ulti: Ability.example_magicalAOEDOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 200, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedTickDMGForB0 = 250 * Ability.example_magicalAOEDOT.value! * heroB0.getDefenseMultiplier(for: .mgc)
        let heroA0ExpectedTickDMGForB1 = 250 * Ability.example_magicalAOEDOT.value! * heroB1.getDefenseMultiplier(for: .mgc)
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedTickDMGForA0 = 210 * Ability.example_magicalAOEDOT.value! * heroA0.getDefenseMultiplier(for: .mgc)
        let heroB0ExpectedTickDMGForA1 = 210 * Ability.example_magicalAOEDOT.value! * heroA1.getDefenseMultiplier(for: .mgc)
        
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 - Int(heroB0ExpectedTickDMGForA0.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) - Int(heroA0ExpectedTickDMGForB0.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMGForA0.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedDMG.rounded()) - Int(heroB0ExpectedTickDMGForA0.rounded())
        
        let expectedHP1_heroA1 = 1000
        let expectedHP1_heroB1 = 1000
        let expectedHP2_heroA1 = expectedHP1_heroA1
        let expectedHP2_heroB1 = expectedHP1_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP3_heroA1 = expectedHP2_heroA1
        let expectedHP3_heroB1 = expectedHP2_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP4_heroA1 = expectedHP3_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP4_heroB1 = expectedHP3_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP5_heroA1 = expectedHP4_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP5_heroB1 = expectedHP4_heroB1 - Int(heroA0ExpectedTickDMGForB1.rounded())
        let expectedHP6_heroA1 = expectedHP5_heroA1 - Int(heroB0ExpectedTickDMGForA1.rounded())
        let expectedHP6_heroB1 = expectedHP5_heroB1
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroB1.currentHP, expectedHP1_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP1_heroA1)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP2_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP2_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP3_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP3_heroA1)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 1)
        XCTAssertEqual(heroB1.currentHP, expectedHP4_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP4_heroA1)

        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 2)
        XCTAssertEqual(heroB1.currentHP, expectedHP5_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP5_heroA1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroA1.currentDebuffs[0].remainingDuration, Ability.example_magicalAOEDOT.duration! - 3)
        XCTAssertEqual(heroB1.currentHP, expectedHP6_heroB1)
        XCTAssertEqual(heroA1.currentHP, expectedHP6_heroA1)
    }
    
    func testCalculateRound1_mgcHOT_AOE() {
        let heroA0ExpectedDMG = 100 * Constants.baseAttackMultiplier
        let heroA0ExpectedTickHeal = 160 * Ability.example_magicalAOEHOT.value!
        let heroB0ExpectedDMG = 200 * Constants.baseAttackMultiplier
        let heroB0ExpectedTickHeal = 230 * Ability.example_magicalAOEHOT.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedTickHeal.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0 + Int(heroA0ExpectedTickHeal.rounded())
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDMG.rounded()) + Int(heroB0ExpectedTickHeal.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedDMG.rounded()) + Int(heroA0ExpectedTickHeal.rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 160, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 230, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_magicalAOEHOT, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroB0.currentHaste, 20)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 3)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a A0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 2)
    }
    
    func testCalculateRound_phyPowerUp_single_1() {
        let buff = Ability.example_phyPowerUp
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0InfusedPhy = Double(100 + Int(buff.value!))
        let heroB0InfusedPhy = Double(200 + Int(buff.value!))
        let heroA0ExpectedBaseDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedBaseDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedInfusedDMG = heroA0InfusedPhy * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedInfusedDMG = heroB0InfusedPhy * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedBaseDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedBaseDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedInfusedDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedInfusedDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedInfusedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedInfusedDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedInfusedDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        
        heroA0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 3)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
    }
    
    func testCalculateRound_phyPowerUp_single_2() {
        let buff = Ability.example_phyPowerUp
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 0, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0InfusedPhy = Double(100 + Int(buff.value!))
        let heroB0InfusedPhy = Double(200 + Int(buff.value!))
        let heroA0ExpectedBaseDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedBaseDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedInfusedDMG = heroA0InfusedPhy * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedInfusedDMG = heroB0InfusedPhy * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedInfusedUltDMG = heroA0InfusedPhy * heroB0.getDefenseMultiplier(for: .phy) * Constants.example_physicalStrikeMultiplier
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedBaseDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedBaseDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedInfusedUltDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedInfusedDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedInfusedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedInfusedDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedInfusedDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        
        heroA1.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 3)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, Ability.example_physicalDOT.duration! - 3)
    }
    
    func testCalculateRound_mgcPowerUp_single_1() {
        let buff = Ability.example_mgcPowerUp
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 312, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0InfusedMgc = Double(312 + Int(buff.value!))
        let heroA0ExpectedBaseDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedBaseDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedInfusedUltDMG = heroA0InfusedMgc * heroB0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedBaseDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedBaseDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedInfusedUltDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        
        heroA1.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, buff.duration! - 3)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, buff.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, buff.duration! - 2)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs[0].remainingDuration, buff.duration! - 3)
    }
    
    func testCalculateRound_phyPowerDown_single_2() {
        let buff = Ability.example_phyPowerDown
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 300, mgc: 0, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 250, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        
        let heroA0DefusedPhy = Double(300 - Int(buff.value!))
        let heroA0ExpectedBaseDMG = 300 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedBaseDMG = 250 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedDefusedDMG = heroA0DefusedPhy * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedDefusedUltDMG = heroA0DefusedPhy * heroB0.getDefenseMultiplier(for: .phy) * Constants.example_physicalStrikeMultiplier
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedBaseDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedBaseDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDefusedUltDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedDefusedDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedDefusedDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedDefusedDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        
        heroB1.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 1)
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 2)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
    }
    
    func testCalculateRound_mgcPowerDown_single_1() {
        let buff = Ability.example_mgcPowerDown
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 100, mgc: 312, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_magicStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 200, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroA0DefusedMgc = Double(312 - Int(buff.value!))
        let heroA0ExpectedBaseDMG = 100 * Constants.baseAttackMultiplier * heroB0.getDefenseMultiplier(for: .phy)
        let heroB0ExpectedBaseDMG = 200 * Constants.baseAttackMultiplier * heroA0.getDefenseMultiplier(for: .phy)
        let heroA0ExpectedDefusedUltDMG = heroA0DefusedMgc * heroB0.getDefenseMultiplier(for: .mgc) * Ability.example_magicStrike.value!
        let expectedHP1_heroB0 = Int((1000 - heroA0ExpectedBaseDMG).rounded())
        let expectedHP1_heroA0 = Int((1000 - heroB0ExpectedBaseDMG).rounded())
        let expectedHP2_heroB0 = expectedHP1_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP2_heroA0 = expectedHP1_heroA0
        let expectedHP3_heroB0 = expectedHP2_heroB0 - Int(heroA0ExpectedDefusedUltDMG.rounded())
        let expectedHP3_heroA0 = expectedHP2_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP4_heroB0 = expectedHP3_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP4_heroA0 = expectedHP3_heroA0
        let expectedHP5_heroB0 = expectedHP4_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP5_heroA0 = expectedHP4_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        let expectedHP6_heroB0 = expectedHP5_heroB0 - Int(heroA0ExpectedBaseDMG.rounded())
        let expectedHP6_heroA0 = expectedHP5_heroA0 - Int(heroB0ExpectedBaseDMG.rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 1)
        XCTAssertEqual(heroB0.currentHP, expectedHP1_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP1_heroA0)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 2)
        XCTAssertEqual(heroB0.currentHP, expectedHP2_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP2_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 1)
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 3)
        XCTAssertEqual(heroB0.currentHP, expectedHP3_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP3_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 2)
        
        heroB0.currentHaste = 650
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 4)
        XCTAssertEqual(heroB0.currentHP, expectedHP4_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP4_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 1)
        
        // Ha 4 a spell duration csak akkor fogy el a B0-n a DOT
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 5)
        XCTAssertEqual(heroB0.currentHP, expectedHP5_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP5_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 2)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(battle.round, 6)
        XCTAssertEqual(heroB0.currentHP, expectedHP6_heroB0)
        XCTAssertEqual(heroA0.currentHP, expectedHP6_heroA0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, buff.duration! - 3)
    }
    
    func testCalculateRound_powerDown_powerUp_phy_1() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_phy_2() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!) - Int(debuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_phy_3() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 50, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 50 + Int(buff.value!) - Int(debuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 50)
    }
    
    func testCalculateRound_powerDown_powerUp_phy_4() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 250, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 250 + Int(buff.value!) - Int(debuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
    }
    
    func testCalculateRound_powerDown_powerUp_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_mgc_2() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_mgc_3() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 50, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 50 - 50 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 50)
    }
    
    func testCalculateRound_powerDown_powerUp_mgc_4() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 250, hp: 1000, haste: 10, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 120, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 250 + Int(buff.value!) - Int(debuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 250)
    }
    
    func testCalculateRound_powerUpAOE_phy_1() {
        let buff = Ability.example_phyPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentPhy, 0 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA1.currentPhy, 0)
    }
    
    func testCalculateRound_powerUpAOE_phy_2() {
        let buff = Ability.example_phyPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 112, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 357, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 112 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentPhy, 357 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 112)
        XCTAssertEqual(heroA1.currentPhy, 357)
    }
    
    func testCalculateRound_powerUpAOE_mgc_1() {
        let buff = Ability.example_mgcPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentMgc, 0 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA1.currentMgc, 0)
    }
    
    func testCalculateRound_powerUpAOE_mgc_2() {
        let buff = Ability.example_mgcPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 112, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 357, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 112 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentMgc, 357 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 112)
        XCTAssertEqual(heroA1.currentMgc, 357)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_phy_1() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeBuff = Ability.example_phyPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!) + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_phy_2() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeBuff = Ability.example_phyPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!) - Int(debuff.value!) + Int(aoeBuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_phy_3() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeBuff = Ability.example_phyPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 50, mgc: 0, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 72, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 50 + Int(buff.value!) + Int(aoeBuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentPhy, 72 + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 50)
        XCTAssertEqual(heroA1.currentPhy, 72)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_phy_4() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeBuff = Ability.example_phyPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 250, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 300, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250 + Int(buff.value!) + Int(aoeBuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentPhy, 300 + Int(aoeBuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
        XCTAssertEqual(heroA1.currentPhy, 300)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeBuff = Ability.example_mgcPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!) + Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_mgc_2() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeBuff = Ability.example_mgcPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 100, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 100 + Int(buff.value!) - Int(debuff.value!) + Int(aoeBuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 100)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_mgc_3() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeBuff = Ability.example_mgcPowerUpAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 50, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 82, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 50 + Int(buff.value!) + Int(aoeBuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentMgc, 82 + Int(aoeBuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 50)
        XCTAssertEqual(heroA1.currentMgc, 82)
    }
    
    func testCalculateRound_powerDown_powerUp_powerUpAOE_mgc_4() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeBuff = Ability.example_mgcPowerUpAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 270, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeBuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 320, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 2)
        XCTAssertEqual(heroA1.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 270 + Int(buff.value!) + Int(aoeBuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentMgc, 320 + Int(aoeBuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 270)
        XCTAssertEqual(heroA1.currentMgc, 320)
    }
    
    func testCalculateRound_powerDownAOE_phy_1() {
        let debuff = Ability.example_phyPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
    }
    
    func testCalculateRound_powerDownAOE_phy_2() {
        let buff = Ability.example_phyPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 112, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 357, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 112 - Int(buff.value!))
        XCTAssertEqual(heroB1.currentPhy, 357 - Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 112)
        XCTAssertEqual(heroB1.currentPhy, 357)
    }
    
    func testCalculateRound_powerDownAOE_mgc_1() {
        let debuff = Ability.example_mgcPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
    }
    
    func testCalculateRound_powerDownAOE_mgc_2() {
        let buff = Ability.example_mgcPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 332, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 357, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 332 - Int(buff.value!))
        XCTAssertEqual(heroB1.currentMgc, 357 - Int(buff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentMgc, 332)
        XCTAssertEqual(heroB1.currentMgc, 357)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_phy_1() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeDebuff = Ability.example_phyPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentPhy, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA1.currentPhy, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_phy_2() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeDebuff = Ability.example_phyPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(aoeDebuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA1.currentPhy, 0)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_phy_3() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeDebuff = Ability.example_phyPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 50, mgc: 0, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 72, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 50 + Int(buff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentPhy, 72)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 50)
        XCTAssertEqual(heroA1.currentPhy, 72)
        XCTAssertEqual(heroB0.currentPhy, 0)
        XCTAssertEqual(heroB1.currentPhy, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_phy_4() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeDebuff = Ability.example_phyPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 250, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 300, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 350, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 400, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 2)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
        XCTAssertEqual(heroA1.currentPhy, 300)
        XCTAssertEqual(heroB0.currentPhy, 350 + Int(buff.value!) - Int(aoeDebuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroB1.currentPhy, 400 - Int(aoeDebuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
        XCTAssertEqual(heroA1.currentPhy, 300)
        XCTAssertEqual(heroB0.currentPhy, 350)
        XCTAssertEqual(heroB1.currentPhy, 400)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_phy_5() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
        let aoeDebuff = Ability.example_phyPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 250, mgc: 0, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 300, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 350, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 400, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 2)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
        XCTAssertEqual(heroA1.currentPhy, 300)
        XCTAssertEqual(heroB0.currentPhy, 350 + Int(buff.value!) - Int(aoeDebuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroB1.currentPhy, 400 - Int(aoeDebuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentPhy, 250)
        XCTAssertEqual(heroA1.currentPhy, 300)
        XCTAssertEqual(heroB0.currentPhy, 350)
        XCTAssertEqual(heroB1.currentPhy, 400)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeDebuff = Ability.example_mgcPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        XCTAssertEqual(heroA1.currentMgc, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA1.currentMgc, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_mgc_2() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeDebuff = Ability.example_mgcPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA1.currentHaste = 600
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA1.currentMgc, 0)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_mgc_3() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeDebuff = Ability.example_mgcPowerDownAOE

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 50, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 72, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 50 + Int(buff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA1.currentMgc, 72)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 50)
        XCTAssertEqual(heroA1.currentMgc, 72)
        XCTAssertEqual(heroB0.currentMgc, 0)
        XCTAssertEqual(heroB1.currentMgc, 0)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_mgc_4() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeDebuff = Ability.example_mgcPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 250, mgc: 250, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 300, mgc: 300, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 350, mgc: 350, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 400, mgc: 400, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 2)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 250)
        XCTAssertEqual(heroA1.currentMgc, 300)
        XCTAssertEqual(heroB0.currentMgc, 350 + Int(buff.value!) - Int(aoeDebuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroB1.currentMgc, 400 - Int(aoeDebuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 250)
        XCTAssertEqual(heroA1.currentMgc, 300)
        XCTAssertEqual(heroB0.currentMgc, 350)
        XCTAssertEqual(heroB1.currentMgc, 400)
    }
    
    func testCalculateRound_powerDown_powerUp_powerDownAOE_mgc_5() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
        let aoeDebuff = Ability.example_mgcPowerDownAOE
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 250, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: aoeDebuff, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 300, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 550, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 450, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroB0.currentHaste = 600
        heroA0.currentHaste = 600
        heroA1.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 2)
        XCTAssertEqual(heroB0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 1)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 250)
        XCTAssertEqual(heroA1.currentMgc, 300)
        XCTAssertEqual(heroB0.currentMgc, 550 + Int(buff.value!) - Int(aoeDebuff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroB1.currentMgc, 450 - Int(aoeDebuff.value!))
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA1.currentDebuffs.count, 0)
        XCTAssertEqual(heroA1.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentMgc, 250)
        XCTAssertEqual(heroA1.currentMgc, 300)
        XCTAssertEqual(heroB0.currentMgc, 550)
        XCTAssertEqual(heroB1.currentMgc, 450)
    }
    
    func testCalculateRound_hasteToZero_0() {
        let spell = Ability.example_hasteZero
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 40, armor: 0, magicResist: 0, ulti: spell, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        heroB0.currentHaste = 650
        heroB1.currentHaste = 300
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentHaste, 0)
        XCTAssertEqual(heroB0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHaste, 310)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentHaste, 40)
        XCTAssertEqual(heroB0.currentHaste, 20)
        XCTAssertEqual(heroB1.currentHaste, 320)
        
        heroA0.currentHaste = 600
        heroB0.currentHP = 0
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentHaste, 0)
        XCTAssertEqual(heroB1.currentHaste, 0)
    }
    
    func testCalculateRound_damageDealtReduction_0() {
        let debuff = Ability.example_damageDealtReduction

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: debuff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
    }
    
    func testCalculateRound_damageDealtIncrease_0() {
        let buff = Ability.example_damageDealtIncrease

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroA0.currentBuffs.count, 0)
    }
    
    func testCalculateRound_damageTakenReduction_0() {
        let buff = Ability.example_damageTakenReduction

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
    }
    
    func testCalculateRound_immune_0() {
        let buff = Ability.example_immune

        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 10, armor: 0, magicResist: 0, ulti: buff, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let battle = Battle(teamA: [heroA0], teamB: [heroB0, heroB1])
        
        heroA0.currentHaste = 600
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentBuffs.count, 0)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB0.currentBuffs.count, 0)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)

        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroB1.currentDebuffs.count, 0)
    }
    
    func testCalculateRound_Enrage_0() {
        let ability = Ability.example_enrage
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        // Mindenki full senkire sem tud castolni, kap hastet
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        
        // Mindenki full senkire sem tud castolni, nem kap hastet mert 600+ van neki kap hastet
        heroA0.currentHaste = 610
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentHaste, 610)
        
        // low hp de nincs elég hastje, kap hastet
        heroA0.currentHaste = 0
        heroA0.currentHP = 10
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentHaste, 40)
        XCTAssertEqual(heroA0.currentHP, 10)
        
        // low hp van elég hastje, castol
        heroA0.currentHaste = 700
        heroA0.currentHP = 10
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, 999)
        XCTAssertEqual(heroA0.currentHaste, 100)
        XCTAssertEqual(heroA0.currentHP, 10)
        
        battle.calculateNextRound(manualNext: true)
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, 997)
        XCTAssertEqual(heroA0.currentHaste, 100)
        XCTAssertEqual(heroA0.currentHP, 10)
    }
    
    func testCalculateRound_hasteRegenerationBlock_0() {
        let ability = Ability.example_hasteRegenerationBlockDOT
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 20, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 30, armor: 0, magicResist: 0, ulti: ability, level: 0)
        let heroB0 = SimulationEntity(name: "heroB0", phy: 100, mgc: 0, hp: 1000, haste: 50, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        
        let heroB0ExpectedUltDMG = Int((100.0 * Ability.example_physicalStrike.value!).rounded())
        
        let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0])
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroB0.currentDebuffs.count, 0)
        XCTAssertEqual(heroA0.currentHaste, 20)
        XCTAssertEqual(heroA1.currentHaste, 30)
        XCTAssertEqual(heroB0.currentHaste, 50)
        
        //Kör elején még megkapja a hastet
        heroA0.currentHaste = 610
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentHaste, 10)
        XCTAssertEqual(heroA1.currentHaste, 60)
        XCTAssertEqual(heroB0.currentHaste, 100)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentHaste, 30)
        XCTAssertEqual(heroA1.currentHaste, 90)
        XCTAssertEqual(heroB0.currentHaste, 100)
        
        heroA0.currentHP = 1000
        heroB0.currentHaste = 610
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroB0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentHaste, 50)
        XCTAssertEqual(heroA1.currentHaste, 120)
        XCTAssertEqual(heroB0.currentHaste, 10)
        XCTAssertEqual(heroA0.currentHP, 1000 - heroB0ExpectedUltDMG)
        
        battle.calculateNextRound(manualNext: true)
        XCTAssertEqual(heroA0.currentHaste, 70)
        XCTAssertEqual(heroA1.currentHaste, 150)
        XCTAssertEqual(heroB0.currentHaste, 10)
    }
}
