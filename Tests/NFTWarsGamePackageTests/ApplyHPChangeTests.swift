//
//  ApplyHPChangeTests.swift
//  NFTWarsSimTests
//
//  Created by Korsós Tibor on 2023. 02. 17..
//

import XCTest
import NFTWarsGamePackage

final class ApplyHPChangeTests: XCTestCase {

	override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
	}
	
	func testApplyHPChange_Damage() {
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 975
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 930
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_Heal() {
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 950
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		// 900 hp-ra 50-et healel akkor 950-lesz
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 50, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		// 950 hp-ra 100-at healel akkor 1000-lesz, egyébként tul tud szaladni a heal a max HP-n
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 100, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_LifeSteal() {
		let expectedNormalHPAHero1 = 1000
		let expectedCurrentHPAHero1 = 910
		let expectedNormalHPBHero1 = 1000
		let expectedCurrentHPBHero1 = 890
		let expectedNormalHPAHero2 = 1000
		let expectedCurrentHPAHero2 = 860
		let expectedNormalHPBHero2 = 1000
		let expectedCurrentHPBHero2 = 940
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 10)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero1)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero1)
		
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 50)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero2)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero2)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero2)
	}
	
	func testApplyHPChange_Damage_withDamageDealtIncrease() {
		let buff = Ability.example_damageDealtIncrease
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 1000 - Int((25 * (1.0 + statusEffect.ultimate.value!)).rounded())
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000 - Int((70 * (1.0 + statusEffect.ultimate.value!)).rounded())

		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		heroA0.applyBuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		heroB0.applyBuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_LifeSteal_withDamageDealtIncrease() {
		let buff = Ability.example_damageDealtIncrease
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHPAHero1 = 1000
		let expectedCurrentHPAHero1 = 900 + Int((10 * (1.0 + statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero1 = 1000
		let expectedCurrentHPBHero1 = 900 - Int((10 * (1.0 + statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPAHero2 = 1000
		let expectedCurrentHPAHero2 = 900 - Int((50 * (1.0 + statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero2 = 1000
		let expectedCurrentHPBHero2 = 900 + Int((50 * (1.0 + statusEffect.ultimate.value!)).rounded())
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 10)
		heroA0.applyBuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero1)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero1)
		
		heroA0.currentHP = 900
		heroB0.currentHP = 900
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 50)
		heroB0.applyBuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero2)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero2)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero2)
	}
	
	func testApplyHPChange_Damage_withDamageDealtReduction() {
		let debuff = Ability.example_damageDealtReduction
		let statusEffect = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 1000 - Int((25 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000 - Int((70 * (1.0 - statusEffect.ultimate.value!)).rounded())

		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		heroA0.applyDebuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		heroB0.applyDebuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_LifeSteal_withDamageDealtReduction() {
		let debuff = Ability.example_damageDealtReduction
		let statusEffect = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHPAHero1 = 1000
		let expectedCurrentHPAHero1 = 900 + Int((10 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero1 = 1000
		let expectedCurrentHPBHero1 = 900 - Int((10 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPAHero2 = 1000
		let expectedCurrentHPAHero2 = 900 - Int((50 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero2 = 1000
		let expectedCurrentHPBHero2 = 900 + Int((50 * (1.0 - statusEffect.ultimate.value!)).rounded())
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 10)
		heroA0.applyDebuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero1)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero1)
		
		heroA0.currentHP = 900
		heroB0.currentHP = 900
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 50)
		heroB0.applyDebuff(newEffect: statusEffect)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero2)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero2)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero2)
	}
	
	func testApplyHPChange_Damage_withDamageTakenReduction() {
		let buff = Ability.example_damageTakenReduction
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 1000 - Int((25 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000 - Int((70 * (1.0 - statusEffect.ultimate.value!)).rounded())

		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		heroA0.applyBuff(newEffect: statusEffect)
		heroB0.applyBuff(newEffect: statusEffect)
		
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_LifeSteal_withDamageTakenReduction() {
		let buff = Ability.example_damageTakenReduction
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHPAHero1 = 1000
		let expectedCurrentHPAHero1 = 900 + Int((10 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero1 = 1000
		let expectedCurrentHPBHero1 = 900 - Int((10 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPAHero2 = 1000
		let expectedCurrentHPAHero2 = 900 - Int((50 * (1.0 - statusEffect.ultimate.value!)).rounded())
		let expectedNormalHPBHero2 = 1000
		let expectedCurrentHPBHero2 = 900 + Int((50 * (1.0 - statusEffect.ultimate.value!)).rounded())
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		heroB0.applyBuff(newEffect: statusEffect)
		heroA0.applyBuff(newEffect: statusEffect)
		
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 10)
   
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero1)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero1)
		
		heroA0.currentHP = 900
		heroB0.currentHP = 900
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 50)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero2)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero2)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero2)
	}
	
	func testApplyHPChange_Damage_withImmune() {
		let buff = Ability.example_immune
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 1000
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000

		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		heroA0.applyBuff(newEffect: statusEffect)
		heroB0.applyBuff(newEffect: statusEffect)
		
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_LifeSteal_withImmune() {
		let buff = Ability.example_immune
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHPAHero1 = 1000
		let expectedCurrentHPAHero1 = 900
		let expectedNormalHPBHero1 = 1000
		let expectedCurrentHPBHero1 = 900
		let expectedNormalHPAHero2 = 1000
		let expectedCurrentHPAHero2 = 900
		let expectedNormalHPBHero2 = 1000
		let expectedCurrentHPBHero2 = 900
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		heroB0.applyBuff(newEffect: statusEffect)
		heroA0.applyBuff(newEffect: statusEffect)
		
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 10)
   
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero1)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero1)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero1)
		
		heroA0.currentHP = 900
		heroB0.currentHP = 900
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 0, lifeSteal: 50)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHPAHero2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHPAHero2)
		XCTAssertEqual(heroB0.hp, expectedNormalHPBHero2)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHPBHero2)
	}
	
	func testApplyHPChange_HealWithImmune() {
		let buff = Ability.example_immune
		let statusEffect = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		
		let expectedNormalHP1 = 1000
		let expectedCurrentHP1 = 950
		let expectedNormalHP2 = 1000
		let expectedCurrentHP2 = 1000
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroA0.currentHP = 900
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		heroB0.currentHP = 900
		
		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		heroB0.applyBuff(newEffect: statusEffect)
		heroA0.applyBuff(newEffect: statusEffect)
		
		// 900 hp-ra 50-et healel akkor 950-lesz
		let hpChangeValue1 = HealthChangeValue(damage: 0, heal: 50, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, expectedNormalHP1)
		XCTAssertEqual(heroB0.currentHP, expectedCurrentHP1)
		
		// 950 hp-ra 100-at healel akkor 1000-lesz, egyébként tul tud szaladni a heal a max HP-n
		let hpChangeValue2 = HealthChangeValue(damage: 0, heal: 100, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, expectedNormalHP2)
		XCTAssertEqual(heroA0.currentHP, expectedCurrentHP2)
	}
	
	func testApplyHPChange_Damage_withAbsorb_0() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		heroA0.currentAbsorb = 100
		heroB0.currentAbsorb = 100
		let hpChangeValue1 = HealthChangeValue(damage: -25, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, 1000)
		XCTAssertEqual(heroB0.currentHP, 1000)
		XCTAssertEqual(heroB0.currentAbsorb, 75)
		
		let hpChangeValue2 = HealthChangeValue(damage: -70, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, 1000)
		XCTAssertEqual(heroA0.currentHP, 1000)
		XCTAssertEqual(heroA0.currentAbsorb, 30)
	}
	
	func testApplyHPChange_Damage_withAbsorb_1() {
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_AOEHeal, level: 0)

		let battle = Battle(teamA: [heroA0], teamB: [heroB0])
		
		heroA0.currentAbsorb = 100
		heroB0.currentAbsorb = 100
		let hpChangeValue1 = HealthChangeValue(damage: -125, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroA0, target: heroB0, value: hpChangeValue1)
		XCTAssertEqual(heroB0.hp, 1000)
		XCTAssertEqual(heroB0.currentHP, 975)
		XCTAssertEqual(heroB0.currentAbsorb, 0)
		
		let hpChangeValue2 = HealthChangeValue(damage: -170, heal: 0, lifeSteal: 0)
		battle.applyHPChange(caster: heroB0, target: heroA0, value: hpChangeValue2)
		XCTAssertEqual(heroA0.hp, 1000)
		XCTAssertEqual(heroA0.currentHP, 930)
		XCTAssertEqual(heroA0.currentAbsorb, 0)
	}
}
