//
//  NFTWarsSimTests.swift
//  NFTWarsSimTests
//
//  Created by Korsós Tibor on 2022. 12. 13..
//

import XCTest
import NFTWarsGamePackage

class BattleTests: XCTestCase {

    override func setUpWithError() throws {
		BattleLogger.shared.logLevel = .none
		SimulationEntity.useDiscreteValues = true
    }

    func testUseBaseAttackDamage1() throws {
		let testPhySingle1 = SimulationEntity(name: "Attacker", phy: 250, mgc: 0, hp: 15000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		testPhySingle1.isTeamA = true
		let dummy = SimulationEntity(name: "Dummy", phy: 0, mgc: 0, hp: 15000, haste: 0, armor: 200, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		dummy.isTeamA = false
		let battle = Battle(teamA: [], teamB: [])
        
        let expectedDamage = Int(((250 * dummy.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1 = 15000 - expectedDamage
        let expectedHP2 = 15000 - expectedDamage - expectedDamage
		
		let baseAttackDamage1 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage1, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP1)
		
		let baseAttackDamage2 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage2, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP2)
    }
	
	func testUseBaseAttackDamage2() throws {
		let testPhySingle1 = SimulationEntity(name: "Attacker", phy: 100, mgc: 0, hp: 15000, haste: 00, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		testPhySingle1.isTeamA = true
		let dummy = SimulationEntity(name: "Dummy", phy: 0, mgc: 0, hp: 10000, haste: 0, armor: 200, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		dummy.isTeamA = false
		let battle = Battle(teamA: [], teamB: [])
        
        let expectedDamage = Int(((100 * dummy.getDefenseMultiplier(for: .phy)) * Constants.baseAttackMultiplier).rounded())
        let expectedHP1 = 10000 - expectedDamage
        let expectedHP2 = 10000 - expectedDamage - expectedDamage
		
		let baseAttackDamage1 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage1, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP1)
		
		let baseAttackDamage2 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage2, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP2)
	}
	
	func testUseBaseAttackDamage3() throws {
        let expectedDamage = Int((100 * Constants.baseAttackMultiplier).rounded())
		let expectedHP1 = 10000 - expectedDamage
		let expectedHP2 = 10000 - expectedDamage - expectedDamage
		
		let testPhySingle1 = SimulationEntity(name: "Attacker", phy: 100, mgc: 0, hp: 15000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		testPhySingle1.isTeamA = true
		let dummy = SimulationEntity(name: "Dummy", phy: 0, mgc: 0, hp: 10000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		dummy.isTeamA = false
		let battle = Battle(teamA: [], teamB: [])
		
		let baseAttackDamage1 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage1, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP1)
		
		let baseAttackDamage2 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage2, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP2)
	}
	
	func testUseBaseAttackDamageForUnderflow() throws {
		let expectedDamage = 0
		let expectedHP1 = 10000
		let expectedHP2 = 10000
		
		let testPhySingle1 = SimulationEntity(name: "Attacker", phy: 0, mgc: 0, hp: 15000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		testPhySingle1.isTeamA = true
		let dummy = SimulationEntity(name: "Dummy", phy: 0, mgc: 0, hp: 10000, haste: 0, armor: 200, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
		dummy.isTeamA = false
		let battle = Battle(teamA: [], teamB: [])
		
		let baseAttackDamage1 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage1, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP1)
		
		let baseAttackDamage2 = battle.useBaseAttack(caster: testPhySingle1, target: dummy)
		XCTAssertEqual(baseAttackDamage2, expectedDamage)
		XCTAssertEqual(dummy.currentHP, expectedHP2)
	}

	func testAttackOrder() throws {
		let expectedHeroCount = 5
		let expectedHeroOrder = ["Hero-order-0", "Hero-order-1", "Hero-order-2", "Hero-order-3", "Hero-order-4"]
		
		let hero0 = SimulationEntity(name: "Hero-order-0", phy: 0, mgc: 0, hp: 1, haste: 100, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let hero1 = SimulationEntity(name: "Hero-order-1", phy: 0, mgc: 0, hp: 1, haste: 90, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let hero2 = SimulationEntity(name: "Hero-order-2", phy: 0, mgc: 0, hp: 1, haste: 80, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let hero3 = SimulationEntity(name: "Hero-order-3", phy: 0, mgc: 0, hp: 1, haste: 70, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let hero4 = SimulationEntity(name: "Hero-order-4", phy: 0, mgc: 0, hp: 1, haste: 60, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroDead = SimulationEntity(name: "Hero-order-dead", phy: 0, mgc: 0, hp: 0, haste: 110, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [], teamB: [])
		let attackOrder = battle.getAliveAttackOrder(heroes: [heroDead, hero3, hero2, hero4, hero0, hero1])
		
		XCTAssertEqual(attackOrder.count, expectedHeroCount, "🔴 Maybe the dead is in the attack order too?")
		XCTAssertEqual(attackOrder[0].name, expectedHeroOrder[0])
		XCTAssertEqual(attackOrder[1].name, expectedHeroOrder[1])
		XCTAssertEqual(attackOrder[2].name, expectedHeroOrder[2])
		XCTAssertEqual(attackOrder[3].name, expectedHeroOrder[3])
		XCTAssertEqual(attackOrder[4].name, expectedHeroOrder[4])
	}
	
	func testCalculateHaste() {
		let expectedHeroStartingHaste = 0
		let expectedHeroHasteAfterCalculateCall1 = 10
		let expectedHeroHasteAfterCalculateCall2 = 20
		let expectedHeroHasteAfterCalculateRoundStartCall = 30
		
		let hero = SimulationEntity(name: "Hero", phy: 0, mgc: 0, hp: 1, haste: 10, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let battle = Battle(teamA: [hero], teamB: [])
		
		XCTAssertEqual(hero.currentHaste, expectedHeroStartingHaste)
		
		battle.calculateHastes(heroes: [hero])
		XCTAssertEqual(hero.currentHaste, expectedHeroHasteAfterCalculateCall1)
		
		battle.calculateHastes(heroes: [hero])
		XCTAssertEqual(hero.currentHaste, expectedHeroHasteAfterCalculateCall2)
		
		battle.roundStartEffects()
		XCTAssertEqual(hero.currentHaste, expectedHeroHasteAfterCalculateRoundStartCall)
	}

	func testIsAnyoneAlive() {
		let heroAlive1 = SimulationEntity(name: "heroAlive1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroAlive2 = SimulationEntity(name: "heroAlive2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroDead1 = SimulationEntity(name: "heroDead1", phy: 0, mgc: 0, hp: 0, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroDead2 = SimulationEntity(name: "heroDead2", phy: 0, mgc: 0, hp: 0, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [], teamB: [])
		let allAlive = battle.isAnyoneAlive(team: [heroAlive1, heroAlive2])
		let nooneAlive = battle.isAnyoneAlive(team: [heroDead1, heroDead2])
		let oneAlive = battle.isAnyoneAlive(team: [heroDead1, heroAlive2])
		
		XCTAssertEqual(allAlive, true)
		XCTAssertEqual(nooneAlive, false)
		XCTAssertEqual(oneAlive, true)
	}
	
	func testBattleEnd() {
		let heroAlive1 = SimulationEntity(name: "heroAlive1", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroAlive2 = SimulationEntity(name: "heroAlive2", phy: 0, mgc: 0, hp: 1, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroDead1 = SimulationEntity(name: "heroDead1", phy: 0, mgc: 0, hp: 0, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroDead2 = SimulationEntity(name: "heroDead2", phy: 0, mgc: 0, hp: 0, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle1 = Battle(teamA: [heroAlive1, heroAlive2], teamB: [heroDead1, heroDead2])
		var battle1HomeTeamWon: Bool?
        battle1.completion = { (battleResult ) in battle1HomeTeamWon = battleResult.isTeamAWon }
		let isBattle1Ended = battle1.checkIsBattleEnded()
		XCTAssertEqual(isBattle1Ended, true)
		XCTAssertEqual(battle1HomeTeamWon, true)
		
		let battle2 = Battle(teamA: [heroDead1, heroDead2], teamB: [heroAlive1, heroAlive2])
		var battle2HomeTeamWon: Bool?
        battle2.completion = { (battleResult) in battle2HomeTeamWon = battleResult.isTeamAWon }
		let isBattle2Ended = battle2.checkIsBattleEnded()
		XCTAssertEqual(isBattle2Ended, true)
		XCTAssertEqual(battle2HomeTeamWon, false)
		
		let battle3 = Battle(teamA: [heroAlive1, heroDead1], teamB: [heroAlive2, heroDead2])
		var battle3CompletionCalled = false
		battle3.completion = { (_) in battle3CompletionCalled = true }
		let isBattle3Ended = battle3.checkIsBattleEnded()
		XCTAssertEqual(isBattle3Ended, false)
		XCTAssertEqual(battle3CompletionCalled, false)
		
		let battle4 = Battle(teamA: [heroDead1, heroDead2], teamB: [heroDead1, heroDead2])
		var battle4HomeTeamWon: Bool?
		var battle4CompletionCalled = false
        battle4.completion = { (battleResult) in battle4HomeTeamWon = battleResult.isTeamAWon; battle4CompletionCalled = true }
		let isBattle4Ended = battle4.checkIsBattleEnded()
		XCTAssertEqual(isBattle4Ended, true)
		XCTAssertEqual(battle4HomeTeamWon, nil)
		XCTAssertEqual(battle4CompletionCalled, true)
		
		let battle5 = Battle(teamA: [heroAlive1, heroAlive2], teamB: [heroAlive1, heroAlive2])
		battle5.round = 1000
		var battle5HomeTeamWon: Bool?
		var battle5CompletionCalled = false
        battle5.completion = { (battleResult) in battle5HomeTeamWon = battleResult.isTeamAWon; battle5CompletionCalled = true }
		let isBattle5Ended = battle5.checkIsBattleEnded()
		XCTAssertEqual(isBattle5Ended, true)
		XCTAssertEqual(battle5HomeTeamWon, nil)
		XCTAssertEqual(battle5CompletionCalled, true)
	}
	
	func testGetBaseAttackTarget1() {
		let expectedTarget1Name = "heroB0"
		let expectedTarget2Name = "heroB1"
		let expectedTarget3Name = "heroA0"
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 1, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 2, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 1, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 2, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		let target1 = battle.getTargetForBaseAttack(attacker: heroA1)
		XCTAssertEqual(target1!.name, expectedTarget1Name)
		
		heroB0.currentHP = 0
		let target2 = battle.getTargetForBaseAttack(attacker: heroA1)
		XCTAssertEqual(target2!.name, expectedTarget2Name)
		
		let target3 = battle.getTargetForBaseAttack(attacker: heroB1)
		XCTAssertEqual(target3!.name, expectedTarget3Name)
		
		heroB1.currentHP = 0
		let target4 = battle.getTargetForBaseAttack(attacker: heroA1)
		XCTAssertNil(target4) // Nil mert minden ellenfél halott
	}
	
	func testGetBaseAttackTarget2() {
		let expectedTarget1Name = "heroA0"
		let expectedTarget2Name = "heroA1"
		let expectedTarget3Name = "heroB0"
		
		let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1, haste: 1, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1, haste: 2, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB0 = SimulationEntity(name: "heroB0", phy: 0, mgc: 0, hp: 1, haste: 1, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		let heroB1 = SimulationEntity(name: "heroB1", phy: 0, mgc: 0, hp: 1, haste: 2, armor: 0, magicResist: 0, ulti: Ability.example_heal, level: 0)
		
		let battle = Battle(teamA: [heroA0, heroA1], teamB: [heroB0, heroB1])
		
		let target1 = battle.getTargetForBaseAttack(attacker: heroB1)
		XCTAssertEqual(target1!.name, expectedTarget1Name)
		
		heroA0.currentHP = 0
		let target2 = battle.getTargetForBaseAttack(attacker: heroB1)
		XCTAssertEqual(target2!.name, expectedTarget2Name)
		
		let target3 = battle.getTargetForBaseAttack(attacker: heroA1)
		XCTAssertEqual(target3!.name, expectedTarget3Name)
		
		heroA1.currentHP = 0
		let target4 = battle.getTargetForBaseAttack(attacker: heroB1)
		XCTAssertNil(target4) // Nil mert minden ellenfél halott
	}
	
    func testCalculateStatusEffectsEffect_PhyDOT_1() {
        let DOT = Ability.example_physicalDOT
        let statusEffects = StatusEffect(effect: DOT, casterLevel: 0, casterBaseValue: 100, casterRandomModifier: 1, caster: nil)
        let expectedTickDMG = -Int((100.0 * Ability.example_physicalDOT.value!).rounded())
        
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffects)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_PhyDOT_2() {
        let DOT = Ability.example_physicalDOT
		let statusEffects = StatusEffect(effect: DOT, casterLevel: 0, casterBaseValue: 1000, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let expectedTickDMG = -Int((1000.0 * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalDOT.value!).rounded())
        heroA0.applyDebuff(newEffect: statusEffects)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcDOT_1() {
        let DOT = Ability.example_magicalDOT
		let statusEffects = StatusEffect(effect: DOT, casterLevel: 0, casterBaseValue: 100, casterRandomModifier: 1, caster: nil)
        let expectedTickDMG = -Int((100.0 * Ability.example_magicalDOT.value!).rounded())
        
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffects)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcDOT_2() {
        let DOT = Ability.example_magicalDOT
		let statusEffects = StatusEffect(effect: DOT, casterLevel: 0, casterBaseValue: 1000, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 150, ulti: Ability.example_physicalStrike, level: 0)
        let expectedTickDMG = -Int((1000.0 * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicalDOT.value!).rounded())
        heroA0.applyDebuff(newEffect: statusEffects)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_HOT_1() {
        let HOT = Ability.example_magicalHOT
		let statusEffects = StatusEffect(effect: HOT, casterLevel: 0, casterBaseValue: 100, casterRandomModifier: 1, caster: nil)
        let expectedTickDMG = Int((100.0 * Ability.example_magicalHOT.value!).rounded())
        
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyBuff(newEffect: statusEffects)
        
        heroA0.currentHP = 500
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 500 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1000
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 1000)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_HOT_2() {
        let HOT = Ability.example_magicalHOT
		let statusEffects = StatusEffect(effect: HOT, casterLevel: 0, casterBaseValue: 1000, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 1000, ulti: Ability.example_physicalStrike, level: 0)
        let expectedTickDMG = Int((1000.0 * Ability.example_magicalHOT.value!).rounded())
        heroA0.applyBuff(newEffect: statusEffects)
        
        heroA0.currentHP = 500
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 500 + expectedTickDMG)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 999
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 1000)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1000
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 1000)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcAndPhyDOT_1() {
        let DOT1 = Ability.example_magicalDOT
        let DOT2 = Ability.example_physicalDOT
		let statusEffect1 = StatusEffect(effect: DOT1, casterLevel: 0, casterBaseValue: 100, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: DOT2, casterLevel: 0, casterBaseValue: 150, casterRandomModifier: 1, caster: nil)
        let expectedTickDMG1 = -Int((100.0 * Ability.example_magicalDOT.value!).rounded())
        let expectedTickDMG2 = -Int((150.0 * Ability.example_physicalDOT.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect2)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG1 + expectedTickDMG2)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcAndPhyDOT_2() {
        let DOT1 = Ability.example_magicalDOT
        let DOT2 = Ability.example_physicalDOT
		let statusEffect1 = StatusEffect(effect: DOT1, casterLevel: 0, casterBaseValue: 1000, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: DOT2, casterLevel: 0, casterBaseValue: 1500, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 150, ulti: Ability.example_physicalStrike, level: 0)
        let expectedTickDMG1 = -Int((1000.0 * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicalDOT.value!).rounded())
        let expectedTickDMG2 = -Int((1500.0 * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalDOT.value!).rounded())
        heroA0.applyDebuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect2)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 1000 + expectedTickDMG1 + expectedTickDMG2)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcAndPhyDOTAndHOT_1() {
        let DOT1 = Ability.example_magicalDOT
        let DOT2 = Ability.example_physicalDOT
        let HOT1 = Ability.example_magicalHOT
		let statusEffect1 = StatusEffect(effect: DOT1, casterLevel: 0, casterBaseValue: 100, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: DOT2, casterLevel: 0, casterBaseValue: 150, casterRandomModifier: 1, caster: nil)
		let statusEffect3 = StatusEffect(effect: HOT1, casterLevel: 0, casterBaseValue: 200, casterRandomModifier: 1, caster: nil)
        let expectedTickDMG1 = -Int((100.0 * Ability.example_magicalDOT.value!).rounded())
        let expectedTickDMG2 = -Int((150.0 * Ability.example_physicalDOT.value!).rounded())
        let expectedTickHeal1 = Int((200.0 * Ability.example_magicalHOT.value!).rounded())
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.currentHP = 500
        heroA0.applyDebuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect2)
        heroA0.applyBuff(newEffect: statusEffect3)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 500 + expectedTickDMG1 + expectedTickDMG2 + expectedTickHeal1)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testCalculateStatusEffectsEffect_MgcAndPhyDOTAndHOT_2() {
        let DOT1 = Ability.example_magicalDOT
        let DOT2 = Ability.example_physicalDOT
        let HOT1 = Ability.example_magicalHOT
		let statusEffect1 = StatusEffect(effect: DOT1, casterLevel: 0, casterBaseValue: 300, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: DOT2, casterLevel: 0, casterBaseValue: 250, casterRandomModifier: 1, caster: nil)
		let statusEffect3 = StatusEffect(effect: HOT1, casterLevel: 0, casterBaseValue: 300, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 100, magicResist: 150, ulti: Ability.example_physicalStrike, level: 0)
        let expectedTickDMG1 = -Int((300 * heroA0.getDefenseMultiplier(for: .mgc) * Ability.example_magicalDOT.value!).rounded())
        let expectedTickDMG2 = -Int((250 * heroA0.getDefenseMultiplier(for: .phy) * Ability.example_physicalDOT.value!).rounded())
        let expectedTickHeal1 = Int((300 * Ability.example_magicalHOT.value!).rounded())
        
        heroA0.currentHP = 500
        heroA0.applyDebuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect2)
        heroA0.applyBuff(newEffect: statusEffect3)
        heroA0.calculateStatusEffectsEffect()
        
        XCTAssertEqual(heroA0.currentHP, 500 + expectedTickDMG1 + expectedTickDMG2 + expectedTickHeal1)
        XCTAssertEqual(heroA0.hp, 1000)
        
        heroA0.currentHP = 1
        heroA0.calculateStatusEffectsEffect()
        XCTAssertEqual(heroA0.currentHP, 0)
        XCTAssertEqual(heroA0.hp, 1000)
    }
    
    func testReduceStatusEffectsDuration() {
        let DOT1 = Ability.example_magicalDOT
        let DOT2 = Ability.example_physicalDOT
        let HOT1 = Ability.example_magicalHOT
		let statusEffect1 = StatusEffect(effect: DOT1, casterLevel: 0, casterBaseValue: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: DOT2, casterLevel: 0, casterBaseValue: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect3 = StatusEffect(effect: HOT1, casterLevel: 0, casterBaseValue: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        let battle = Battle(teamA: [heroA0], teamB: [])
        heroA0.applyDebuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect2)
        heroA0.applyBuff(newEffect: statusEffect3)
        heroA0.currentDebuffs[1].remainingDuration -= 1
        
        battle.reduceStatusEffectsDuration()
        
        XCTAssertEqual(heroA0.currentDebuffs.count, 2)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, Ability.example_magicalDOT.duration! - 1)
        XCTAssertEqual(heroA0.currentDebuffs[1].remainingDuration, Ability.example_physicalDOT.duration! - 2)
        XCTAssertEqual(heroA0.currentBuffs[0].remainingDuration, Ability.example_magicalHOT.duration! - 1)

        heroA0.currentDebuffs[0].remainingDuration = 1
        heroA0.currentDebuffs[1].remainingDuration = 2
        heroA0.currentBuffs[0].remainingDuration = 1
        battle.reduceStatusEffectsDuration()
        
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 0)
        XCTAssertEqual(heroA0.currentDebuffs[0].remainingDuration, 1)
        XCTAssertEqual(heroA0.currentDebuffs[0].ultimate.id, DOT2.id)
    }
    
    func testCalculateStatusEffectsEffect_powerUp_phy_1() {
        let buff = Ability.example_phyPowerUp
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: buff, casterLevel: 60, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyBuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        
        statusEffect0.remainingDuration = 2
        heroA0.applyBuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, buff.duration!)
        
        let heroA1 = SimulationEntity(name: "heroA1", phy: 112, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentPhy, 112 + Int(buff.value!))
        
        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        heroA1.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA1.currentPhy, 112)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 110, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyBuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentPhy, 110 + Int(buff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))
    }
    
    func testCalculateStatusEffectsEffect_powerUp_mgc_1() {
        let buff = Ability.example_mgcPowerUp
		let statusEffect0 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: buff, casterLevel: 50, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyBuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        
        statusEffect1.remainingDuration = 2
        heroA0.applyBuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, buff.duration!)
        
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 128, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentMgc, 128 + Int(buff.value!))
        
        heroA0.calculatePowerUpExpiry(effect: statusEffect0)
        heroA1.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA1.currentMgc, 128)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 153, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyBuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentMgc, 153 + Int(buff.value! + 50.0 * Constants.example_mgcPowerUpValueLVLMultiplier))
    }
    
    func testCalculateStatusEffectsEffect_powerDown_phy_1() {
        let debuff = Ability.example_phyPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 60, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 10, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0)

        statusEffect0.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)

        let heroA1 = SimulationEntity(name: "heroA1", phy: 112, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyDebuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentPhy, 112 - Int(debuff.value!))

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA1.calculatePowerDownExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 10)
        XCTAssertEqual(heroA1.currentPhy, 112)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 110, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyDebuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentPhy, 110 - Int(debuff.value! + 60.0 * Constants.example_phyPowerUpValueLVLMultiplier))
    }
    
    func testCalculateStatusEffectsEffect_powerDown_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 50, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        
        statusEffect0.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        
        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 328, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyDebuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentMgc, 328 - Int(debuff.value!))
        
        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA1.calculatePowerDownExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA1.currentMgc, 328)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 500, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyDebuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentMgc, 500 - Int(debuff.value! + 50.0 * Constants.example_mgcPowerUpValueLVLMultiplier))
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_phy_1() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_phy_2() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 10, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 10)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_phy_3() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 300, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 300 + Int(buff.value!) - Int(debuff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 300 + Int(buff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 300)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_phy_4() {
        let debuff = Ability.example_phyPowerDown
        let buff = Ability.example_phyPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyBuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!) - Int(debuff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentPhy, 0 + Int(buff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        XCTAssertEqual(heroA0.currentPhy, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_mgc_1() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_mgc_2() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 10, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 0 + Int(buff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 10)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_mgc_3() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 300, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 300 + Int(buff.value!) - Int(debuff.value!))

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 300 + Int(buff.value!) - Int(debuff.value!))
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 300)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_powerUp_mgc_4() {
        let debuff = Ability.example_mgcPowerDown
        let buff = Ability.example_mgcPowerUp
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: buff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyBuff(newEffect: statusEffect1)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(heroA0.currentMgc, 0)

        statusEffect0.remainingDuration = 2
        statusEffect1.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        heroA0.applyBuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)
        XCTAssertEqual(statusEffect1.remainingDuration, buff.duration!)

        heroA0.calculatePowerUpExpiry(effect: statusEffect1)
        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        XCTAssertEqual(heroA0.currentMgc, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(heroA0.currentBuffs.count, 1)
    }
    
    func testCalculateStatusEffectsEffect_powerDown_armor_1() {
        let debuff = Ability.example_armorPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 60, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 10, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentArmor, 0)

        statusEffect0.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentArmor, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)

        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 112, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyDebuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentArmor, 112 - Int(debuff.value!))

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA1.calculatePowerDownExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentArmor, 10)
        XCTAssertEqual(heroA1.currentArmor, 112)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 110, magicResist: 0, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyDebuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentArmor, 110 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
    }
    
    func testCalculateStatusEffectsEffect_powerDown_magic_resist_1() {
        let debuff = Ability.example_magicResistPowerDown
		let statusEffect0 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect1 = StatusEffect(effect: debuff, casterLevel: 0, casterRandomModifier: 1, caster: nil)
		let statusEffect2 = StatusEffect(effect: debuff, casterLevel: 60, casterRandomModifier: 1, caster: nil)
        
        let heroA0 = SimulationEntity(name: "heroA0", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 10, ulti: Ability.example_physicalStrike, level: 0)
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMR, 0)

        statusEffect0.remainingDuration = 2
        heroA0.applyDebuff(newEffect: statusEffect0)
        XCTAssertEqual(heroA0.currentMR, 0)
        XCTAssertEqual(heroA0.currentDebuffs.count, 1)
        XCTAssertEqual(statusEffect0.remainingDuration, debuff.duration!)

        let heroA1 = SimulationEntity(name: "heroA1", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 112, ulti: Ability.example_physicalStrike, level: 0)
        heroA1.applyDebuff(newEffect: statusEffect1)
        XCTAssertEqual(heroA1.currentMR, 112 - Int(debuff.value!))

        heroA0.calculatePowerDownExpiry(effect: statusEffect0)
        heroA1.calculatePowerDownExpiry(effect: statusEffect1)
        XCTAssertEqual(heroA0.currentMR, 10)
        XCTAssertEqual(heroA1.currentMR, 112)
        
        let heroA2 = SimulationEntity(name: "heroA2", phy: 0, mgc: 0, hp: 1000, haste: 0, armor: 0, magicResist: 110, ulti: Ability.example_physicalStrike, level: 0)
        heroA2.applyDebuff(newEffect: statusEffect2)
        XCTAssertEqual(heroA2.currentMR, 110 - Int(debuff.value! + 60.0 * Constants.example_armorPowerDownValueLVLMultiplier))
    }
}
