//
//  File.swift
//  
//
//  Created by Korsós Tibor on 15/06/2024.
//

import Foundation

public class EntityStatModel {
    
    public var damageDealt: Int = 0
    public var damageTaken: Int = 0
    public var heal: Int = 0
    public var ultimateUsed: Int = 0
    public var roundSurvived: Int = 0
    
}
