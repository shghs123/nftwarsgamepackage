//
//  File.swift
//  
//
//  Created by Korsós Tibor on 15/06/2024.
//

import Foundation

public class StatusEffect {
    
    public var ultimate: HeroUltimate
    public var remainingDuration: Int
    public var casterBaseValue: Int?
    public let casterLevel: Int
    public var casterRandomModifier: Double
    public var powerModifierValue: Int?
    public weak var caster: SimulationEntity?
    
    public init(effect: HeroUltimate, casterLevel: Int, casterBaseValue: Int? = nil, casterRandomModifier: Double, caster: SimulationEntity?) {
        self.ultimate = effect
        self.remainingDuration = effect.duration!
        self.casterLevel = casterLevel
        self.casterBaseValue = casterBaseValue
        self.casterRandomModifier = casterRandomModifier
        self.caster = caster
    }
}
