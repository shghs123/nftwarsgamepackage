//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation

public struct HealthChangeValue {
	public var damage: Int {
		didSet { if damage > 0 { fatalError("Damage is positive.") }}
	}
	public var heal: Int{
		didSet { if heal < 0 { fatalError("Heal is negative.") }}
	}
	public var lifeSteal: Int {
		didSet { if lifeSteal < 0 { fatalError("LifeSteal is negative.") }}
	}
	
	public init(damage: Int = 0, heal: Int = 0, lifeSteal: Int = 0) {
		self.damage = damage
		self.heal = heal
		self.lifeSteal = lifeSteal
	}
}
