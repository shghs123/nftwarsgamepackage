//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation

public class SimulationEntity {
	
	// Stats and info
	public var name: String
	public let hp: Int
	public let phy: Int
	public let mgc: Int
	public let haste: Int
	public let armor: Int
	public let magicResist: Int
	public let ulti: HeroUltimate
	public var level: Int
    public var lowerModifier: Int
    public var upperModifier: Int
	
	public var simulationPriority: Int! // Csak arra van h szimuláláskor megadjuk h milyen pozicioba rakja a TournamentSimulator ha tudja
	
	// Combat and PvP
	public var isTeamA: Bool! // Battle létrehozásnál állítjuk be
	public var teamPosition: Int! // Battle létrehozásnál állítjuk be
	public var onceCastUltCasted: Bool? // Ha a hős ultija egyszer használatos ez jelöli, hogy előtte-e már. Nil ha nem egyszerhasználatos az ulti
    public var statModel: EntityStatModel = EntityStatModel() // Battle Stat számolásra
	
	public var currentHP: Int
	public var currentPhy: Int
	public var currentMgc: Int
	public var currentHaste: Int = 0
	public var currentArmor: Int
	public var currentMR: Int
	public var currentAbsorb: Int = 0
	public var currentBuffs: [StatusEffect] = []
	public var currentDebuffs: [StatusEffect] = []
	public static var useDiscreteValues = false // If set to yes there is no random modification on damage and heal. (Used for testing and balancing)
	
    public init(name: String, phy: Int, mgc: Int, hp: Int, haste: Int, armor: Int, magicResist: Int, ulti: HeroUltimate, level: Int, lowerModifier: Int = -20, upperModifier: Int = 20, simulationPriority: Int? = nil) {
		self.name = name
		self.phy = phy + (level * Constants.phyConstant)
		self.currentPhy = self.phy
		self.mgc = mgc + (level * Constants.mgcConstant)
		self.currentMgc = self.mgc
		self.hp = hp + (level * Constants.healthConstant)
		self.currentHP = self.hp
		self.haste = haste
		self.armor = armor + (level * Constants.armorConstant)
		self.currentArmor = self.armor
		self.magicResist = magicResist + (level * Constants.magicResistConstant)
		self.currentMR = self.magicResist
		self.ulti = ulti
		self.level = level
		self.lowerModifier = lowerModifier
		self.upperModifier = upperModifier
		self.onceCastUltCasted = ulti.onceCast ? false : nil
        self.simulationPriority = simulationPriority
	}
	
	public func copy(name: String? = nil, level: Int? = nil) -> SimulationEntity {
		let name = name == nil ? self.name : name ?? "No name"
		let level = level == nil ? self.level : level ?? 0
		
		let newEntity = SimulationEntity(name: name, phy: phy, mgc: mgc, hp: hp, haste: haste, armor: armor, magicResist: magicResist, ulti: ulti, level: level, simulationPriority: simulationPriority)
		newEntity.simulationPriority = self.simulationPriority
		return newEntity
	}
	
	public func getDefenseMultiplier(for type: EntityDefenseType) -> Double {
		switch type {
		case .phy:
			if currentArmor < 1 { return 1 }
			
			let result = (log2(Double(currentArmor)) * log2(Double(currentArmor))) / Constants.defenseMultiplier
			return 1 - (result/100)
		case .mgc:
			if currentMR < 1 { return 1 }
			
			let result = (log2(Double(currentMR)) * log2(Double(currentMR))) / Constants.defenseMultiplier
			return 1 - (result/100)
		default:
			fatalError("Not implementet")
		}
	}
	
	// A Damage mindig negativ szám
	func applyDamage(damage: Int, randomModifier: Double, battleId: String? = nil) -> Int {
		if damage > 0 { fatalError("Damage is positive.") }
		var damage = damage
        
        // calculate exact execute dmg
        var exacuteDamage: Int?
        if (damage < -(Int.max - 1)) {
            exacuteDamage = self.currentAbsorb + self.currentHP
        }
            
		if !SimulationEntity.useDiscreteValues {
			damage = Int((Double(damage) * randomModifier).rounded())
		}
		
		self.currentAbsorb += damage
		
		var remainigDamage = 0
		if self.currentAbsorb < 0 {
			remainigDamage = self.currentAbsorb
			self.currentAbsorb = 0
		}
		
		self.currentHP += remainigDamage
        
        // if execute happended log & return exact execute dmg
        if let exacuteDamage {
            if let battleId {
                BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟠 DMG:  \(exacuteDamage)", battleId: battleId)
            }
            
            return exacuteDamage
        }
		
		if let battleId {
			BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟠 DMG:  \(damage)", battleId: battleId)
		}
		
		return damage
	}
	
	// a heal mindig pozitiv szám
	public func applyHeal(heal: Int, randomModifier: Double, battleId: String? = nil) -> Int {
		if (heal < 0) { fatalError("Heal is negative.") }
		var heal = heal
		
		if !SimulationEntity.useDiscreteValues {
			heal = Int((Double(heal) * randomModifier).rounded())
		}
		
		self.currentHP += heal
		
		if self.currentHP > self.hp { self.currentHP = self.hp }
		
		if let battleId = battleId {
			BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟢 HEAL:  \(heal)", battleId: battleId)
		}
		
		return heal
	}
	
	/**
	 Ha még nincs a hősőn ilyen buff akkor felteszi, ha már van akkor a Duration-t a max-ra állítja
	 */
	public func applyBuff(newEffect: StatusEffect, battleId: String? = nil) {
		if let alreadyAppliedEffect = currentBuffs.first(where: { $0.ultimate.id ==  newEffect.ultimate.id}) {
			alreadyAppliedEffect.remainingDuration = newEffect.ultimate.duration!
			
			if let battleId = battleId {
				BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟢 BUFF refreshed: \(newEffect.ultimate.name), duration: \(newEffect.remainingDuration)", battleId: battleId)
			}
		} else {
			currentBuffs.append(newEffect)
			
			if newEffect.ultimate.effectType == .powerUp {
				calculatePowerUpApply(effect: newEffect)
			}
			
			if let battleId = battleId {
				BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟢 BUFF applied: \(newEffect.ultimate.name), duration: \(newEffect.remainingDuration)", battleId: battleId)
			}
		}
	}
	
	/**
	 Ha még nincs a hősőn ilyen debuff akkor felteszi, ha már van akkor a Duration-t a max-ra állítja
	 */
	public func applyDebuff(newEffect: StatusEffect, battleId: String? = nil) {
		if let alreadyAppliedEffect = currentDebuffs.first(where: { $0.ultimate.id ==  newEffect.ultimate.id}) {
			alreadyAppliedEffect.remainingDuration = newEffect.ultimate.duration!
			
			if let battleId = battleId {
				BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟠 DEBUFF refreshed:  \(newEffect.ultimate.name), duration: \(newEffect.remainingDuration)", battleId: battleId)
			}
		} else {
			currentDebuffs.append(newEffect)
			
			if newEffect.ultimate.effectType == .powerDown{
				calculatePowerDownApply(effect: newEffect)
			}
			
			if let battleId = battleId {
				BattleLogger.shared.log("       - target: \(self.getHPDescription()), 🟠 DEBUFF applied:  \(newEffect.ultimate.name), duration: \(newEffect.remainingDuration)", battleId: battleId)
			}
		}
	}
	
	/**
	 Először végig megy az összes buffon és ha az HOT tipusu akkor annyit healel, ha tul healelné akkor vissza állítja a currentHP-t a max HP-ra
	 Utáa végig megy az összes debuffon és ha HOT tipusu, akkor ugyanugy kiszámolja a DMG-et mint a többi sebzés esetben a defense-el és ki is vonja az életből. Ha túl sebezné 0ra állítja az életet
	 */
	public func calculateStatusEffectsEffect(battleId: String? = nil) {
		for buff in currentBuffs {
			switch buff.ultimate.effectType {
			case .HOT:
				let healValue = Int((Double(buff.casterBaseValue!) * buff.ultimate.value!).rounded())
				let finalHeal = self.applyHeal(heal: healValue, randomModifier: buff.casterRandomModifier)
                
                buff.caster?.statModel.heal += finalHeal
                
				if let battleId = battleId {
					BattleLogger.shared.log("       - target: \(self.getHPDescription()), \(buff.ultimate.name), 🟢 HEAL:  \(finalHeal), Remaining duration: \(buff.remainingDuration)", battleId: battleId)
				}
			default:
				break;
			}
		}
		
		if self.currentHP > self.hp { self.currentHP = self.hp }
		
		for debuff in currentDebuffs {
			switch debuff.ultimate.effectType {
			case .DOT:
				var defenseMultiplier = 1.0
				
				switch debuff.ultimate.valueType {
				case .physical:
					defenseMultiplier = self.getDefenseMultiplier(for: .phy)
				case .magical:
					defenseMultiplier = self.getDefenseMultiplier(for: .mgc)
				default:
					break
				}
				
				let power = Double(debuff.casterBaseValue!) * defenseMultiplier
				let realPower = power < 0 ? 0 : power
				let value = -Int((realPower * debuff.ultimate.value!).rounded())
				let finalDamage = self.applyDamage(damage: value, randomModifier: debuff.casterRandomModifier)
                
                statModel.damageTaken += finalDamage
                debuff.caster?.statModel.damageDealt += finalDamage
				
				if let battleId = battleId {
					BattleLogger.shared.log("       - target: \(self.getHPDescription()), \(debuff.ultimate.name), 🟠 DMG:  \(finalDamage), Remaining duration: \(debuff.remainingDuration)", battleId: battleId)
				}
			default:
				break;
			}
		}
		
		if self.currentHP < 0 { self.currentHP = 0 }
	}
	
	public func calculatePowerUpApply(effect: StatusEffect)  {
		switch effect.ultimate.valueType {
		case .phyChange:
			let powerUpValue = effect.ultimate.value! + (Double(effect.casterLevel) * Constants.example_phyPowerUpValueLVLMultiplier)
			self.currentPhy += Int(powerUpValue.rounded())
			effect.powerModifierValue = Int(powerUpValue.rounded())
		case .mgcChange:
			let powerUpValue = effect.ultimate.value! +  (Double(effect.casterLevel) * Constants.example_mgcPowerUpValueLVLMultiplier)
			self.currentMgc += Int(powerUpValue.rounded())
			effect.powerModifierValue = Int(powerUpValue.rounded())
		case .damageDealt, .damageTaken:
			break
		default:
			fatalError("Ilyen tipusnak nem kellene jelentkeznie.")
		}
	}
	
	public func calculatePowerUpExpiry(effect: StatusEffect)  {
		switch effect.ultimate.valueType {
		case .phyChange:
			self.currentPhy -= effect.powerModifierValue!
		case .mgcChange:
			self.currentMgc -= effect.powerModifierValue!
		case .damageDealt, .damageTaken:
			break
		default:
			fatalError("Ilyen tipusnak nem kellene jelentkeznie.")
		}
	}
	
	public func calculatePowerDownApply(effect: StatusEffect)  {
		switch effect.ultimate.valueType {
		case .phyChange:
			let powerDownValue = effect.ultimate.value! + (Double(effect.casterLevel) * Constants.example_phyPowerDownValueLVLMultiplier)
			let modifiedStat = Double(self.currentPhy) - powerDownValue
			
			if modifiedStat < 0 {
				let realPowerDownValue = powerDownValue + modifiedStat
				self.currentPhy -= Int(realPowerDownValue.rounded())
				effect.powerModifierValue = Int(realPowerDownValue.rounded())
			} else {
				self.currentPhy -= Int(powerDownValue.rounded())
				effect.powerModifierValue = Int(powerDownValue.rounded())
			}
		case .mgcChange:
			let powerDownValue = effect.ultimate.value! + (Double(effect.casterLevel) * Constants.example_mgcPowerDownValueLVLMultiplier)
			let modifiedStat = Double(self.currentMgc) - powerDownValue
			
			if modifiedStat < 0 {
				let realPowerDownValue = powerDownValue + modifiedStat
				self.currentMgc -= Int(realPowerDownValue.rounded())
				effect.powerModifierValue = Int(realPowerDownValue.rounded())
			} else {
				self.currentMgc -= Int(powerDownValue.rounded())
				effect.powerModifierValue = Int(powerDownValue.rounded())
			}
		case .armorChange:
			let powerDownValue = effect.ultimate.value! + (Double(effect.casterLevel) * Constants.example_armorPowerDownValueLVLMultiplier)
			let modifiedStat = Double(self.currentArmor) - powerDownValue
			
			if modifiedStat < 0 {
				let realPowerDownValue = powerDownValue + modifiedStat
				self.currentArmor -= Int(realPowerDownValue.rounded())
				effect.powerModifierValue = Int(realPowerDownValue.rounded())
			} else {
				self.currentArmor -= Int(powerDownValue.rounded())
				effect.powerModifierValue = Int(powerDownValue.rounded())
			}
		case .magicResistChange:
			let powerDownValue = effect.ultimate.value! + (Double(effect.casterLevel) * Constants.example_magicResistPowerDownValueLVLMultiplier)
			let modifiedStat = Double(self.currentMR) - powerDownValue
			
			if modifiedStat < 0 {
				let realPowerDownValue = powerDownValue + modifiedStat
				self.currentMR -= Int(realPowerDownValue.rounded())
				effect.powerModifierValue = Int(realPowerDownValue.rounded())
			} else {
				self.currentMR -= Int(powerDownValue.rounded())
				effect.powerModifierValue = Int(powerDownValue.rounded())
			}
		case .damageDealt, .damageTaken:
			break
		default:
			fatalError("Ilyen tipusnak nem kellene jelentkeznie.")
		}
	}
	
	public func calculatePowerDownExpiry(effect: StatusEffect)  {
		switch effect.ultimate.valueType {
		case .phyChange:
			self.currentPhy += effect.powerModifierValue!
		case .mgcChange:
			self.currentMgc += effect.powerModifierValue!
		case .armorChange:
			self.currentArmor += effect.powerModifierValue!
		case .magicResistChange:
			self.currentMR += effect.powerModifierValue!
		case .damageDealt, .damageTaken:
			break
		default:
			fatalError("Ilyen tipusnak nem kellene jelentkeznie.")
		}
	}
	
	public func removeStatusEffects() {
		currentBuffs = []
		currentDebuffs = []
	}
	
	public func isThereStatusEffects() -> Bool {
		let statusEffectCount = currentBuffs.count + currentDebuffs.count
		
		return statusEffectCount != 0
	}
	
	public func isThereDamageDealtIncreaseSpell() -> Bool {
		for buff in currentBuffs {
			if buff.ultimate.effectType == .damageModifierUp && buff.ultimate.valueType == .damageDealt { return true }
		}
		
		return false
	}
	
	public func isThereDamageDealtDecreaseSpell() -> Bool {
		for debuff in currentDebuffs {
			if debuff.ultimate.effectType == .damageModifierDown && debuff.ultimate.valueType == .damageDealt { return true }
		}
		
		return false
	}
	
	public func isThereDamageTakenDecreaseSpell() -> Bool {
		for buff in currentBuffs {
			if buff.ultimate.effectType == .damageModifierDown && buff.ultimate.valueType == .damageTaken { return true }
		}
		
		return false
	}
	
	public func isThereHasteRegenerationBlockDOT() -> Bool {
		for debuff in currentDebuffs {
			if debuff.ultimate.effectType == .hasteRegenerationBlock { return true }
		}
		
		return false
	}
	
	public func isThereStunDOT() -> Bool {
		for debuff in currentDebuffs {
			if debuff.ultimate.effectType == .stun { return true }
		}
		
		return false
	}
	
	public func getBiggestCC() -> StatusEffect? {
		return currentDebuffs.filter { $0.ultimate.effectType == .stun }.sorted{ $0.remainingDuration > $1.remainingDuration }.first
	}
	
	public func getRandomModifier() -> Double {
		let range = lowerModifier...upperModifier
		let randomNumber = Int.random(in: range)
		let modifier: Double = (100.0 + Double(randomNumber)) / 100.0
		
		return modifier
	}
	
	public func getBaseDescription() -> String {
		return "\(name)(HP: \(hp)/\(currentHP), HST: \(Constants.hasteConstantForUlt)/\(currentHaste))"
	}
	
	public func getGeneralDescription() -> String {
		return "\(isTeamA ? "▫️" : "◾️")\(name)(HP: \(hp)/\(currentHP), HST: \(Constants.hasteConstantForUlt)/\(currentHaste))"
	}
	
	func getHPDescription() -> String {
		return "\(isTeamA ? "▫️" : "◾️")\(self.name)(Remaining hp: \(self.hp)/\(self.currentHP), absorb: \(self.currentAbsorb))"
	}
	
	func getHPDescriptionWithCCDuration() -> String {
		if let cc = getBiggestCC() {
			return "\(isTeamA ? "▫️" : "◾️")\(String(describing: self.getHPDescription)), [CC: (\(cc.ultimate.name), Remaining duration: \(cc.remainingDuration))]"
		} else {
			fatalError("It shouldn't be..")
			// return "\(isTeamA ? "▫️" : "◾️")\(self.name)(Remaining hp: \(self.hp)/\(self.currentHP), absorb: \(self.currentAbsorb))"
		}
	}
}

public enum EntityDefenseType {
	case phy, mgc
}

extension SimulationEntity: CustomDebugStringConvertible {
	public var debugDescription: String {
		return "ENTITY(\(self.name), \(self.level))"
	}
}
