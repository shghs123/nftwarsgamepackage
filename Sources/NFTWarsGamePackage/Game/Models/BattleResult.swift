//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 09. 27..
//

import Foundation

public class BattleResult {
	
	public var isTeamAWon: Bool?
	public var roundCount: Int
    public var roundsStates: [(Int, Int)]
    public var teamAStats: [EntityStatModel]
    public var teamBStats: [EntityStatModel]
	
    init(isTeamAWon: Bool?, roundCount: Int, roundStates: [(Int, Int)], teamAStats: [EntityStatModel], teamBStats: [EntityStatModel]) {
		self.isTeamAWon = isTeamAWon
		self.roundCount = roundCount
        self.roundsStates = roundStates
        self.teamAStats = teamAStats
        self.teamBStats = teamBStats
	}
}
