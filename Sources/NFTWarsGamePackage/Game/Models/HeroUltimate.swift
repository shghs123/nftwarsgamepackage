//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation


public class HeroUltimate: Codable {
	
	public let id: Int
	public let name: String
	public let target: HeroUltimateTarget // ulti targete
	public let type: HeroUltimateType // Ulti tipusa
	public let effectType: HeroUltimateEffectType // A hatás tipusa ha a HeroUltimateType buff, debuff vagy cc
	public let valueType: HeroUltimateValueType // A value, érték típusa
	public var value: Double? // Az ulti sebzés szorzoja, vagy konstants dmg érték
	public var duration: Int? // A hatás időtartama (körök száma)
	public var onceCast: Bool
	
	init(id: Int, name: String, target: HeroUltimateTarget, type: HeroUltimateType, effectType: HeroUltimateEffectType = .none, valueType: HeroUltimateValueType = .none, value: Double? = nil, duration: Int? = nil, onceCast: Bool = false ) {
		self.id = id
		self.name = name
		self.target = target
		self.effectType = effectType
		self.type = type
		self.valueType = valueType
		self.value = value
		self.duration = duration
		self.onceCast = onceCast
	}
	
	
	// MARK: - Helpers
	
	public enum HeroUltimateTarget: String, Codable {
		case all, allEnemy, allFriendly, allRandom
		case singleFirstEnemy, singleRandomEnemy, mostBuffedEnemy, singleLastEnemy
		case singleFirstFriendly, singleRandomFriendly, singleLowestCurrentHPFriendly, `self`, selfUnder30, mostDebuffedFriendly
		case singleEnemyRandomBelow20
	}
	
	public enum HeroUltimateType: String, Codable {
		case damage, heal, lifeSteal, trueDmg, maxHPDmg, currentHPDmg, missingHPDmg, buff, debuff, tactical, absorb
	}
	
	// HOT, DOT az kör végén lép életbe, a többi pedig instant
	public enum HeroUltimateEffectType: String, Codable {
		case DOT, HOT, powerUp, powerDown, none
		case damageModifierUp, damageModifierDown
		case hasteToZero, hasteRegenerationBlock
		case reorderEnemyTeam
		case cleanse, dispell
		case stun
	}
	
	public enum HeroUltimateValueType: String, Codable {
		case physical, magical, mgcPhyMixed, trueDmg, none
		case phyChange, mgcChange, armorChange, magicResistChange
		case damageDealt, damageTaken, absorbUp
	}
	
	public func isItDamageDealtIncreaseSpell() -> Bool {
		if self.effectType == .damageModifierUp && self.valueType == .damageDealt { return true }
		return false
	}
	
	public func isItDamageTakenDecreaseSpell() -> Bool {
		if self.effectType == .damageModifierDown && self.valueType == .damageTaken { return true }
		return false
	}
	
	public func isItDamageDealtDecreaseSpell() -> Bool {
		if self.effectType == .damageModifierDown && self.valueType == .damageDealt { return true }
		return false
	}
	
	public func getEncodedModel() -> [String: Any]? {
		do {
			let data = try JSONEncoder().encode(self)
			
			guard let ultimateData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
				print("🔴 Error converting HeroUltimate to dictionary.")
				return nil
			}

			return ultimateData
		} catch {
			print("🔴 Error encoding HeroUltimate: \(error)")
			return nil
		}
	}
}

