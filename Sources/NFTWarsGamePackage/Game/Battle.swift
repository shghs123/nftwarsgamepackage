//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation

public class Battle {
	
	var teamA: [SimulationEntity]
	var teamB: [SimulationEntity]
	
	public var round = 0
    public var roundStateArray: [(Int, Int)] = []
	var battleId: String
	public var completion: ((BattleResult) -> Void)?
	
	public init(teamA: [SimulationEntity], teamB: [SimulationEntity], gameId: String? = nil, completion: ((BattleResult) -> Void)? = nil ) {
		for (index, hero) in teamA.enumerated() {
			hero.isTeamA = true
			hero.teamPosition = index
		}
		
		for (index, hero) in teamB.enumerated() {
			hero.isTeamA = false
			hero.teamPosition = index
		}
		
		if let gameId = gameId {
			self.battleId = gameId
		} else {
			self.battleId = UUID().uuidString
		}
		
		
		self.teamA = teamA
		self.teamB = teamB
		
		self.completion = completion
	}
	
	public func startBattle() {
		BattleLogger.shared.startLog(battleId: battleId)
		BattleLogger.shared.log("🟩 Battle started: id: #\(battleId) @\(Date())", battleId: battleId)
		
		BattleLogger.shared.log("🔵 Starting Attack Order:", battleId: battleId)
		let heroesInAttackOrder = (teamA + teamB).sorted(by: { $0.haste > $1.haste }).filter({ $0.currentHP > 0 })
		for attacker in heroesInAttackOrder { BattleLogger.shared.log("    -\(attacker.getGeneralDescription()), HST gain: \(attacker.haste)", battleId: battleId) }

		calculateNextRound()
	}
	
    public func calculateNextRound(manualNext: Bool = false) {
        while true {
            round += 1
            roundStartEffects()
            
            // Count hero HPs for battle state stat
            if round == 1 || round % 10 == 0 {
                let battleState: (Int, Int) = (teamA.map({ $0.currentHP }).reduce(0, +), teamB.map({ $0.currentHP }).reduce(0, +))
                roundStateArray.append(battleState)
            }

            let allHeroes = teamA + teamB
            let attackOrder = getAliveAttackOrder(heroes: allHeroes)

            for attacker in attackOrder {
                if attacker.currentHP < 1 {
                    BattleLogger.shared.log("  ☠️ \(attacker.getHPDescription()) is already dead, can't attack.", battleId: battleId)
                    continue
                }

                attacker.statModel.roundSurvived += 1

                if attacker.isThereStunDOT() {
                    BattleLogger.shared.log("  ⛓ \(attacker.getHPDescriptionWithCCDuration()), is paralysed, can't attack.", battleId: battleId)
                    continue
                }

                if attacker.currentHaste >= Constants.hasteConstantForUlt {
                    let shouldLowerHaste = castUltimateAttack(caster: attacker)
                    if shouldLowerHaste {
                        attacker.currentHaste -= Constants.hasteConstantForUlt
                    } else {
                        if let target = getTargetForBaseAttack(attacker: attacker) {
                            let _ = useBaseAttack(caster: attacker, target: target)
                        }
                    }
                } else {
                    if let target = getTargetForBaseAttack(attacker: attacker) {
                        let _ = useBaseAttack(caster: attacker, target: target)
                    }
                }
            }

            roundEndEffects()

            if checkIsBattleEnded() || manualNext {
                return
            }
        }
    }
	
	// MARK: - Attack
	
	/**
		Először megkeressük a megfelelő targeteket, ha nincs akkor egyből mehet a skip ult,
		Utána csökkentjük vagy növeljük képességtől függően a megfelelő target(ek) életét
		Aztán megvizsgáljük h a lehetségesnél több-e vagy kevesebb-e a target élete és ha szükséges korrigáljuk
		Ha elkasztolta az ultit akkor levonodik a Haste-ből a UltCastHasteConstant
		Return: Hogy elkasztolta-e az ultit
	 */
	public func castUltimateAttack(caster: SimulationEntity) -> Bool {
		BattleLogger.shared.log("  🪄 \(caster.getGeneralDescription()) cast ULT - \(caster.ulti.name): ", battleId: battleId)
		
		let targets = getTargetForUlt(caster: caster)
		
		if targets.count == 0 {
			BattleLogger.shared.log("    - No target, skipped ult", battleId: battleId)
			return false
		}
		
		if let onceCastedUltCasted = caster.onceCastUltCasted, onceCastedUltCasted { return false }
        
        caster.statModel.ultimateUsed += 1
		
		switch caster.ulti.type {
		case .damage, .heal, .lifeSteal, .trueDmg, .maxHPDmg, .currentHPDmg, .missingHPDmg:
			for target in targets {
				let hpChangeValue = getHealthChangeValue(caster: caster, target: target)
				applyHPChange(caster: caster, target: target, value: hpChangeValue)
				
				if target.currentHP > target.hp { target.currentHP = target.hp }
				if target.currentHP < 0 { target.currentHP = 0 }
				if caster.currentHP > caster.hp { caster.currentHP = caster.hp }
			}
		case .buff:
			for target in targets {
                let effect = StatusEffect(effect: caster.ulti, casterLevel: caster.level, casterRandomModifier: caster.getRandomModifier(), caster: caster)
				
				switch caster.ulti.valueType {
				case .physical:
					effect.casterBaseValue = caster.currentPhy
				case .magical:
					effect.casterBaseValue = caster.currentMgc
				default:
					break
				}
				
				target.applyBuff(newEffect: effect, battleId: battleId)
			}
		case .debuff:
			for target in targets {
                let effect = StatusEffect(effect: caster.ulti, casterLevel: caster.level, casterRandomModifier: caster.getRandomModifier(), caster: caster)
				
				switch caster.ulti.valueType {
				case .physical:
					effect.casterBaseValue = caster.currentPhy
				case .magical:
					effect.casterBaseValue = caster.currentMgc
				default:
					break
				}
				
				target.applyDebuff(newEffect: effect, battleId: battleId)
			}
		case .tactical:
			switch caster.ulti.effectType {
			case .hasteToZero:
				for target in targets {
					target.currentHaste = 0
					BattleLogger.shared.log("       - target: \(target.getHPDescription()), ultimate appied:  \(caster.ulti.name)", battleId: battleId)

				}
			case .reorderEnemyTeam:
				var possiblePositions: [Int] = Array(0..<targets.count)
				possiblePositions.shuffle()
				
				for index in 0..<targets.count {
					targets[index].teamPosition = possiblePositions[index]
				}
			case .cleanse:
				for target in targets {
					for debuff in target.currentDebuffs {
						removeDebuff(debuff: debuff, from: target)
					}
				}
			case .dispell:
				for target in targets {
					for buff in target.currentBuffs {
						removeBuff(buff: buff, from: target)
					}
				}
			default:
				fatalError("Ilyennek nem kellene léteznie")
			}
		case .absorb:
			switch caster.ulti.valueType {
			case .absorbUp:
				for target in targets {
					target.currentAbsorb += Int(caster.ulti.value!)
					BattleLogger.shared.log("       - target: \(target.getHPDescription()), Current absorb \(target.currentAbsorb), ultimate applied:  \(caster.ulti.name)", battleId: battleId)
				}
			default:
				fatalError("Ilyen eset nem létezik.")
			}
		default:
			fatalError("No ult type for this")
		}
		
		if caster.ulti.onceCast == true {
			caster.onceCastUltCasted = true
		}
		
		return true
	}
	
	/***
		Először csökkentjük a támado PHY értékét a  vedekezo.defensePowerrel és aztán jön rá a 0.1-es fix BaseAttack szorzó
	 */
	public func useBaseAttack(caster: SimulationEntity, target: SimulationEntity) -> Int {
		BattleLogger.shared.log("  ⚔️ \(caster.getGeneralDescription()) BaseAttacks:", battleId: battleId)
		
		let defenseMultiplier = target.getDefenseMultiplier(for: .phy)
		let reducedAttackPower = Double(caster.currentPhy) * defenseMultiplier
		let realReducedAttackPower = reducedAttackPower < 0 ? 0 : reducedAttackPower

		let realDamage = Int((realReducedAttackPower * Constants.baseAttackMultiplier).rounded())
		let hpChangeValue = HealthChangeValue(damage: -realDamage)
		applyHPChange(caster: caster, target: target, value: hpChangeValue)
		
		if target.currentHP < 0 { target.currentHP = 0 }
		
		return realDamage
	}
	
	//MARK: - Effects
	
	
	public func roundStartEffects() {
		BattleLogger.shared.log("🟦 ROUND #\(round) start 🟦", battleId: battleId)
		
		calculateHastes(heroes: teamA + teamB)
	}
	
	public func roundEndEffects() {
		calculateStatusEffectsEffect()
		reduceStatusEffectsDuration()
	}
	
	public func reduceStatusEffectsDuration() {
		let allHero = teamA + teamB
		
		for hero in allHero {
			if hero.currentHP < 1 { continue }
			
			for buff in hero.currentBuffs {
				buff.remainingDuration -= 1
				
				if buff.remainingDuration == 0 {
					removeBuff(buff: buff, from: hero)
				}
			}
			
			for debuff in hero.currentDebuffs {
				debuff.remainingDuration -= 1
				
				if debuff.remainingDuration == 0 {
					removeDebuff(debuff: debuff, from: hero)
				}
			}
		}
	}
	
	/**
	 Ha powerdown effect van a hősön akkor vissza állítja az alapján a stattokat normál állapotba.
	 Végül leveszi a debuffot a hősröl
	 powerUp effect nem kell mert debuff mint powerUp nincs sok értelme
	 */
	public func removeDebuff(debuff: StatusEffect, from hero: SimulationEntity) {
		if let index = hero.currentDebuffs.firstIndex(where: { $0.ultimate.id == debuff.ultimate.id }) {
			if debuff.ultimate.effectType == .powerDown {
				hero.calculatePowerDownExpiry(effect: debuff)
			}
            hero.currentDebuffs[index].caster = nil
			hero.currentDebuffs.remove(at: index)
		}
	}
	
	public func removeBuff(buff: StatusEffect, from hero: SimulationEntity) {
		if let index = hero.currentBuffs.firstIndex(where: { $0.ultimate.id == buff.ultimate.id }) {
			if buff.ultimate.effectType == .powerUp {
				hero.calculatePowerUpExpiry(effect: buff)
			}
			
            hero.currentBuffs[index].caster = nil
			hero.currentBuffs.remove(at: index)
		}
	}
	
	/**
	 A  halottaknak leszedi a status effectjeit
	 Csak akkor logol end turn effect ha bárkint van is statusEffect
	 Egyébként mindenkire ráhivja a hero.calculateStatusEffectet
	 */
	public func calculateStatusEffectsEffect() {
		let allHero = teamA + teamB
		
		for deadHero in allHero.filter({ $0.currentHP < 1 }) { deadHero.removeStatusEffects() }
		
		var isThereAnyStatusEffect = false
		for hero in allHero {
			if hero.isThereStatusEffects() {
				isThereAnyStatusEffect = true
			}
		}
		
		if isThereAnyStatusEffect {
			BattleLogger.shared.log("\n  ⌛️ End turn effects:", battleId: battleId)
		}
		
		for hero in allHero.filter({ $0.currentHP > 1 }) {
			hero.calculateStatusEffectsEffect(battleId: battleId)
		}
	}
	
	
	// MARK: - Helpers
	
	/**
	 Haste alapján rendezi be az attack sorrendet, és ignorálja a halottakat
	 */
	public func getAliveAttackOrder(heroes: [SimulationEntity]) -> [SimulationEntity] {
		let attackOrder = heroes.sorted(by: { $0.haste > $1.haste }).filter({ $0.currentHP > 0 })
		return attackOrder
	}
	
	/**
	 0 tól indulva minding kör elején hozzáadodik a Hős currenHastjéhez a Haste értéke
	 Ha elérte a hasteConstantot akkor nem adodik hozzá
	 */
	public func calculateHastes(heroes: [SimulationEntity]) {
		for hero in heroes {
			if hero.currentHaste >= Constants.hasteConstantForUlt { continue }
			if hero.currentHP < 1 { continue }
			if let onceCastedUltCasted = hero.onceCastUltCasted, onceCastedUltCasted { continue }
			if hero.isThereHasteRegenerationBlockDOT() { continue }
			if hero.isThereStunDOT() { continue }
				
			hero.currentHaste += hero.haste
		}
	}
	
	/**
	 Megnézi, hogy van e olyan csapat ahol mindenki halott, ha van akkor az a csapat veszített. Ha mindenki halott mindkét csapatban akkor döntetlen. Max 1000 kör, utána döntetlen
	 */
	public func checkIsBattleEnded() -> Bool {
		if !isAnyoneAlive(team: teamA) && !isAnyoneAlive(team: teamB) {
			BattleLogger.shared.log("🟩 It's a draw.", battleId: battleId)
			describeTeam(team: teamA)
			describeTeam(team: teamB)
			
			BattleLogger.shared.saveLog(battleId: battleId)
			
            let battleState: (Int, Int) = (teamA.map({$0.currentHP}).reduce(0, +), teamB.map({$0.currentHP}).reduce(0, +))
            roundStateArray.append(battleState)
            let result = BattleResult(isTeamAWon: nil, roundCount: round, roundStates: self.roundStateArray, teamAStats: teamA.map({$0.statModel}), teamBStats: teamB.map({$0.statModel}))
			completion?(result)
			return true
		}
		
		if !isAnyoneAlive(team: teamA) {
			BattleLogger.shared.log("🟩 Battle ended: TEAMB won.", battleId: battleId)
			describeTeam(team: teamA)
			describeTeam(team: teamB)
			
			BattleLogger.shared.saveLog(battleId: battleId)
			
            let battleState: (Int, Int) = (teamA.map({$0.currentHP}).reduce(0, +), teamB.map({$0.currentHP}).reduce(0, +))
            roundStateArray.append(battleState)
			let result = BattleResult(isTeamAWon: false, roundCount: round, roundStates: self.roundStateArray, teamAStats: teamA.map({$0.statModel}), teamBStats: teamB.map({$0.statModel}))
			completion?(result)
			return true
		}
		
		if !isAnyoneAlive(team: teamB) {
			BattleLogger.shared.log("🟩 Battle ended: TEAMA won.", battleId: battleId)
			describeTeam(team: teamA)
			describeTeam(team: teamB)
			
			BattleLogger.shared.saveLog(battleId: battleId)
			
            let battleState: (Int, Int) = (teamA.map({$0.currentHP}).reduce(0, +), teamB.map({$0.currentHP}).reduce(0, +))
            roundStateArray.append(battleState)
			let result = BattleResult(isTeamAWon: true, roundCount: round, roundStates: self.roundStateArray, teamAStats: teamA.map({$0.statModel}), teamBStats: teamB.map({$0.statModel}))
			completion?(result)
			return true
		}
		
		if round == Constants.gameMaxRounds {
			BattleLogger.shared.log("🟩 It's an OVERTIME draw.", battleId: battleId)
			describeTeam(team: teamA)
			describeTeam(team: teamB)
			
			BattleLogger.shared.saveLog(battleId: battleId)
			
            let battleState: (Int, Int) = (teamA.map({$0.currentHP}).reduce(0, +), teamB.map({$0.currentHP}).reduce(0, +))
            roundStateArray.append(battleState)
			let result = BattleResult(isTeamAWon: nil, roundCount: round, roundStates: self.roundStateArray, teamAStats: teamA.map({$0.statModel}), teamBStats: teamB.map({$0.statModel}))
			completion?(result)
			return true
		}
		
		return false
	}
	
	func describeTeam(team: [SimulationEntity]) {
		BattleLogger.shared.log("   Team\(team[0].isTeamA ? "A" : "B"):", battleId: battleId)
		for hero in team {
			BattleLogger.shared.log("     -\(hero.getGeneralDescription())", battleId: battleId)
		}
	}
	
	/**
	 Megnézi, hogy adott csapatban van e még élő hős
	 */
	public func isAnyoneAlive(team: [SimulationEntity]) -> Bool {
		if team.first(where: { $0.currentHP >= 1}) == nil {
			// Nincs élő hero
			return false
		} else {
			// Van élő hero
			return true
		}
	}
	
	/**
	 Csapat pozicio szerinti legkisebb (Vagyis aki éppen elől áll) élő hős-t adja vissza.
	 */
	public func getTargetForBaseAttack(attacker: SimulationEntity) -> SimulationEntity? {
		var enemies: [SimulationEntity] = []
		
		if attacker.isTeamA {
			enemies = teamB
		} else {
			enemies = teamA
		}
		
		// Sorbarendezzuk csapat pozicio szerint aztán kivesszuk az első nem halott elemet
		let target = enemies.sorted(by: { $0.teamPosition < $1.teamPosition }).first(where: { $0.currentHP > 0 })
		
		if target == nil {
			BattleLogger.shared.log("🟠 \(attacker.getHPDescription())can't attack, no enemy alive", battleId: battleId)
		}
		
		return target
	}
	
	public func getTargetForUlt(caster: SimulationEntity) -> [SimulationEntity] {
		switch caster.ulti.target {
		case .allEnemy:
			if caster.isTeamA {
				return teamB.filter({ $0.currentHP > 0 })
			} else {
				return teamA.filter({ $0.currentHP > 0 })
			}
		case .allFriendly:
			if caster.isTeamA {
				if caster.ulti.type == .heal {
					return teamA.filter({ $0.currentHP > 0 && $0.currentHP < $0.hp })
				}
				
				return teamA.filter({ $0.currentHP > 0 })
			} else {
				if caster.ulti.type == .heal {
					return teamB.filter({ $0.currentHP > 0 && $0.currentHP < $0.hp })
				}
				
				return teamB.filter({ $0.currentHP > 0 })
			}
		case .singleFirstEnemy:
			if caster.isTeamA {
				let target = teamB.sorted(by: { $0.teamPosition < $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				let target = teamA.sorted(by: { $0.teamPosition < $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .singleLastEnemy:
			if caster.isTeamA {
				let target = teamB.sorted(by: { $0.teamPosition > $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				let target = teamA.sorted(by: { $0.teamPosition > $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .singleLowestCurrentHPFriendly:
			if caster.isTeamA {
				if caster.ulti.type == .heal || caster.ulti.effectType == .HOT || caster.ulti.type == .absorb {
					let target = teamA.sorted(by: { $0.currentHP < $1.currentHP }).first(where: { $0.currentHP > 0 && $0.currentHP < $0.hp })
					return [target].compactMap({ $0 })
				}
				
				let target = teamA.sorted(by: { $0.currentHP < $1.currentHP }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				if caster.ulti.type == .heal || caster.ulti.effectType == .HOT || caster.ulti.type == .absorb {
					let target = teamB.sorted(by: { $0.currentHP < $1.currentHP }).first(where: { $0.currentHP > 0 && $0.currentHP < $0.hp })
					return [target].compactMap({ $0 })
				}
				
				let target = teamB.sorted(by: { $0.currentHP < $1.currentHP }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .singleEnemyRandomBelow20:
			if caster.isTeamA {
				if let target = teamB.filter({ $0.currentHP > 0 }).filter({ Double($0.currentHP) <=  (Double($0.hp) * 0.2) }).randomElement() {
					return [target]
				}
				return []
			} else {
				if let target = teamA.filter({ $0.currentHP > 0 }).filter({ Double($0.currentHP) <=  (Double($0.hp) * 0.2) }).randomElement() {
					return [target]
				}
				return []
			}
		case .singleFirstFriendly:
			if caster.isTeamA {
				let target = teamA.sorted(by: { $0.teamPosition < $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				let target = teamB.sorted(by: { $0.teamPosition < $1.teamPosition }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .singleRandomEnemy:
			if caster.isTeamA {
				let enemy = teamB.filter({ $0.currentHP > 0 }).randomElement()
				return enemy != nil ? [enemy!] : []
			} else {
				let enemy = teamA.filter({ $0.currentHP > 0 }).randomElement()
				return enemy != nil ? [enemy!] : []
			}
		case .mostBuffedEnemy:
			if caster.isTeamA {
				if teamB.reduce(0, { $0 + $1.currentBuffs.count }) == 0 { return [] }
				
				let target = teamB.sorted(by: { $0.currentBuffs.count > $1.currentBuffs.count }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				if teamA.reduce(0, { $0 + $1.currentBuffs.count }) == 0 { return [] }
				
				let target = teamA.sorted(by: { $0.currentBuffs.count > $1.currentBuffs.count }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .mostDebuffedFriendly:
			if caster.isTeamA {
				if teamA.reduce(0, { $0 + $1.currentDebuffs.count }) == 0 { return [] }
				
				let target = teamA.sorted(by: { $0.currentDebuffs.count > $1.currentDebuffs.count }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			} else {
				if teamB.reduce(0, { $0 + $1.currentDebuffs.count }) == 0 { return [] }
				
				let target = teamB.sorted(by: { $0.currentDebuffs.count > $1.currentDebuffs.count }).first(where: { $0.currentHP > 0 })
				return [target].compactMap({ $0 })
			}
		case .`self`:
			return [caster]
		case .selfUnder30:
			return Double(caster.currentHP) <=  (Double(caster.hp) * 0.3) ? [caster] : []
		default:
			fatalError("🔴 Unhandled case in: getTargetForUlt()")
		}
	}
	
	public func getHealthChangeValue(caster: SimulationEntity, target: SimulationEntity) -> HealthChangeValue {
		var hpChangeValue = HealthChangeValue()
		
		switch caster.ulti.valueType {
		case .physical:
			switch caster.ulti.type {
			case .damage:
				// Először csökkentjük a támado PHY értékét a védekező.defensePowerrel-el és aztán jön rá spell szorzó

				let defenseMultiplier = target.getDefenseMultiplier(for: .phy)
				let attackPower = Double(caster.currentPhy) * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower * caster.ulti.value!).rounded())
				
				hpChangeValue.damage = -value
			case .heal:
				fatalError("No Physical heal in the game, yet.")
			case .lifeSteal:
				// Először csökkentjük a támado PHY értékét a védekező.defensePowerrel-el és aztán jön rá spell szorzó
				
				let defenseMultiplier = target.getDefenseMultiplier(for: .phy)
				let attackPower = Double(caster.currentPhy) * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower * caster.ulti.value!).rounded())
				
				hpChangeValue.lifeSteal = value
			case .maxHPDmg:
				// Csökkentjük a támado értéket(vedekezo.maxHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .phy)
				let attackPower = Double(target.hp) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			case .currentHPDmg:
				// Csökkentjük a támado értéket(vedekezo.currentHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .phy)
				let attackPower = Double(target.currentHP) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			case .missingHPDmg:
				// Csökkentjük a támado értéket(vedekezo.missingHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .phy)
				let defenderMissingHP = target.hp - target.currentHP
				let realDefenderMissingHP = defenderMissingHP < 0 ? 0 : defenderMissingHP
				let attackPower = Double(realDefenderMissingHP) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			default:
				fatalError("🔴 Unhandled case in: getHealthChangeValue(), attacker.ulti.type")
			}
		case .magical:
			switch caster.ulti.type {
			case .damage:
				// Először csökkentjük a támado MAGIC értékét a védekező.defensePowerrel-el és aztán jön rá spell szorzó
				let defenseMultiplier = target.getDefenseMultiplier(for: .mgc)
				let magicPower = Double(caster.currentMgc) * defenseMultiplier
				let realMagicPower = magicPower < 0 ? 0 : magicPower
				let value = Int((realMagicPower * caster.ulti.value!).rounded())
				
				hpChangeValue.damage = -value
			case .heal:
				let healValue = Int((Double(caster.currentMgc) * caster.ulti.value!).rounded())
				hpChangeValue.heal = healValue
			case .lifeSteal:
				// Először csökkentjük a támado MAGIC értékét a védekező.defensePowerrel-el és aztán jön rá spell szorzó
				let defenseMultiplier = target.getDefenseMultiplier(for: .mgc)
				let magicPower = Double(caster.currentMgc) * defenseMultiplier
				let realMagicPower = magicPower < 0 ? 0 : magicPower
				let value = Int((realMagicPower * caster.ulti.value!).rounded())
				
				hpChangeValue.lifeSteal = value
			case .maxHPDmg:
				// Csökkentjük a támado értéket(vedekezo.maxHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .mgc)
				let attackPower = Double(target.hp) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			case .currentHPDmg:
				// Csökkentjük a támado értéket(vedekezo.currentHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .mgc)
				let attackPower = Double(target.currentHP) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			case .missingHPDmg:
				// Csökkentjük a támado értéket(vedekezo.missingHP*spell szorzó)-t a védekező.defensePowerrel-el
				let defenseMultiplier =  target.getDefenseMultiplier(for: .mgc)
				let defenderMissingHP = target.hp - target.currentHP
				let realDefenderMissingHP = defenderMissingHP < 0 ? 0 : defenderMissingHP
				let attackPower = Double(realDefenderMissingHP) * caster.ulti.value! * defenseMultiplier
				let realAttackPower = attackPower < 0 ? 0 : attackPower
				let value = Int((realAttackPower).rounded())
				
				hpChangeValue.damage = -value
			default:
				fatalError("🔴 Unhandled case in: getHealthChangeValue(), attacker.ulti.type")
			}
		case .mgcPhyMixed:
			switch caster.ulti.type {
			case .damage:
				// // Először csökkentjük a támado MAGIC értékét a védekező.defensePowerrel-el és aztán jön rá (spell szorzó/2), aztán csökkentjük a támado PHY értékét a védekező.defensePowerrel-el és aztán jön rá (spell szorzó/2)
				let magicDefenseMultiplier = target.getDefenseMultiplier(for: .mgc)
				let magicPower = Double(caster.currentMgc) * magicDefenseMultiplier
				let realMagicPower = magicPower < 0 ? 0 : magicPower
				let magicValue = Int((realMagicPower * (caster.ulti.value!)/2).rounded())
				
				let physicalDefenseMultiplier = target.getDefenseMultiplier(for: .phy)
				let physicalPower = Double(caster.currentPhy) * physicalDefenseMultiplier
				let realPhysicalPower = physicalPower < 0 ? 0 : physicalPower
				let physicalValue = Int((realPhysicalPower * (caster.ulti.value!)/2).rounded())
				
				let value = magicValue + physicalValue
				
				hpChangeValue.damage = -value
			default:
				fatalError("🔴 Unhandled case in: getHealthChangeValue(), attacker.ulti.type")
			}
		case .trueDmg:
			let trueDamage = caster.ulti.value! + (Double(caster.level) * Constants.example_trueDamageStrikeValueLVLMultiplier)
			let value = trueDamage < 0 ? 0 : trueDamage
			
			hpChangeValue.damage = Int(-value.rounded())
		default:
			fatalError("🔴 Unhandled case in: getHealthChangeValue(), attacker.ulti.multiplierType")
		}
		
		return hpChangeValue
	}
	
	public func applyHPChange(caster: SimulationEntity, target: SimulationEntity, value: HealthChangeValue) {
		var value = value
		if caster.isThereDamageDealtIncreaseSpell() {
			for buff in caster.currentBuffs {
				if buff.ultimate.isItDamageDealtIncreaseSpell() {
					value.damage = Int((Double(value.damage) * (1.0 + buff.ultimate.value!)).rounded())
					value.lifeSteal = Int((Double(value.lifeSteal) * (1.0 + buff.ultimate.value!)).rounded())
				}
			}
		}
		
		if caster.isThereDamageDealtDecreaseSpell() {
			for debuff in caster.currentDebuffs {
				if debuff.ultimate.isItDamageDealtDecreaseSpell() {
					value.damage = Int((Double(value.damage) * (1.0 - debuff.ultimate.value!)).rounded())
					value.lifeSteal = Int((Double(value.lifeSteal) * (1.0 - debuff.ultimate.value!)).rounded())
				}
			}
		}
		
		if target.isThereDamageTakenDecreaseSpell() {
			for buff in target.currentBuffs {
				if buff.ultimate.isItDamageTakenDecreaseSpell() {
					value.damage = Int((Double(value.damage) * (1.0 - buff.ultimate.value!)).rounded())
					value.lifeSteal = Int((Double(value.lifeSteal) * (1.0 - buff.ultimate.value!)).rounded())
				}
			}
		}
		
		if value.damage != 0 {
			let damageValue = target.applyDamage(damage: value.damage, randomModifier: caster.getRandomModifier(), battleId: battleId)
            caster.statModel.damageDealt += damageValue
            target.statModel.damageTaken += damageValue
		}
		
		if value.heal != 0 {
			let healValue = target.applyHeal(heal: value.heal, randomModifier: caster.getRandomModifier(), battleId: battleId)
            caster.statModel.heal += healValue
		}
		
		if value.lifeSteal != 0 {
			// No battle ID so dmg and heal wont log, only life steal will log
			let finalHealValue = caster.applyHeal(heal: value.lifeSteal, randomModifier: caster.getRandomModifier())
			let finalDmgValue = target.applyDamage(damage: -value.lifeSteal, randomModifier: caster.getRandomModifier())
            
            caster.statModel.damageDealt += finalDmgValue
            caster.statModel.heal += finalHealValue
            target.statModel.damageTaken += finalDmgValue
			
			BattleLogger.shared.log("       - target: \(target.getHPDescription()), 🟡 LIFESTEAL:  healed \(finalHealValue), damaged \(finalDmgValue)", battleId: battleId)
		}
	}
}
