//
//  Constants.swift
//  NFTWarsFastSimulator
//
//  Created by Korsós Tibor on 2022. 12. 09..
//

import Foundation

public class Constants {
	
	// MARK: - Hero stat value constants
	static let healthConstant = 156
	static let armorConstant = 0
	static let magicResistConstant = 0
	static let phyConstant = 16
	static let mgcConstant = 16
	
	// MARK: - Spell constants
	
	// Instant spells
	public static let example_whirlwindMultiplier = 0.4
	public static let example_physicalStrikeMultiplier = 0.7
	public static let example_magicStrikeMultiplier = 0.8
	public static let example_mgcPhyStrikeMultiplier = 0.7
	public static let example_trueDamageStrikeValue = 175.0
	public static let example_trueDamageStrikeValueLVLMultiplier = 0.1
	public static let example_tornadoMultiplier = 0.4
	public static let example_healMultiplier = 0.7
	public static let example_AOEHealMultiplier = 0.15
	public static let example_magicLifeStealMultiplier = 0.4
	public static let example_physicalLifeStealMultiplier = 0.35
	public static let example_physicalMaxHPStrikeMultiplier = 0.02
	public static let example_magicMaxHPStrikeMultiplier = 0.02
	public static let example_physicalCurrentHPStrikeMultiplier = 0.03
	public static let example_magicCurrentHPStrikeMultiplier = 0.03
	public static let example_physicalMissingHPStrikeMultiplier = 0.05
	public static let example_magicMissingHPStrikeMultiplier = 0.05
	public static let example_executeMultiplier = Double(Int.max)
	public static let example_singleAbsorbValue = 100.0
	
	// OT Spells
	public static let example_physicalDOTMultiplier = 0.175
	public static let example_magicalDOTMultiplier = 0.175
	public static let example_magicalHOTMultiplier = 0.175
	public static let example_physicalAOEDOTMultiplier = 0.1
	public static let example_magicalAOEDOTMultiplier = 0.1
	public static let example_magicalAOEHOTMultiplier = 0.175
	
	// Power modification Spells
	public static let example_phyPowerUpValue = 100.0
	public static let example_phyPowerUpValueLVLMultiplier = 0.1
	public static let example_mgcPowerUpValue = 150.0
	public static let example_mgcPowerUpValueLVLMultiplier = 0.1
	public static let example_phyPowerDownValue = 50.0
	public static let example_phyPowerDownValueLVLMultiplier = 0.1
	public static let example_mgcPowerDownValue = 200.0
	public static let example_mgcPowerDownValueLVLMultiplier = 0.1
	public static let example_armorPowerDownValue = 50.0
	public static let example_armorPowerDownValueLVLMultiplier = 0.1
	public static let example_magicResistPowerDownValue = 50.0
	public static let example_magicResistPowerDownValueLVLMultiplier = 0.1
	public static let example_phyPowerUpAOEValue = 100.0
	public static let example_mgcPowerUpAOEValue = 150.0
	public static let example_phyPowerDownAOEValue = 50.0
	public static let example_mgcPowerDownAOEValue = 200.0
	public static let example_armorPowerDownAOEValue = 50.0
	public static let example_magicResistPowerDownAOEValue = 50.0
	public static let example_damageDealtReductionMultiplier = 0.2
	public static let example_damageDealtIncreaseMultiplier = 0.2
	public static let example_damageTakenReductionMultiplier = 0.2
	public static let example_enrageMultiplier = 1.0
	
	 // MARK: - Spell constants
	 
	 // Instant spells
	public static let whirlwindMultiplier = 0.3
	public static let physicalStrikeMultiplier = 0.7
	public static let magicStrikeMultiplier = 0.9
	public static let mgcPhyStrikeMultiplier = 0.7
	public static let trueDamageStrikeValue = 175.0
	public static let trueDamageStrikeValueLVLMultiplier = 0.1
	public static let tornadoMultiplier = 0.75
	public static let healMultiplier = 0.65
	public static let AOEHealMultiplier = 0.25
	public static let magicLifeStealMultiplier = 0.4
	public static let physicalLifeStealMultiplier = 0.35
	public static let physicalMaxHPStrikeMultiplier = 0.02
	public static let magicMaxHPStrikeMultiplier = 0.02
	public static let physicalCurrentHPStrikeMultiplier = 0.03
	public static let magicCurrentHPStrikeMultiplier = 0.03
	public static let physicalMissingHPStrikeMultiplier = 0.05
	public static let magicMissingHPStrikeMultiplier = 0.05
	public static let executeMultiplier = Double(Int.max)
	public static let singleAbsorbValue = 100.0
	 
	 // OT Spells
	public static let physicalDOTMultiplier = 0.175
	public static let magicalDOTMultiplier = 0.175
	public static let magicalHOTMultiplier = 0.175
	public static let physicalAOEDOTMultiplier = 0.1
	public static let magicalAOEDOTMultiplier = 0.1
	public static let magicalAOEHOTMultiplier = 0.175
	 
	 // Power modification Spells
	public static let phyPowerUpValue = 100.0
	public static let phyPowerUpValueLVLMultiplier = 0.1
	public static let mgcPowerUpValue = 150.0
	public static let mgcPowerUpValueLVLMultiplier = 0.1
	public static let phyPowerDownValue = 50.0
	public static let phyPowerDownValueLVLMultiplier = 0.1
	public static let mgcPowerDownValue = 200.0
	public static let mgcPowerDownValueLVLMultiplier = 0.1
	public static let armorPowerDownValue = 50.0
	public static let armorPowerDownValueLVLMultiplier = 0.1
	public static let magicResistPowerDownValue = 50.0
	public static let magicResistPowerDownValueLVLMultiplier = 0.1
	public static let phyPowerUpAOEValue = 100.0
	public static let mgcPowerUpAOEValue = 150.0
	public static let phyPowerDownAOEValue = 50.0
	public static let mgcPowerDownAOEValue = 200.0
	public static let armorPowerDownAOEValue = 50.0
	public static let magicResistPowerDownAOEValue = 50.0
	public static let damageDealtReductionMultiplier = 0.2
	public static let damageDealtIncreaseMultiplier = 0.2
	public static let damageTakenReductionMultiplier = 0.2
	public static let enrageMultiplier = 1.0
	
	// MARK: - Combat constants
	
	public static let baseAttackMultiplier = 0.2
	public static let defenseMultiplier = 1.5
	public static let hasteConstantForUlt = 600
	
	// Game constants
	public static let gameMaxRounds = 1000
}
