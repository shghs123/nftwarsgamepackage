//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation

public enum LogLevel {
	case none, both, file, console
}

public class BattleLogger {
	
	public static let shared: BattleLogger = BattleLogger()
	
	public var logLevel: LogLevel = .none
	private var logs: [String: String] = [:]
	
	func startLog(battleId: String) {
		if logLevel == .none { return }
		
		logs[battleId] = ""
	}
	
	func log(_ logString: String, battleId: String) {
		if logLevel == .none { return }
		
		if logLevel == .console || logLevel == .both {
			print(logString)
		}
		
		if logs[battleId] != nil {
			logs[battleId]! += "\n" + logString
		}
	}

	func saveLog(battleId: String) {
		if logLevel == .none || logLevel == .console {
			logs.removeValue(forKey: battleId)
			return
		}
		
		if let log = logs[battleId] {
			let file = "game_\(battleId)_log.txt"

			if let doc = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
				let dirPath = doc.appendingPathComponent("nftLogs")
				let fileURL = dirPath.appendingPathComponent(file)
				
				do {
					try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true)
					
					
					FileManager.default.createFile(atPath: fileURL.path, contents: nil)
					try log.write(to: fileURL, atomically: false, encoding: .utf8)
					logs.removeValue(forKey: battleId)
				} catch {
					print("🔴 Error saving log id: game_\(battleId): \(error.localizedDescription)")
				}
			}
			
			logs.removeValue(forKey: battleId)
		}
	}
	
	public func getSavePath(for battleId: String) -> URL? {
		let fileName = "game_\(battleId)_log.txt"
		
		if let doc = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
			let dirPath = doc.appendingPathComponent("nftLogs")
			let fileURL = dirPath.appendingPathComponent(fileName)
			
			return fileURL
		}
		
		return nil
	}
}
