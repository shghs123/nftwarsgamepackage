//
//  File.swift
//  
//
//  Created by Korsós Tibor on 2023. 07. 31..
//

import Foundation

public class Ability {
	
	/**
	 *************
	 BASE ABILITIES:
	 *************
	 */
	
	// Mark: - Base abilities:
	public static let whirlwind = HeroUltimate(id: 0, name: "Whirlwind", target: .allEnemy, type: .damage, valueType: .physical, value: 0.25)
	public static let smallWhirlwind = HeroUltimate(id: 0, name: "Small Whirlwind", target: .allEnemy, type: .damage, valueType: .physical, value: 0.15)
	public static let physicalStrike = HeroUltimate(id: 0, name: "Physical strike", target: .singleFirstEnemy, type: .damage, valueType: .physical, value: 0.85)
    public static let dummyPhysicalStrike = HeroUltimate(id: 0, name: "Physical strike", target: .singleFirstEnemy, type: .damage, valueType: .physical, value: 0.75)
	public static let bigPhysicalStrike = HeroUltimate(id: 0, name: "Big Physical strike", target: .singleFirstEnemy, type: .damage, valueType: .physical, value: 1.25)
	public static let smallPhysicalStrike = HeroUltimate(id: 0, name: "Small Physical strike", target: .singleFirstEnemy, type: .damage, valueType: .physical, value: 0.5)
    public static let magicStrike = HeroUltimate(id: 0, name: "Magic strike", target: .singleFirstEnemy, type: .damage, valueType: .magical, value: 1.0)
	public static let bigMagicStrike = HeroUltimate(id: 0, name: "Big Magic strike", target: .singleFirstEnemy, type: .damage, valueType: .magical, value: 1.4)
	public static let smallMagicStrike = HeroUltimate(id: 0, name: "Small Magic strike", target: .singleFirstEnemy, type: .damage, valueType: .magical, value: 0.6)
	public static let tornado = HeroUltimate(id: 0, name: "Tornado", target: .allEnemy, type: .damage, valueType: .magical, value: 0.90)
	public static let mgcPhyStrike = HeroUltimate(id: 0, name: "Mixed(mgc+phy) strike", target: .singleFirstEnemy, type: .damage, valueType: .mgcPhyMixed, value: 0.7)
    public static let bigMgcPhyStrike = HeroUltimate(id: 0, name: "Big Mixed(mgc+phy) strike", target: .singleFirstEnemy, type: .damage, valueType: .mgcPhyMixed, value: 2.1)
	public static let smallMgcPhyStrike = HeroUltimate(id: 0, name: "Small Mixed(mgc+phy) strike", target: .singleFirstEnemy, type: .damage, valueType: .mgcPhyMixed, value: 0.6)
	public static let mgcPhyAOE = HeroUltimate(id: 0, name: "Mixed(mgc+phy) AOE", target: .allEnemy, type: .damage, valueType: .mgcPhyMixed, value: 0.25)
	public static let physicalAmbush = HeroUltimate(id: 0, name: "Physical Ambush", target: .singleLastEnemy, type: .damage, valueType: .physical, value: 1.4)
	public static let magicAmbush = HeroUltimate(id: 0, name: "Magic Ambush", target: .singleLastEnemy, type: .damage, valueType: .magical, value: 1.0)
	public static let trueStrike = HeroUltimate(id: 0, name: "True strike", target: .singleFirstEnemy, type: .trueDmg, valueType: .trueDmg, value: 180.0)
	public static let trueAOE = HeroUltimate(id: 0, name: "True AOE", target: .allEnemy, type: .trueDmg, valueType: .trueDmg, value: 55.0)
	public static let magicLifeSteal = HeroUltimate(id: 0, name: "Magic Life Steal", target: .singleFirstEnemy, type: .lifeSteal, valueType: .magical, value: 0.3)
	public static let physicalLifeSteal = HeroUltimate(id: 0, name: "Physical Life Steal", target: .singleFirstEnemy, type: .lifeSteal, valueType: .physical, value: 0.25)
	public static let execute = HeroUltimate(id: 0, name: "Execute", target: .singleEnemyRandomBelow20, type: .trueDmg, valueType: .trueDmg, value: Double(Int.max))
	public static let physicalMaxHPStrike = HeroUltimate(id: 0, name: "Physical maxHP strike", target: .singleFirstEnemy, type: .maxHPDmg, valueType: .physical, value: 0.03)
	public static let magicMaxHPStrike = HeroUltimate(id: 0, name: "Magic maxHP strike", target: .singleFirstEnemy, type: .maxHPDmg, valueType: .magical, value: 0.02)
	public static let physicalCurrentHPStrike = HeroUltimate(id: 0, name: "Physical currentHP strike", target: .singleFirstEnemy, type: .currentHPDmg, valueType: .physical, value: 0.06)
	public static let magicCurrentHPStrike = HeroUltimate(id: 0, name: "Magic currentHP strike", target: .singleFirstEnemy, type: .currentHPDmg, valueType: .magical, value: 0.065)
	public static let physicalMissingHPStrike = HeroUltimate(id: 0, name: "Physical missingHP strike", target: .singleFirstEnemy, type: .missingHPDmg, valueType: .physical, value: 0.05)
	public static let magicMissingHPStrike = HeroUltimate(id: 0, name: "Magic missingHP strike", target: .singleFirstEnemy, type: .missingHPDmg, valueType: .magical, value: 0.075)
	
    public static let heal = HeroUltimate(id: 0, name: "Heal", target: .singleLowestCurrentHPFriendly, type: .heal, valueType: .magical, value: 1.45)
	public static let selfHeal = HeroUltimate(id: 0, name: "Self Heal", target: .`self`, type: .heal, valueType: .magical, value: 0.5)
	public static let heal2 = HeroUltimate(id: 0, name: "Heal", target: .singleLowestCurrentHPFriendly, type: .heal, valueType: .magical, value: 0.6)
	public static let smallHeal = HeroUltimate(id: 0, name: "Small Heal", target: .singleLowestCurrentHPFriendly, type: .heal, valueType: .magical, value: 0.4)
	public static let bigHeal = HeroUltimate(id: 0, name: "Big Heal", target: .singleLowestCurrentHPFriendly, type: .heal, valueType: .magical, value: 1.0)
	public static let AOEHeal = HeroUltimate(id: 0, name: "AOE Heal", target: .allFriendly, type: .heal, valueType: .magical, value: 0.3)
	public static let smallAOEHeal = HeroUltimate(id: 0, name: "Small AOE Heal", target: .allFriendly, type: .heal, valueType: .magical, value: 0.15)
	public static let singleAbsorb = HeroUltimate(id: 0, name: "Absorb", target: .singleFirstFriendly, type: .absorb, valueType: .absorbUp, value: 285)
	public static let singleAbsorb2 = HeroUltimate(id: 0, name: "Absorb2", target: .singleLowestCurrentHPFriendly, type: .absorb, valueType: .absorbUp, value: 300)
	public static let smallSingleAbsorb = HeroUltimate(id: 0, name: "Small Absorb", target: .singleLowestCurrentHPFriendly, type: .absorb, valueType: .absorbUp, value: 170)
	public static let bigSingleAbsorb = HeroUltimate(id: 0, name: "Big Absorb", target: .singleFirstFriendly, type: .absorb, valueType: .absorbUp, value: 500)
	
	// MARK: - OverTime abilities:
	 
	public static let physicalDOT = HeroUltimate(id: 0, name: "Physical Damage OT", target: .singleFirstEnemy, type: .debuff, effectType: .DOT, valueType: .physical, value: 0.6, duration: 5)
	public static let magicalDOT = HeroUltimate(id: 0, name: "Magical Damage OT", target: .singleFirstEnemy, type: .debuff, effectType: .DOT, valueType: .magical, value: 0.4, duration: 3)
	public static let magicalHOT = HeroUltimate(id: 0, name: "Magic heal OT", target: .singleLowestCurrentHPFriendly, type: .buff, effectType: .HOT, valueType: .magical, value: 0.155, duration: 4)
	public static let magicalHOTSelf = HeroUltimate(id: 0, name: "Magic heal OT Self", target: .`self`, type: .buff, effectType: .HOT, valueType: .magical, value: 0.1065, duration: 6)
	public static let physicalAOEDOT = HeroUltimate(id: 0, name: "Physical Damage AOE OT", target: .allEnemy, type: .debuff, effectType: .DOT, valueType: .physical, value: 0.1, duration: 4)
	public static let magicalAOEDOT = HeroUltimate(id: 0, name: "Magical Damage AOE OT", target: .allEnemy, type: .debuff, effectType: .DOT, valueType: .magical, value: 0.1, duration: 4)
	public static let magicalAOEHOT = HeroUltimate(id: 0, name: "Magic heal AOE OT", target: .allFriendly, type: .buff, effectType: .HOT, valueType: .magical, value: 0.15, duration: 3)
	 
	// MARK: - Status Effect abilities
	 
	public static let phyPowerUp = HeroUltimate(id: 0, name: "Phy PowerUp", target: .singleFirstFriendly, type: .buff, effectType: .powerUp, valueType: .phyChange, value: 100, duration: 4)
	public static let mgcPowerUp = HeroUltimate(id: 0, name: "Mgc PowerUp", target: .singleFirstFriendly, type: .buff, effectType: .powerUp, valueType: .mgcChange, value: 150, duration: 4)
	public static let phyPowerDown = HeroUltimate(id: 0, name: "Phy PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .phyChange, value: 80, duration: 4)
	public static let mgcPowerDown = HeroUltimate(id: 0, name: "Mgc PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .mgcChange, value: 100, duration: 4)
	public static let phyPowerUpAOE = HeroUltimate(id: 0, name: "Phy PowerUp AOE", target: .allFriendly, type: .buff, effectType: .powerUp, valueType: .phyChange, value: 100, duration: 4)
	public static let mgcPowerUpAOE = HeroUltimate(id: 0, name: "Mgc PowerUp AOE", target: .allFriendly, type: .buff, effectType: .powerUp, valueType: .mgcChange, value: 150, duration: 4)
	public static let phyPowerDownAOE = HeroUltimate(id: 0, name: "Phy PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .phyChange, value: 110, duration: 4)
	public static let mgcPowerDownAOE = HeroUltimate(id: 0, name: "Mgc PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .mgcChange, value: 150, duration: 8)
	public static let armorPowerDown = HeroUltimate(id: 0, name: "Armor PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .armorChange, value: 50.0, duration: 4)
	public static let magicResistPowerDown = HeroUltimate(id: 0, name: "Magic Resist PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .magicResistChange, value: 50, duration: 4)
	public static let armorPowerDownAOE = HeroUltimate(id: 0, name: "Armor PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .armorChange, value: 50, duration: 4)
	public static let magicResistPowerDownAOE = HeroUltimate(id: 0, name: "Magic Resist PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .magicResistChange, value: 50, duration: 4)
	public static let damageDealtReduction = HeroUltimate(id: 0, name: "Damage dealt reduction", target: .singleFirstEnemy, type: .debuff, effectType: .damageModifierDown, valueType: .damageDealt, value: 0.2, duration: 4)
	public static let damageDealtIncrease = HeroUltimate(id: 0, name: "Damage dealt increase", target: .singleFirstFriendly, type: .buff, effectType: .damageModifierUp, valueType: .damageDealt, value: 0.2, duration: 4)
	public static let damageTakenReduction = HeroUltimate(id: 0, name: "Damage taken reduction", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 0.45, duration: 3)
    public static let damageTakenReductionNonNFT = HeroUltimate(id: 0, name: "Damage taken reduction nonNFT", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 0.2, duration: 4)
	public static let damageTakenReduction2 = HeroUltimate(id: 0, name: "Damage taken reduction", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 0.4, duration: 2)
	public static let tankImmune = HeroUltimate(id: 0, name: "Tank Immune", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 1.0, duration: 2)
	public static let immune = HeroUltimate(id: 0, name: "Immune", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 1.0, duration: 3)
	public static let hasteRegenerationBlockDOT = HeroUltimate(id: 0, name: "Haste regeneration block DOT", target: .singleRandomEnemy, type: .debuff, effectType: .hasteRegenerationBlock, duration: 4)
	 
	 // MARK: - Tactical:
	 
	public static let hasteZero = HeroUltimate(id: 0, name: "Haste to zero", target: .singleFirstEnemy, type: .tactical, effectType: .hasteToZero)
	public static let reorderEnemyTeam = HeroUltimate(id: 0, name: "Reorder enemy team", target: .allEnemy, type: .tactical, effectType: .reorderEnemyTeam)
	public static let singleCleanse = HeroUltimate(id: 0, name: "Single cleanse", target: .mostDebuffedFriendly, type: .tactical, effectType: .cleanse)
	public static let singleDispell = HeroUltimate(id: 0, name: "Singel dispell", target: .mostBuffedEnemy, type: .tactical, effectType: .dispell)
	public static let aoeCleanse = HeroUltimate(id: 0, name: "AOE cleanse", target: .allFriendly, type: .tactical, effectType: .cleanse)
	public static let aoeDispell = HeroUltimate(id: 0, name: "AOE dispell", target: .allEnemy, type: .tactical, effectType: .dispell)
	public static let singleStun = HeroUltimate(id: 0, name: "Single stun", target: .singleLastEnemy, type: .debuff, effectType: .stun, duration: 4)
	public static let lastStun = HeroUltimate(id: 0, name: "Last stun", target: .singleLastEnemy, type: .debuff, effectType: .stun, duration: 3)
	public static let aoeStun = HeroUltimate(id: 0, name: "AOE stun", target: .allEnemy, type: .debuff, effectType: .stun, duration: 3)
	 
	 // MARK: - Once use spells:
	public static let enrage = HeroUltimate(id: 0, name: "Enrage", target: .selfUnder30, type: .buff, effectType: .damageModifierUp, valueType: .damageDealt, value: Constants.enrageMultiplier, duration: 1000, onceCast: true)
	 
	 
	 // MARK: - CrowdControll abilities:
	 

	
	/**
	 ****************
	 EXAMPLE ABILITIES:
	 ****************
	 */
	
	public static let example_whirlwind = HeroUltimate(id: 0, name: "Whirlwind", target: .allEnemy, type: .damage, valueType: .physical, value: Constants.example_whirlwindMultiplier)
	public static let example_physicalStrike = HeroUltimate(id: 1, name: "Physical strike", target: .singleFirstEnemy, type: .damage, valueType: .physical, value: Constants.example_physicalStrikeMultiplier)
	public static let example_physicalAmbush = HeroUltimate(id: 50, name: "Physical Ambush", target: .singleLastEnemy, type: .damage, valueType: .physical, value: 0.7)
	public static let example_magicStrike = HeroUltimate(id: 2, name: "Magic strike", target: .singleFirstEnemy, type: .damage, valueType: .magical, value: Constants.example_magicStrikeMultiplier)
	public static let example_mgcPhyStrike = HeroUltimate(id: 3, name: "Mixed(mgc+phy) strike", target: .singleFirstEnemy, type: .damage, valueType: .mgcPhyMixed, value: Constants.example_mgcPhyStrikeMultiplier)
	public static let example_trueStrike = HeroUltimate(id: 4, name: "True strike", target: .singleFirstEnemy, type: .trueDmg, valueType: .trueDmg, value: Constants.example_trueDamageStrikeValue)
	public static let example_tornado = HeroUltimate(id: 5, name: "Tornado", target: .allEnemy, type: .damage, valueType: .magical, value: Constants.example_tornadoMultiplier)
	public static let example_heal = HeroUltimate(id: 6, name: "Heal", target: .singleLowestCurrentHPFriendly, type: .heal, valueType: .magical, value: Constants.example_healMultiplier)
	public static let example_AOEHeal = HeroUltimate(id: 7, name: "AOE Heal", target: .allFriendly, type: .heal, valueType: .magical, value: Constants.example_AOEHealMultiplier)
	public static let example_magicLifeSteal = HeroUltimate(id: 8, name: "Magic Life Steal", target: .singleFirstEnemy, type: .lifeSteal, valueType: .magical, value: Constants.example_magicLifeStealMultiplier)
	public static let example_physicalLifeSteal = HeroUltimate(id: 9, name: "Physical Life Steal", target: .singleFirstEnemy, type: .lifeSteal, valueType: .physical, value: Constants.example_physicalLifeStealMultiplier)
	public static let example_physicalMaxHPStrike = HeroUltimate(id: 10, name: "Physical maxHP strike", target: .singleFirstEnemy, type: .maxHPDmg, valueType: .physical, value: Constants.example_physicalMaxHPStrikeMultiplier)
	public static let example_magicMaxHPStrike = HeroUltimate(id: 11, name: "Magic maxHP strike", target: .singleFirstEnemy, type: .maxHPDmg, valueType: .magical, value: Constants.example_magicMaxHPStrikeMultiplier)
	public static let example_physicalCurrentHPStrike = HeroUltimate(id: 12, name: "Physical currentHP strike", target: .singleFirstEnemy, type: .currentHPDmg, valueType: .physical, value: Constants.example_magicCurrentHPStrikeMultiplier)
	public static let example_magicCurrentHPStrike = HeroUltimate(id: 13, name: "Magic currentHP strike", target: .singleFirstEnemy, type: .currentHPDmg, valueType: .magical, value: Constants.example_magicCurrentHPStrikeMultiplier)
	public static let example_physicalMissingHPStrike = HeroUltimate(id: 14, name: "Physical missingHP strike", target: .singleFirstEnemy, type: .missingHPDmg, valueType: .physical, value: Constants.example_magicMissingHPStrikeMultiplier)
	public static let example_magicMissingHPStrike = HeroUltimate(id: 15, name: "Magic missingHP strike", target: .singleFirstEnemy, type: .missingHPDmg, valueType: .magical, value: Constants.example_magicMissingHPStrikeMultiplier)
	public static let example_execute = HeroUltimate(id: 16, name: "Execute", target: .singleEnemyRandomBelow20, type: .trueDmg, valueType: .trueDmg, value: Constants.example_executeMultiplier)
	public static let example_singleAbsorb = HeroUltimate(id: 17, name: "Absorb", target: .singleFirstFriendly, type: .absorb, valueType: .absorbUp, value: Constants.example_singleAbsorbValue)
	
	// MARK: - OverTime abilities:
	
	public static let example_physicalDOT = HeroUltimate(id: 100, name: "Physical Damage OT", target: .singleFirstEnemy, type: .debuff, effectType: .DOT, valueType: .physical, value: Constants.example_physicalDOTMultiplier, duration: 4)
	public static let example_magicalDOT = HeroUltimate(id: 101, name: "Magical Damage OT", target: .singleFirstEnemy, type: .debuff, effectType: .DOT, valueType: .magical, value: Constants.example_magicalDOTMultiplier, duration: 4)
	public static let example_magicalHOT = HeroUltimate(id: 102, name: "Magic heal OT", target: .singleLowestCurrentHPFriendly, type: .buff, effectType: .HOT, valueType: .magical, value: Constants.example_magicalHOTMultiplier, duration: 4)
	public static let example_physicalAOEDOT = HeroUltimate(id: 103, name: "Physical Damage AOE OT", target: .allEnemy, type: .debuff, effectType: .DOT, valueType: .physical, value: Constants.example_physicalAOEDOTMultiplier, duration: 4)
	public static let example_magicalAOEDOT = HeroUltimate(id: 104, name: "Magical Damage AOE OT", target: .allEnemy, type: .debuff, effectType: .DOT, valueType: .magical, value: Constants.example_magicalAOEDOTMultiplier, duration: 4)
	public static let example_magicalAOEHOT = HeroUltimate(id: 105, name: "Magic heal AOE OT", target: .allFriendly, type: .buff, effectType: .HOT, valueType: .magical, value: Constants.example_magicalAOEHOTMultiplier, duration: 4)
	
	// MARK: - Status Effect abilities
	
	public static let example_phyPowerUp = HeroUltimate(id: 150, name: "Phy PowerUp", target: .singleFirstFriendly, type: .buff, effectType: .powerUp, valueType: .phyChange, value: Constants.example_phyPowerUpValue, duration: 4)
	public static let example_mgcPowerUp = HeroUltimate(id: 151, name: "Mgc PowerUp", target: .singleFirstFriendly, type: .buff, effectType: .powerUp, valueType: .mgcChange, value: Constants.example_mgcPowerUpValue, duration: 4)
	public static let example_phyPowerDown = HeroUltimate(id: 152, name: "Phy PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .phyChange, value: Constants.example_phyPowerDownValue, duration: 4)
	public static let example_mgcPowerDown = HeroUltimate(id: 153, name: "Mgc PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .mgcChange, value: Constants.example_mgcPowerDownValue, duration: 4)
	public static let example_phyPowerUpAOE = HeroUltimate(id: 154, name: "Phy PowerUp AOE", target: .allFriendly, type: .buff, effectType: .powerUp, valueType: .phyChange, value: Constants.example_phyPowerUpAOEValue, duration: 4)
	public static let example_mgcPowerUpAOE = HeroUltimate(id: 155, name: "Mgc PowerUp AOE", target: .allFriendly, type: .buff, effectType: .powerUp, valueType: .mgcChange, value: Constants.example_mgcPowerUpAOEValue, duration: 4)
	public static let example_phyPowerDownAOE = HeroUltimate(id: 156, name: "Phy PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .phyChange, value: Constants.example_phyPowerDownAOEValue, duration: 4)
	public static let example_mgcPowerDownAOE = HeroUltimate(id: 157, name: "Mgc PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .mgcChange, value: Constants.example_mgcPowerDownAOEValue, duration: 4)
	public static let example_armorPowerDown = HeroUltimate(id: 158, name: "Armor PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .armorChange, value: Constants.example_armorPowerDownValue, duration: 4)
	public static let example_magicResistPowerDown = HeroUltimate(id: 158, name: "Magic Resist PowerDown", target: .singleFirstEnemy, type: .debuff, effectType: .powerDown, valueType: .magicResistChange, value: Constants.example_magicResistPowerDownValue, duration: 4)
	public static let example_armorPowerDownAOE = HeroUltimate(id: 158, name: "Armor PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .armorChange, value: Constants.example_armorPowerDownAOEValue, duration: 4)
	public static let example_magicResistPowerDownAOE = HeroUltimate(id: 158, name: "Magic Resist PowerDown AOE", target: .allEnemy, type: .debuff, effectType: .powerDown, valueType: .magicResistChange, value: Constants.example_magicResistPowerDownAOEValue, duration: 4)
	public static let example_damageDealtReduction = HeroUltimate(id: 159, name: "Damage dealt reduction", target: .singleFirstEnemy, type: .debuff, effectType: .damageModifierDown, valueType: .damageDealt, value: Constants.example_damageDealtReductionMultiplier, duration: 4)
	public static let example_damageDealtIncrease = HeroUltimate(id: 160, name: "Damage dealt increase", target: .singleFirstFriendly, type: .buff, effectType: .damageModifierUp, valueType: .damageDealt, value: Constants.example_damageDealtIncreaseMultiplier, duration: 4)
	public static let example_damageTakenReduction = HeroUltimate(id: 161, name: "Damage taken reduction", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: Constants.example_damageTakenReductionMultiplier, duration: 4)
	public static let example_immune = HeroUltimate(id: 162, name: "Immune", target: .`self`, type: .buff, effectType: .damageModifierDown, valueType: .damageTaken, value: 1.0, duration: 3)
	public static let example_hasteRegenerationBlockDOT = HeroUltimate(id: 163, name: "Haste regeneration block DOT", target: .singleRandomEnemy, type: .debuff, effectType: .hasteRegenerationBlock, duration: 4)
	
	// MARK: - Tactical:
	
	public static let example_hasteZero = HeroUltimate(id: 200, name: "Haste to zero", target: .singleFirstEnemy, type: .tactical, effectType: .hasteToZero)
	public static let example_reorderEnemyTeam = HeroUltimate(id: 201, name: "Reorder enemy team", target: .allEnemy, type: .tactical, effectType: .reorderEnemyTeam)
	public static let example_singleCleanse = HeroUltimate(id: 202, name: "Single cleanse", target: .mostDebuffedFriendly, type: .tactical, effectType: .cleanse)
	public static let example_singleDispell = HeroUltimate(id: 203, name: "Singel dispell", target: .mostBuffedEnemy, type: .tactical, effectType: .dispell)
	public static let example_aoeCleanse = HeroUltimate(id: 204, name: "AOE cleanse", target: .allFriendly, type: .tactical, effectType: .cleanse)
	public static let example_aoeDispell = HeroUltimate(id: 205, name: "AOE dispell", target: .allEnemy, type: .tactical, effectType: .dispell)
	public static let example_singleStun = HeroUltimate(id: 206, name: "Single stun", target: .singleFirstEnemy, type: .debuff, effectType: .stun, duration: 4)
	public static let example_aoeStun = HeroUltimate(id: 207, name: "AOE stun", target: .allEnemy, type: .debuff, effectType: .stun, duration: 3)
	
	// MARK: - Once use spells:
	public static let example_enrage = HeroUltimate(id: 300, name: "Enrage", target: .selfUnder30, type: .buff, effectType: .damageModifierUp, valueType: .damageDealt, value: Constants.example_enrageMultiplier, duration: 1000, onceCast: true)
	
	
	// MARK: - CrowdControll abilities:
	
	public static func getAllAbility() -> [HeroUltimate] {
		var abilities: [HeroUltimate] = []
		
		abilities.append(example_whirlwind)
		abilities.append(example_physicalStrike)
		abilities.append(example_physicalAmbush)
		abilities.append(example_magicStrike)
		abilities.append(example_mgcPhyStrike)
		abilities.append(example_trueStrike)
		abilities.append(example_tornado)
		abilities.append(example_heal)
		abilities.append(example_AOEHeal)
		abilities.append(example_magicLifeSteal)
		abilities.append(example_physicalLifeSteal)
		abilities.append(example_physicalMaxHPStrike)
		abilities.append(example_magicMaxHPStrike)
		abilities.append(example_physicalCurrentHPStrike)
		abilities.append(example_magicCurrentHPStrike)
		abilities.append(example_physicalMissingHPStrike)
		abilities.append(example_magicMissingHPStrike)
		abilities.append(example_execute)
		abilities.append(example_singleAbsorb)
		abilities.append(example_physicalDOT)
		abilities.append(example_magicalDOT)
		abilities.append(example_magicalHOT)
		abilities.append(example_physicalAOEDOT)
		abilities.append(example_magicalAOEDOT)
		abilities.append(example_magicalAOEHOT)
		abilities.append(example_phyPowerUp)
		abilities.append(example_mgcPowerUp)
		abilities.append(example_phyPowerDown)
		abilities.append(example_mgcPowerDown)
		abilities.append(example_phyPowerUpAOE)
		abilities.append(example_mgcPowerUpAOE)
		abilities.append(example_phyPowerDownAOE)
		abilities.append(example_mgcPowerDownAOE)
		abilities.append(example_armorPowerDown)
		abilities.append(example_magicResistPowerDown)
		abilities.append(example_armorPowerDownAOE)
		abilities.append(example_magicResistPowerDownAOE)
		abilities.append(example_damageDealtReduction)
		abilities.append(example_damageDealtIncrease)
		abilities.append(example_damageTakenReduction)
		abilities.append(example_immune)
		abilities.append(example_hasteRegenerationBlockDOT)
		abilities.append(example_hasteZero)
		abilities.append(example_reorderEnemyTeam)
		abilities.append(example_singleCleanse)
		abilities.append(example_singleDispell)
		abilities.append(example_aoeCleanse)
		abilities.append(example_aoeDispell)
		abilities.append(example_singleStun)
		abilities.append(example_aoeStun)
		abilities.append(example_enrage)
		
		return abilities
	}
	
	public static func getAbility(by id: Int) -> HeroUltimate {
		return getAllAbility().first(where: { $0.id == id})!
	}
}
