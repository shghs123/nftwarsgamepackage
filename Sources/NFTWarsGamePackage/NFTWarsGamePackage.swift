// The Swift Programming Language
// https://docs.swift.org/swift-book
// Version: 0.3

public class GamePackageVersion {
    
    public static let version = "0.4"
}
